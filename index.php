<?php require_once("includes/inc_files.php"); if($session->is_logged_in()) {
	$user = User::find_by_id($_SESSION['biginsights']['inc']['user_id']);
	
	redirect_to("my-insights/index.php");
}
$current_page = "home";

if(isset($_SESSION['oauth_message'])) {
	$message = $_SESSION['oauth_message'];
	unset($_SESSION['oauth_message']);
}

?> <?php $page_title = "Big Insights | DEPLOY, MOBILIZE INSIGHTS, MAKE DECISIONS"; require_once("includes/themes/".THEME_NAME."/iheader.php"); ?> <header id="home"> <div class="color-overlay">
	
	
	<!-- HEADING, FEATURES AND REGISTRATION FORM CONTAINER -->
	<div class="container">
		
		<div class="row">
			
			<div class="col-md-6">
			
			<!-- SCREENSHOT -->
			<div data-device="macbook" data-orientation="landscape" data-color="black" class="home-screenshot device-mockup">
                <div class="device">
					<div class="screen"><img src="<?php echo WWW; ?>includes/themes/<?php echo THEME_NAME; ?>/homecss/bgi.png" alt="" 
width="577" height="433" class="img-responsive"></div>
                </div>
            </div>
			
		</div>
			
			<!-- RIGHT - HEADING AND TEXTS -->
			<div class="col-md-6 intro-section">
				
				<h1 class="intro text-left"> <span class="strong colored-text">Deploy,</span> Mobilize <br> <strong>Insights</strong>, Make 
<strong>Decisions</strong><br><br>
				</h1>
				
				<p class="sub-heading text-left">
				    A powerful <strong>Data Collection, Aggregation and Analytics Engine,</strong> available as an Android App, Android Library, Web-based Portal & JavaScript API
				</p>
				<p style="color:#ffffff;">
				FOR FIELD RESEARCH, USER STUDIES, CONSUMER SURVEYS, POLLS AND MORE.  
				</p>
				
				<!-- CTA BUTTONS -->
				<div id="cta-5" class="button-container">
				
				<a href="<?php echo WWW; ?>register.php" class="btn standard-button pull-left">Register</a>
				<a href="<?php echo WWW; ?>signin.php" id="signin_link" class="btn secondary-button-white pull-left">Login</a>
				
				</div>
				
			</div>
		</div>
		
	</div>
	<!-- /END HEADING, FEATURES AND REGISTRATION FORM CONTAINER -->
	
</div> </header> <?php require_once("includes/themes/".THEME_NAME."/ifooter.php"); ?>
