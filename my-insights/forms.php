<?php
include_once "inc/head.php";
require_once("../includes/inc_files.php");
require_once("genkey/genkeyv2.php");

$page_title = "Forms"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="Forms";


// create a new form
if (isset($_POST['createform'])) {

	include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
	$_POST= str_replace("'", "''", $_POST);
	$_POST= str_replace('"', '\"', $_POST);
		
    $formname=$_POST['formname'];
	$generatedkey = keygen(50);
	$sql = "INSERT INTO `forms` SET servekey='$generatedkey', name='$formname', createdby='$myid', created=NOW(), public='1'";
	mysqli_query($link, $sql) or die('Error, query failed');
	header("location:forms.php");
}
?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-list-alt icon-large"></i>Web Surveys </h3> 	
						<div class="newsurvey-left">
							<a href="#myModal" class="btn btn-info" data-toggle="modal"> <i class="icon-plus"></i> New Web Survey</a>
						</div>	
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->

				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<div class="col-md-12">
								<!-- forms -->
								<div class="widget contacts-widget">

									<!-- Widget head -->
									<div class="widget-head">
										<h5 class="pull-left"><i class="icon-list-alt"></i>All Web Surveys</h5>	
										<div class="widget-head-btns pull-right">
											<a href="#" class="wclose"><i class="icon-remove"></i></a>
										</div>
										<div class="clearfix"></div>
									</div>

									<!-- Widget body -->
									<div class="widget-body 300-scroll">

										<ul class="list-unstyled">
										
											<!-- number of forms -->
											<?
											if ($myadmin==0) { // if i am not admin
												// get list of forms that are public or I have created
												//$resultforms=mysqli_query($link, "select * from `forms` where createdby='$myid' OR //public='1' ORDER BY id DESC");
												$resultforms=mysqli_query($link, "select * from `forms` where createdby='$myid' ORDER BY id DESC");
												
											} else { // if i am admin
												// get all forms
												$resultforms=mysqli_query($link, "select * from `forms` where createdby='$myid' ORDER BY id DESC");
											}
											$thisformscount=mysqli_num_rows($resultforms);
											?>
											<li class="contact-alpha">
												All Surveys <span class="label label-info pull-right"><? echo $thisformscount; ?> total</span>
												<div class="clearfix"></div>
											</li>
											
											<? 
											while ($rowforms=mysqli_fetch_array($resultforms)){
												$thisformid=$rowforms['id'];
												$thisformgenkey = $rowforms['servekey'];
												$thisformname=$rowforms['name'];
												$thisformcreated=$rowforms['created'];
												$thisformclosed=$rowforms['closed'];
												$thisformpublic=$rowforms['public'];
												
												if ($thisformclosed!="0000-00-00") { // if is closed
													$createdtext="Created: ".$thisformcreated.", Closed: ".$thisformclosed;
													$closedicon="remove-circle";
													$closedcolor="red";
													$closedtooltip="Closed";
												} else {
													$createdtext="Created: ".$thisformcreated;
													$closedicon="ok-circle";
													$closedcolor="green";
													$closedtooltip="Open";
												}
												
												if ($thisformpublic=="1") { // if is public
													$icon="eye-open";
													$color="green";
													$tooltip="Public";
												} else {
													$icon="eye-close";
													$color="red";
													$tooltip="Private";
												}
											?>

												
											<!-- Single form -->
											<li class="c-list">
												<!-- Contact pic -->
												<div class="contact-pic">
													<i class="icon-<? echo $icon." ".$color; ?> bs-tooltip" title="<? echo $tooltip; ?>" data-placement="top"></i>
													<i class="icon-<? echo $closedicon." ".$closedcolor; ?> bs-tooltip" title="<? echo $closedtooltip; ?>" data-placement="top"></i>
												</div>
												<!-- Contact details -->
												<div class="contact-details">
													<div class="pull-left">
														<strong><a href="data.php?biginsightsid=<? echo $thisformgenkey; ?>"><? echo $thisformname; ?></a></strong>
														<small><? echo $createdtext; ?></small>
													</div>
													<!-- Call, Message and Email buttons -->
													<div class="pull-right">
														<a href="elements.php?biginsightsid=<? echo $thisformgenkey; ?>" class="btn btn-primary btn-xs bs-tooltip" title="Edit Form"><i class="icon-cogs"></i></a>
														<a href="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>" class="btn btn-success btn-xs bs-tooltip" title="Settings"><i class="icon-cog"></i></a>
														<a href="invitations.php?biginsightsid=<? echo $thisformgenkey; ?>" class="btn btn-info btn-xs bs-tooltip" title="Invitations"><i class="icon-envelope"></i></a>
														<a href="data.php?biginsightsid=<? echo $thisformgenkey; ?>" class="btn btn-warning btn-xs bs-tooltip" title="Submissions & Statistics"><i class="icon-bar-chart"></i></a>
													</div>
													<div class="clearfix"></div>
												</div>
											</li>
											<!-- end single form -->
										<? } ?>
										</ul>
										
									</div>

									<!-- Widget foot -->
									<div class="widget-foot">
									</div>

								</div>
						
							</div>
						</div>
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      <? include_once "inc/foot.php"; ?>

									<!-- Modal -->
									<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										 <div class="modal-dialog">
										   <div class="modal-content">
											   <div class="modal-header">
												 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
												 <h4 class="modal-title">Create a New Survey</h4>
											   </div>
											   <div class="modal-body">
												 <form class="form-horizontal" id="ValidForm" role="form" method="post" action="forms.php">
									  
													<div class="form-group">
													  <label class="col-lg-2 control-label">Survey Name</label>
													  <div class="col-lg-10">
														<input type="text" name="formname" class="form-control" placeholder="Descriptive Name">
													  </div>
													</div>

													<hr>
													<div class="form-group">
													  <div class="col-lg-offset-2 col-lg-10">
														<button type="submit" class="btn btn-primary pull-right" name="createform">Create Survey</button>
													  </div>
													</div>
												  </form>
											   </div>
										</div>
									</div>
	</body>	
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>
