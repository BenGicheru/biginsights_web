<?
	// get form id
	if ((isset($_GET['biginsightsid'])) ) {
    //$thisformid=$_GET['id'];
	$thisformgenkey=$_GET['biginsightsid'];
	$thisformkey=$_GET['key'];
    $resultform=mysqli_query($link, "select * from `forms` WHERE servekey='$thisformgenkey' LIMIT 1");
	} else {
		header("location:viewformfinish.php");
	}
	
	// make sure there's actually such a form
	if (mysqli_num_rows($resultform)=='0'){
		header("location:viewformfinish.php");
	}
	
	// get form info
	while ($rowform=mysqli_fetch_array($resultform)){
		$thisformkey = $rowform['servekey'];
		$thisformname=$rowform['name'];
		$thisformcreatedby=$rowform['createdby'];
		$thisformcreated=$rowform['created'];
		$thisformclosed=$rowform['closed'];
		$thisformthankyou=$rowform['thankyoumessage'];
		$thisforminaccessible=$rowform['inaccessible'];
		$thisformtheme=$rowform['theme'];
		$thisformredirecturl=$rowform['redirecturl'];
		$thisformcssfile=$rowform['cssfile'];
		$thisformfavicon=$rowform['favicon'];
		$thisformbgimage=$rowform['bgimage'];
		$thisformsubmitstring=$rowform['submitstring'];
		$thisforminvsubject=$rowform['invitationsubject'];
		$thisforminvbody=$rowform['invitationbody'];
		$thisforminstructions=$rowform['forminstructions'];
		$thisformpublic=$rowform['public'];
		$thisforminvitation=$rowform['invitation'];
		$thisformmultiple=$rowform['multiple'];
	}
	
	// rename image url in instructions
	$thisforminstructions = str_replace('<img', '<img class="img-responsive" ', $thisforminstructions);
	
	// check if this user is invited and has the right key
	$resultinvited=mysqli_query($link, "select * from `invitations` WHERE formid='$thisformgenkey' AND ikey='$thisformkey' AND deleted='0'");
	$thisinvited=mysqli_num_rows($resultinvited);
	
	if ($thisinvited=='0') {
		$thissubmitted=0;
	} else {
		while ($rowinvited=mysqli_fetch_array($resultinvited)){
			$thisinvitationid=$rowinvited['id'];
		}
		// check if this user has submitted
		$resultsubmitted=mysqli_query($link, "select * from `submissions` WHERE formid='$thisformgenkey' AND invitationid='$thisinvitationid' LIMIT 1");
		$thissubmitted=mysqli_num_rows($resultsubmitted);
	}
	
	if (($thisformcreatedby!=$myid) && ($myadmin!='1')) {
		// if this form is closed, take them out
		if ($thisformclosed!='0000-00-00') {
			header("location:viewformfinish.php?biginsightsid=".$thisformgenkey);
		}
		
		// if this form is invitation based and user isn't invited, take them out
		if (($thisforminvitation=='1') && ($thisinvited=='0')) {
			header("location:viewformfinish.php?biginsightsid=".$thisformgenkey."&inaccessible1");
		}
		
		// if this form doesn't allow multiple submissions and the user has already submitted, take them out
		if (($thisformmultiple=='0') && ($thissubmitted=='1')) {
			header("location:viewformfinish.php?biginsightsid=".$thisformgenkey."&inaccessible");
		}
	}
?>