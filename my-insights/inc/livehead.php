<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title><? echo $thisformname; ?></title>
      
		<!-- Google web fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- jQuery -->
		<script src="js/jquery.js"></script>
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- jQuery UI -->
		<link href="css/jquery-ui.css" rel="stylesheet">
		<!-- datetimepicker CSS -->
		<link href="css/datetimepicker.min.css" rel="stylesheet">
		<!-- Bootstrap Switch -->
		<link href="css/bootstrap-switch.css" rel="stylesheet">
		<!-- jQuery Datatables -->
		<link href="css/jquery.dataTables.css" rel="stylesheet">
		<!-- Rateit -->
		<link href="css/rateit.css" rel="stylesheet">		
		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<!-- Default CSS -->
		<link href="css/style.css" rel="stylesheet">		
		<? if ($thisformfavicon!="") { // if there's a favicon ?>
		<!-- Favicon -->
		<link rel="shortcut icon" href="uploads/<? echo $thisformfavicon; ?>">
		<? } ?>
		<? if ($thisformtheme!="") { // if there's a custom css ?>
		<!-- custom css -->
		<link href="themes/<? echo $thisformtheme; ?>" rel="stylesheet">
		<? } ?>
		<? if ($thisformcssfile!="") { // if there's an uploaded custom css ?>
		<!-- custom css (uploaded) -->
		<link href="uploads/<? echo $thisformcssfile; ?>" rel="stylesheet">
		<? } ?>
		<? if ($thisformbgimage!="") { // if there's a background image ?>
		<!-- get background image -->
		<style type="text/css">
		body { 
		 	  background: url(uploads/<? echo $thisformbgimage; ?>) no-repeat center center fixed;
		 	 -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;
		 }
		</style>
		<? } ?>
		<script>
		$().ready(function() {
			// validate form on keyup and submit
			$("#ValidForm").validate({
				errorElement:'div',
				rules: {
					<?
					// get elements list
					$resultelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0' ORDER BY serial ASC");
					while ($rowelement=mysqli_fetch_array($resultelement)){
						$elementid=$rowelement['id'];
						$elementtype=$rowelement['type'];
						
						$name=$rowelement['name'];
						$label=$rowelement['label'];
						$placeholder=$rowelement['placeholder'];
						$url=$rowelement['url'];
						$email=$rowelement['email'];
						$number=$rowelement['number'];
						$digits=$rowelement['digits'];
						$creditcard=$rowelement['creditcard'];
						$minlength=$rowelement['minlength'];
						$maxlength=$rowelement['maxlength'];
						$min=$rowelement['min'];
						$max=$rowelement['max'];
						$dateformat=$rowelement['dateformat'];
						$timeformat=$rowelement['timeformat'];
						$extensions=$rowelement['extensions'];
						$onvalue=$rowelement['onvalue'];
						$offvalue=$rowelement['offvalue'];
						$required=$rowelement['required'];
						
						$extensions = preg_replace('/\s+/', '', $extensions); // remove whitespaces
						$extensions = str_replace(',', '|', $extensions); // replace "," with "|"
						
						echo $elementid.": {";
						if ($required!="") {
						echo "	required: true,";
						}
						if ($extensions!="") {
						echo "	extension: '".$extensions."',";						
						}
						if ($minlength!="") {
						echo "	minlength: ".$minlength.",";
						}
						if ($maxlength!="") {
						echo "	maxlength: ".$maxlength.",";
						}
						if ($min!="") {
						echo "	min: ".$min.",";
						}
						if ($max!="") {
						echo "	max: ".$max.",";
						}
						if ($url=="on") {
						echo "	url: true,";						
						}
						if ($email=="on") {
						echo "	email: true,";						
						}
						if ($number=="on") {
						echo "	number: true,";						
						}
						if ($digits=="on") {
						echo "	digits: true,";						
						}
						if ($creditcard=="on") {
						echo "	creditcard: true,";						
						}
						echo "},";
					}
					?>
				},
			});
		});
		</script>
	</head>
