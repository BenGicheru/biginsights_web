<?
	// get list id
	if ((isset($_GET['biginsightsid']))) {
    $thislistid=$_GET['biginsightsid'];
    $resultlist=mysqli_query($link, "select * from `lists` WHERE id='$thislistid' LIMIT 1");
	} else {
		//header("location:lists.php");
	}
	
	// make sure there's actually such a list
	if (mysqli_num_rows($resultlist)=='0'){
		header("location:lists.php");
	}
	
	// get list info
	while ($rowlist=mysqli_fetch_array($resultlist)){
		$thislistname=$rowlist['name'];
		$thislistcreatedby=$rowlist['createdby'];
		$thislistcreated=$rowlist['created'];
		$thislistpublic=$rowlist['public'];
		$thislistdeleted=$rowlist['deleted'];
	}
	
	if ($thislistpublic==1) {
		$thislistpublictext="public list";
		$thislistswitchtext="Make Private";
		$thislistswitchicon="eye-close";
	} else {
		$thislistpublictext="private list";
		$thislistswitchtext="Make Public";
		$thislistswitchicon="eye-open";
	}
	
	// make sure i have the right to see this list
	// if i am not admin AND this list isn't public & not created by me, THEN get me out
	if  (($myadmin==0) && (($thislistpublic==0) && ($thislistcreatedby!='$myid'))) {
		header("location:lists.php");
	}
?>