<?
	// get form id
	if ((isset($_GET['biginsightsid']))) {
    //$thisformid=$_GET['id'];
	$thisformgenkey=$_GET['biginsightsid'];
    $resultform=mysqli_query($link, "select * from `forms` WHERE servekey='$thisformgenkey' LIMIT 1");
	} else {
		header("location:forms.php");
	}
	
	// make sure there's actually such a form
	if (mysqli_num_rows($resultform)=='0'){
		header("location:forms.php");
	}
	
	// get form info
	while ($rowform=mysqli_fetch_array($resultform)){
		$thisformkey=$rowform['servekey'];
		$thisformname=$rowform['name'];
		$thisformcreatedby=$rowform['createdby'];
		$thisformcreated=$rowform['created'];
		$thisformclosed=$rowform['closed'];
		$thisformthankyou=$rowform['thankyoumessage'];
		$thisforminaccessible=$rowform['inaccessible'];
		$thisformtheme=$rowform['theme'];
		$thisformredirecturl=$rowform['redirecturl'];
		$thisformcssfile=$rowform['cssfile'];
		$thisformfavicon=$rowform['favicon'];
		$thisformbgimage=$rowform['bgimage'];
		$thisformsubmitstring=$rowform['submitstring'];
		$thisforminvsubject=$rowform['invitationsubject'];
		$thisforminvbody=$rowform['invitationbody'];
		$thisforminstructions=$rowform['forminstructions'];
		$thisformpublic=$rowform['public'];
		$thisforminvitation=$rowform['invitation'];
		$thisformmultiple=$rowform['multiple'];
	}
	
	// get "checked" status for the form
	if ($thisforminvitation==1) {
		$invitationchecked="checked";
	}
	if ($thisformmultiple==1) {
		$multiplechecked="checked";
	}
	
	// count invitations
	$resultforminv=mysqli_query($link, "select * from `invitations` WHERE formid='$thisformgenkey' AND deleted='0'");
	$thisforminvtotal=mysqli_num_rows($resultforminv);
	
	// count submissions
	$resultformsub=mysqli_query($link, "select * from `submissions` WHERE formid='$thisformgenkey'");
	$thisformsubtotal=mysqli_num_rows($resultformsub);
	
	// get the percentage
	if ($thisforminvtotal!='0') {
		$subinvpercent=round(($thisformsubtotal/$thisforminvtotal)*100);
		$subinvpercent=$subinvpercent." %";
	} else {
		$subinvpercent="n/a";
	}
	
	if ($thisformpublic==1) {
		$thisformpublictext="public form";
		$thisformswitchtext="Make Private";
		$thisformswitchicon="eye-close";
	} else {
		$thisformpublictext="private form";
		$thisformswitchtext="Make Public";
		$thisformswitchicon="eye-open";
	}
	
	if ($thisformclosed=="0000-00-00") {
		$thisformclosecolor="danger";
		$thisformclosetext="Close Form";
		$thisformcloseicon="remove-circle";
	} else {
		$thisformclosecolor="success";
		$thisformclosetext="Open Form";
		$thisformcloseicon="ok-circle";
	}
	
	// get total main elements
	$resulttotalelements=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0'");
	$formmainelements=mysqli_num_rows($resulttotalelements);
	
	// make sure i have the right to see this list
	// if i am not admin AND this list isn't public & not created by me, THEN get me out
	if  (($myadmin==0) && ($thisformpublic==0) && ($thisformcreatedby!=$myid)) {
		header("location:forms.php");
	}
?>