<div class="sidebar">
                  <!-- Logo starts -->
                  <div class="logo">
                     <h1><a href="index.php">My Insights</a></h1>
                  </div>
                  <!-- Logo ends -->
                  
                  <!-- Sidebar buttons starts -->
                  <div class="sidebar-buttons text-center">
                     <!-- User button -->
                     <div class="btn-group">
                       <a href="qpanel/myinfo.php" class="btn btn-black btn-xs"><i class="icon-user"></i></a>
                       <a href="qpanel/myinfo.php" class="btn btn-danger btn-xs"><? echo $myname; ?></a>
                     </div>
                     <!-- Logout button -->
                     <div class="btn-group">
                       <a href="index.php?logout" class="btn btn-black btn-xs"><i class="icon-off"></i></a>
                       <a href="index.php?logout" class="btn btn-danger btn-xs">Logout</a>
                     </div>
                  </div>
                  <!-- Sidebar buttons ends -->
                  
                  <!-- Sidebar navigation starts -->
				  
				  <div class="sidebar-dropdown"><a href="#">Navigation</a></div>
				  
                  <div class="sidey">
                     <ul class="nav">
                         <!-- Main navigation. Refer Notes.txt files for reference. -->
                         
                         <!-- Use the class "current" in main menu to hightlight current main menu -->
                         <li><a href="dashboard.php"><i class="icon-desktop"></i> Dashboard</a></li>
						 <li><a href="forms.php"><i class="icon-list-alt"></i> Surveys</a></li>
						 <li><a href="lists.php"><i class="icon-list"></i> Lists</a></li>
						 <?
						 // only admin can see this
						if ($myadmin==1) {
						 ?>
						 <li><a href="emailsettings.php"><i class="icon-forward"></i> Email Settings</a></li>
						 <li><a href="systemusers.php"><i class="icon-user"></i> Users</a></li>
						<? } ?>
                     </ul>               
                  </div>
                  <!-- Sidebar navigation ends -->
                  
            </div>