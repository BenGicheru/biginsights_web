<?
// get elements list
$resultelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0' ORDER BY serial ASC");
while ($rowelement=mysqli_fetch_array($resultelement)){
	$elementid=$rowelement['id'];
	$elementtype=$rowelement['type'];
	
	$name=$rowelement['name'];
	$label=$rowelement['label'];
	$placeholder=$rowelement['placeholder'];
	$url=$rowelement['url'];
	$email=$rowelement['email'];
	$number=$rowelement['number'];
	$digits=$rowelement['digits'];
	$creditcard=$rowelement['creditcard'];
	$minlength=$rowelement['minlength'];
	$maxlength=$rowelement['maxlength'];
	$min=$rowelement['min'];
	$max=$rowelement['max'];
	$dateformat=$rowelement['dateformat'];
	$timeformat=$rowelement['timeformat'];
	$extensions=$rowelement['extensions'];
	$onvalue=$rowelement['onvalue'];
	$offvalue=$rowelement['offvalue'];
	$required=$rowelement['required'];
	
	// interpret to form values
	if ($required=="on") {
		$required="checked";
	} else {
		$required="";
	}
	
	if ($url!="") {
		$inputtype="url";
		$inputtypetext="URL";
	} else if ($email!="") {
		$inputtype="email";
		$inputtypetext="Email";
	} else if ($number!="") {
		$inputtype="number";
		$inputtypetext="Number";
	} else if ($digits!="") {
		$inputtype="digits";
		$inputtypetext="Digits";
	} else if ($creditcard!="") {
		$inputtype="creditcard";
		$inputtypetext="Credit Card";
	}
?>
<? if ($elementtype=="textbox") { ?>
<!-- Text Box Modal -->
	<div id="<? echo $elementid; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Text Box Settings</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $elementid; ?>">
									  
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-10">
								<input type="text" name="name" value="<? echo $name; ?>" class="form-control" placeholder="element name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label" value="<? echo $label; ?>" class="form-control" placeholder="label">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Required</label>
							<div class="col-lg-10">
								<input type="checkbox" name="required" id="inlineCheckbox1" <? echo $required; ?>>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Minimum length</label>
							<div class="col-lg-10">
								<input type="text" name="minlength" value="<? echo $minlength; ?>" class="form-control" placeholder="minimum length">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Maximum Length</label>
							<div class="col-lg-10">
								<input type="text" name="maxlength" value="<? echo $maxlength; ?>" class="form-control" placeholder="maximum length">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Type</label>
							<div class="col-lg-10">
								<select class="form-control" name="inputtype">
									<?
									if (isset($inputtype)) {
									?>
									<option value="<? echo $inputtype; ?>"><? echo $inputtypetext; ?></option>
									<?
									}
									?>
									<option value="any">Any</option>
									<option value="url">URL</option>
									<option value="email">Email</option>
									<option value="number">Number</option>
									<option value="digits">Digits</option>
									<option value="creditcard">Credit Card</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Minimum Value (numbers only)</label>
							<div class="col-lg-10">
								<input type="text" name="min" value="<? echo $min; ?>" class="form-control" placeholder="mimimum value">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Maximum Value (numbers only)</label>
							<div class="col-lg-10">
								<input type="text" name="max" value="<? echo $max; ?>" class="form-control" placeholder="maximum value">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Placeholder</label>
							<div class="col-lg-10">
								<input type="text" name="placeholder" value="<? echo $placeholder; ?>" class="form-control" placeholder="placeholder">
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } else if ($elementtype=="textarea") { ?>
	<!-- Text Area Modal -->
	<div id="<? echo $elementid; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Text Area Settings</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $elementid; ?>">
									  
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-10">
								<input type="text" name="name" value="<? echo $name; ?>" class="form-control" placeholder="element name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label" value="<? echo $label; ?>" class="form-control" placeholder="label">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Required</label>
							<div class="col-lg-10">
								<input type="checkbox" name="required" id="inlineCheckbox1" <? echo $required; ?>>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Minimum length</label>
							<div class="col-lg-10">
								<input type="text" name="minlength" value="<? echo $minlength; ?>" class="form-control" placeholder="minimum length">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Maximum Length</label>
							<div class="col-lg-10">
								<input type="text" name="maxlength" value="<? echo $maxlength; ?>" class="form-control" placeholder="maximum length">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Placeholder</label>
							<div class="col-lg-10">
								<input type="text" name="plaveholder" value="<? echo $placeholder; ?>" class="form-control" placeholder="placeholder">
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } else if ($elementtype=="radio") { ?>
	<!-- Radio Modal -->
	<div id="<? echo $elementid; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Radio Settings</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $elementid; ?>">
									  
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-10">
								<input type="text" name="name" value="<? echo $name; ?>" class="form-control" placeholder="Element Name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label"  value="<? echo $label; ?>"class="form-control" placeholder="label">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Required</label>
							<div class="col-lg-10">
								<input type="checkbox" name="required" id="inlineCheckbox1" <? echo $required; ?>>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } else if ($elementtype=="dropdown") { ?>
	<!-- Drop Down Modal -->
	<div id="<? echo $elementid; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Drop Down Settings</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $elementid; ?>">
									  
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-10">
								<input type="text" name="name" value="<? echo $name; ?>" class="form-control" placeholder="Element Name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label"  value="<? echo $label; ?>"class="form-control" placeholder="label">
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } else if ($elementtype=="checkbox") { ?>
	<!-- Checkbox Modal -->
	<div id="<? echo $elementid; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Checkbox Settings</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $elementid; ?>">
									  
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-10">
								<input type="text" name="name" value="<? echo $name; ?>" class="form-control" placeholder="element name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label" value="<? echo $label; ?>" class="form-control" placeholder="label">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Required</label>
							<div class="col-lg-10">
								<input type="checkbox" name="required" id="inlineCheckbox1" <? echo $required; ?>>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } else if ($elementtype=="upload") { ?>
	<!-- File Upload Box Modal -->
	<div id="<? echo $elementid; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">File Upload Box Settings</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $elementid; ?>">
									  
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-10">
								<input type="text" name="name" value="<? echo $name; ?>" class="form-control" placeholder="element name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label"  value="<? echo $label; ?>"class="form-control" placeholder="label">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Allowed Extensions</label>
							<div class="col-lg-10">
								<input type="text" name="extensions" value="<? echo $extensions; ?>" class="form-control" placeholder="allowed extensions, comma separated">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Required</label>
							<div class="col-lg-10">
								<input type="checkbox" name="required" id="inlineCheckbox1" <? echo $required; ?>>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } else if ($elementtype=="star") { ?>
	<!-- Star Rating Modal -->
	<div id="<? echo $elementid; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Star Rating Settings</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $elementid; ?>">
									  
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-10">
								<input type="text" name="name" value="<? echo $name; ?>" class="form-control" placeholder="element name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label" value="<? echo $label; ?>" class="form-control" placeholder="label">
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } else if ($elementtype=="toggle") { ?>
	<!-- Toggle Modal -->
	<div id="<? echo $elementid; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Toggle Settings</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $elementid; ?>">
									  
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-10">
								<input type="text" name="name" value="<? echo $name; ?>" class="form-control" placeholder="element name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label" value="<? echo $label; ?>" class="form-control" placeholder="label">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Off Value</label>
							<div class="col-lg-10">
								<input type="text" name="offvalue" value="<? echo $offvalue; ?>" class="form-control" placeholder="off value">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">On Value</label>
							<div class="col-lg-10">
								<input type="text" name="onvalue" value="<? echo $onvalue; ?>" class="form-control" placeholder="on value">
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } else if ($elementtype=="password") { ?>
	<!-- Password Modal -->
	<div id="<? echo $elementid; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Password Settings</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $elementid; ?>">
									  
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-10">
								<input type="text" name="name" value="<? echo $name; ?>" class="form-control" placeholder="element name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label"  value="<? echo $label; ?>" class="form-control" placeholder="label">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Placeholder</label>
							<div class="col-lg-10">
								<input type="text" name="plaveholder" value="<? echo $placeholder; ?>" class="form-control" placeholder="placeholder">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Required</label>
							<div class="col-lg-10">
								<input type="checkbox" name="required" id="inlineCheckbox1" <? echo $required; ?>>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Minimum length</label>
							<div class="col-lg-10">
								<input type="text" name="minlength" value="<? echo $minlength; ?>" class="form-control" placeholder="minimum length">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Maximum Length</label>
							<div class="col-lg-10">
								<input type="text" name="maxlength" value="<? echo $maxlength; ?>" class="form-control" placeholder="maximum length">
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } else if ($elementtype=="datepicker") { ?>
	<!-- Datepicker Modal -->
	<div id="<? echo $elementid; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Date Picker Settings</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $elementid; ?>">
									  
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-10">
								<input type="text" name="name" value="<? echo $name; ?>" class="form-control" placeholder="element name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label"  value="<? echo $label; ?>"class="form-control" placeholder="label">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Format</label>
							<div class="col-lg-10">
								<select class="form-control" name="dateformat">
									<?
									if ($dateformat!="") {
									?>
									<option><? echo $dateformat; ?></option>
									<?
									}
									?>
									<option>MM/dd/yyyy</option>
									<option>dd/MM/yyyy</option>
									<option>MM-dd-yyyy</option>
									<option>dd-MM-yyyy</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Required</label>
							<div class="col-lg-10">
								<input type="checkbox" name="required" id="inlineCheckbox1" <? echo $required; ?>>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } else if ($elementtype=="timepicker") { ?>
	<!-- Time Picker Modal -->
	<div id="<? echo $elementid; ?>Modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Time Picker Settings</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $elementid; ?>">
									  
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-10">
								<input type="text" name="name" value="<? echo $name; ?>" class="form-control" placeholder="element name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label" value="<? echo $label; ?>" class="form-control" placeholder="label">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Format</label>
							<div class="col-lg-10">
								<select class="form-control" name="timeformat">
									<?
									if ($timeformat!="") {
									?>
									<option><? echo $timeformat; ?></option>
									<?
									}
									?>
									<option>24h</option>
									<option>12h</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Required</label>
							<div class="col-lg-10">
								<input type="checkbox" name="required" id="inlineCheckbox1" <? echo $required; ?>>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } // end if ?>
	<? } // end while?>
	<?
	// get sub elements list
	$resultsubelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof!='0' AND deleted='0' ORDER BY serial ASC");
	while ($rowsubelement=mysqli_fetch_array($resultsubelement)){
		$subelementid=$rowsubelement['id'];
		$subelementlabel=$rowsubelement['label'];
	?>
	<!-- Time Picker Modal -->
	<div id="<? echo $subelementid; ?>Sub" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h4 class="modal-title">Sub-Element Label</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal ValidForm4" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
					<input type="hidden" name="elementid" value="<? echo $subelementid; ?>">
						<div class="form-group">
							<label class="col-lg-2 control-label">Label</label>
							<div class="col-lg-10">
								<input type="text" name="label" value="<? echo $subelementlabel; ?>" class="form-control" placeholder="label">
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary btn-xs pull-right" name="saveelement">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<? } ?>