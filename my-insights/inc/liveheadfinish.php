<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title><? echo $thisformname; ?></title>
      
		<!-- Google web fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- jQuery -->
		<script src="js/jquery.js"></script>
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- jQuery UI -->
		<link href="css/jquery-ui.css" rel="stylesheet">
		<!-- datetimepicker CSS -->
		<link href="css/datetimepicker.min.css" rel="stylesheet">
		<!-- Bootstrap Switch -->
		<link href="css/bootstrap-switch.css" rel="stylesheet">
		<!-- jQuery Datatables -->
		<link href="css/jquery.dataTables.css" rel="stylesheet">
		<!-- Rateit -->
		<link href="css/rateit.css" rel="stylesheet">		
		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">		
		<? if ($thisformfavicon!="") { // if there's a favicon ?>
		<!-- Favicon -->
		<link rel="shortcut icon" href="uploads/<? echo $thisformfavicon; ?>">
		<? } ?>
		<? if ($thisformtheme!="") { // if there's a custom css ?>
		<!-- custom css -->
		<link href="themes/<? echo $thisformtheme; ?>" rel="stylesheet">
		<? } ?>
		<? if ($thisformcssfile!="") { // if there's an uploaded custom css ?>
		<!-- custom css (uploaded) -->
		<link href="uploads/<? echo $thisformcssfile; ?>" rel="stylesheet">
		<? } ?>
		<? if ($thisformbgimage!="") { // if there's a background image ?>
		<!-- get background image -->
		<style type="text/css">
		body { 
		 	  background: url(uploads/<? echo $thisformbgimage; ?>) no-repeat center center fixed;
		 	 -webkit-background-size: cover;
			  -moz-background-size: cover;
			  -o-background-size: cover;
			  background-size: cover;
		 }
		</style>
		<? } ?>
	</head>
