<div class="sidebar">
                  <!-- Logo starts -->
                  <div class="logo">
                     <h1><a href="index.php"><img src="img/biginsights-logo.png" width="230px" height="92px"/></a></h1>
                  </div>
                  <!-- Logo ends -->
                  
                  
                  
                  <!-- Sidebar navigation starts -->
				  
				  <div class="sidebar-dropdown"><a href="#">Navigation</a></div>
				  
                  <div class="sidey">
                     <ul class="nav">
                         <!-- Main navigation. Refer Notes.txt files for reference. -->
                         
                         <!-- Use the class "current" in main menu to hightlight current main menu -->
                         <li><a href="dashboard.php"><i class="icon-desktop"></i> Dashboard</a></li>
						 <li><a href="forms.php"><i class="icon-list-alt"></i>Web Surveys</a></li>
						<li><a href="mobidash.php"><i class="icon-list-alt"></i> Mobile Surveys</a></li>
						 <li><a href="lists.php"><i class="icon-list"></i> Lists</a></li>
						 <?
						 // only admin can see this
						if ($myadmin==1) {
						 ?>
						 <li><a href="emailsettings.php"><i class="icon-forward"></i> Email Settings</a></li>
						 <li><a href="systemusers.php"><i class="icon-user"></i> Users</a></li>
						<? } ?>
                     </ul>               
                  </div>
                  <!-- Sidebar navigation ends -->
                  
            </div>
