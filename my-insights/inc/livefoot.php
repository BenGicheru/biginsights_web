       <!-- Scroll to top -->
      <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 
	  
	 <a class="biginsights-left" href="http://biginsights.io" target="_blank"><i></i></a>
	
	  <script src="js/bootstrap.min.js"></script>
	  
	  <script src="js/datetimepicker.min.js"></script>
      
      <script src="js/sparkline.js"></script>
     
      <script src="js/jquery-ui-1.10.2.custom.min.js"></script>
      
      
      <script src="js/jquery.flot.min.js"></script>     
      <script src="js/jquery.flot.pie.min.js"></script>
      <script src="js/jquery.flot.resize.min.js"></script>
	  
	  <script src="js/jquery.dataTables.min.js"></script>
	
	  <script src="js/bootstrap-switch.min.js"></script>
	
	  <script src="js/jquery.rateit.min.js"></script>

	  <script src="js/jquery.slimscroll.min.js"></script>
	
	  <script src="js/respond.min.js"></script>

	  <script src="js/html5shiv.js"></script>

	  <script src="js/custom.notification.js"></script>
	  <script src="js/custom.js"></script>
	
	  <script src="js/jquery.fitvids.js"></script>
		<script>
			$(".container").fitVids();
			$(".container").fitVids({ customSelector: "iframe[src^='http://x20labs.com']"});
		</script>
		<script src="js/jquery.validate.js"></script>
		<script src="js/additional-methods.js"></script>
      
	</body>	
</html>