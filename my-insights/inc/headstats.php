<?php
include 'inc/connect.php';
require_once("../includes/inc_files.php"); 

if(!$session->is_logged_in()) {redirect_to("../signin.php");}
else  {
	$user = User::find_by_id($_SESSION['biginsights']['inc']['user_id']);
	$myid=$user->id;
}

include 'inc/getmyinfo.php';
include 'inc/general.php';
?>

<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title><? echo $pagetitle; ?> - Big Insights</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Deploy, Generate Insights, Make Decisions.">
		<meta name="keywords" content="big-insights, insights, big, big-data, better-decisions, digital-research,">
		<meta name="author" content="Big Insights">

		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
		
	<!--map comes here-->
<link type="text/css" rel="stylesheet" href="ui1assets/css/map.css" />
<link type="text/css" rel="stylesheet" href="vmapassets/css/vectormap.css" />
<link href="vmapassets/plugins/jvectormap/jquery-jvectormap-2.0.1.css" rel="stylesheet" type="text/css" media="screen"/> 
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- jQuery -->
	    <script src="js/jquery.js"></script>
		<!-- Validate plugin -->
		<script src="js/jquery.validate.js"></script>
		<script>
		$().ready(function() {
			// validate form on keyup and submit
			$("#ValidForm").validate({
				rules: {
					name: {
						required: true,
						maxlength: 20
					},
					username: {
						required: true,
						maxlength: 20
					},
					sendername: "required",
					email: {
						required: true,
						email: true
					},
					formname: {
						required: true,
						maxlength: 100
					},
					listname: {
						required: true,
						maxlength: 100
					},
					password: "required",
					submitstring: "required",
					redirecturl: {
						url: true
					},
					baseurl: {
						required: true,
						url: true
					},
					elementname: {
						required: true,
						maxlength: 8
					}
				},
				messages: {
					name: {
						required: "Please enter some name",
						maxlength: "maximum 20 characters"
					},
					username: {
						required: "Please enter some username",
						maxlength: "maximum 20 characters"
					},
					sendername: "Please enter sender name",
					email: "Please enter a valid email address",
					formname: {
						required: "Please specify a name for your form",
						maxlength: "100 characters maximum"
					},
					listname: {
						required: "Please specify a name for your list",
						maxlength: "100 characters maximum"
					},
					password: "Please enter some password",
					submitstring: "Please enter value, ex: submit, send, ..etc",
					redirecturl: {
						url: "Please enter a valid url"
					},
					baseurl: {
						url: "Please enter a valid url"
					},
					elementname: {
						required: "Please enter element name",
						maxlength: "8 characaters maximum"
					}
				}
			});
			$("#ValidForm2").validate({
				rules: {
					username: "required",
					email: "required"
				},
				messages: {
					username: "Please enter your username",
					email: "Please enter your email"
				}
			});
			$("#ValidForm3").validate({
				rules: {
					elementname: {
						required: true,
						maxlength: 8
					}
				},
				messages: {
					elementname: {
						required: "Please enter element name",
						maxlength: "8 characaters maximum"
					}
				}
			});
		});
		$(document).ready(function() {

			$('form').each(function() {  // attach to all form elements on page
				$(this).validate({       // initialize plugin on each form
					rules: {
						name: {
							required: true,
							maxlength: 8
						},
						label: "required",
						min: {
							digits: true
						},
						max: {
							digits: true
						},
						minlength: {
							digits: true
						},
						maxlength: {
							digits: true
						},
						onvlaue: {
							required: true,
							maxlength: 5,
						},
						offvalue: {
							required: true,
							maxlength: 5,
						}
					},
					messages: {
						name: {
							required: "Please enter element name",
							maxlength: "8 characaters maximum"
						},
						label: "required",
						min: {
							digits: "Only digits allowed"
						},
						max: {
							digits: "Only digits allowed"
						},
						minlength: {
							digits: "Only digits allowed"
						},
						maxlength: {
							digits: "Only digits allowed"
						},
						onvlaue: {
							required: "Please specify what to show as 'on' value",
							maxlength: "Maximum characters allowed is 5",
						},
						offvalue: {
							required: "Please specify what to show as 'off' value",
							maxlength: "Maximum characters allowed is 5",
						}
					}
				});
			});

		});
		</script>
	<!-- end validate plugin -->
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- jQuery UI -->
		<link href="css/jquery-ui.css" rel="stylesheet">
		
		
		<!-- jQuery Gritter -->
		<link href="css/jquery.gritter.css" rel="stylesheet">
		<!-- Bootstrap Switch -->
		<link href="css/bootstrap-switch.css" rel="stylesheet">
		<!-- jQuery Datatables -->
		<link href="css/jquery.dataTables.css" rel="stylesheet">
		<!-- Rateit -->
		<link href="css/rateit.css" rel="stylesheet">
		<!-- jQuery prettyPhoto -->
		<link href="css/prettyPhoto.css" rel="stylesheet">
		<!-- Full calendar -->
		<link href="css/fullcalendar.css" rel="stylesheet">		
		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">		
		<!-- Custom CSS -->
		<link href="css/style.css" rel="stylesheet">
		<link href="font-awesome/css/font-awesome.css" rel="stylesheet">
		
		<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="css/style-ie.css" />
		<![endif]-->
            
		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.png">
		
		<!--tinymce -->
		<script type="text/javascript" src="tinymce/tinymce.min.js"></script>
		<script type="text/javascript">
		tinymce.init({
			selector: "textarea.instructions",
			plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste jbimages"
			],
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
			fullscreen_new_window : true
		});
		tinymce.init({
			selector: "textarea.email",
			plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
			],
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
			fullscreen_new_window : true
		});
		</script>

	</head>
	
