<!-- Scroll to top -->
      <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span> 
		
	  <!-- Javascript files -->
	  <!-- Sortable -->
	 <script>
	$(function() {
		$('.sortable').sortable({
			axis: 'y',
			stop: function (event, ui) {
				var data = $(this).sortable('serialize');

				// POST to server using $.post or $.ajax
				$.ajax({
					type: 'POST',
					url: 'inc/saveorder.php',
					data: { 
						theorder: data
					}
				});
			}
		});
	});
	</script>
	  <!-- Bootstrap JS -->
	  <script src="js/bootstrap.min.js"></script>
      <!-- Sparkline for Mini charts -->
      <script src="js/sparkline.js"></script>
      <!-- jQuery UI -->
      <script src="js/jquery-ui-1.10.2.custom.min.js"></script>
	  <script src="js/jqueryui-2.js"></script>
      
      <!-- jQuery flot -->
      <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
      <script src="js/jquery.flot.min.js"></script>     
      <script src="js/jquery.flot.pie.min.js"></script>
      <script src="js/jquery.flot.resize.min.js"></script>
	  
	  <!-- jQuery Knob -->
	  <script src="js/jquery.knob.js"></script>
	  <!-- jQuery Data Tables -->
	  <script src="js/jquery.dataTables.min.js"></script>
	  <!-- jQuery Knob -->
	  <script src="js/bootstrap-switch.min.js"></script>
	  <!-- jQuery Knob -->
	  <script src="js/jquery.rateit.min.js"></script>
	  <!-- jQuery prettyPhoto -->
	  <script src="js/jquery.prettyPhoto.js"></script>
	  <!-- jquery slim scroll -->
	  <script src="js/jquery.slimscroll.min.js"></script>
	  <!-- jQuery gritter -->
	  <script src="js/jquery.gritter.min.js"></script>
	  <!-- jQuery full calendar -->
	  <script src="js/fullcalendar.min.js"></script>
	  <!-- Respond JS for IE8 -->
	  <script src="js/respond.min.js"></script>
	  <!-- HTML5 Support for IE -->
	  <script src="js/html5shiv.js"></script>
	  <!-- Custom JS -->
	  <script src="js/custom.notification.js"></script>
	  <script src="js/custom.js"></script>