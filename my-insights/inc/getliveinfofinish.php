<?
	// get form id
	if ((isset($_GET['biginsightsid']))) {
    //$thisformid=$_GET['biginsightsid'];
	$thisformgenkey=$_GET['biginsightsid'];
    $resultform=mysqli_query($link, "select * from `forms` WHERE servekey='$thisformgenkey' LIMIT 1");
	}
	
	// get form info
	while ($rowform=mysqli_fetch_array($resultform)){
		$$thisformgenkey= $rowform['servekey'];
		$thisformname=$rowform['name'];
		$thisformcreatedby=$rowform['createdby'];
		$thisformcreated=$rowform['created'];
		$thisformclosed=$rowform['closed'];
		$thisformthankyou=$rowform['thankyoumessage'];
		$thisforminaccessible=$rowform['inaccessible'];
		$thisformtheme=$rowform['theme'];
		$thisformredirecturl=$rowform['redirecturl'];
		$thisformcssfile=$rowform['cssfile'];
		$thisformfavicon=$rowform['favicon'];
		$thisformbgimage=$rowform['bgimage'];
		$thisformsubmitstring=$rowform['submitstring'];
		$thisforminvsubject=$rowform['invitationsubject'];
		$thisforminvbody=$rowform['invitationbody'];
		$thisforminstructions=$rowform['forminstructions'];
		$thisformpublic=$rowform['public'];
		$thisforminvitation=$rowform['invitation'];
		$thisformmultiple=$rowform['multiple'];
	}

	// if this form is closed or has a thank you variable
	if ($thisformclosed!='0000-00-00') {
		if ((isset($thisforminaccessible)) && ($thisforminaccessible!="")) {
			$message=$thisforminaccessible;
		} else {
			$message="This form is inaccessible";
		}
	} else if (isset($_GET['thankyou'])) {
		if ((isset($thisformthankyou)) && ($thisformthankyou!="")) {
			$message=$thisformthankyou;
		} else {
			$message="Thank you!";
		}
	} else if (isset($_GET['inaccessible'])) {
		if ((isset($thisforminaccessible)) && ($thisforminaccessible!="")) {
			$message=$thisforminaccessible;
		} else {
			$message="This form is inaccessible";
		}
	}
	
	// if iam logged in, give me the regular message
	if (($thisformcreatedby==$myid) xor ($myadmin=='1')) {
		if ((isset($thisformthankyou)) && ($thisformthankyou!="")) {
			$message=$thisformthankyou;
		} else {
			$message="Thank you!";
		}
	}
	
	// make sure there's actually such a form
	if (mysqli_num_rows($resultform)=='0'){
		$message="This form does not exist";
	}
	
	// if redirect is set, do it
	if ((isset($_GET['thankyou'])) && (isset($thisformredirecturl)) && ($thisformredirecturl!="")){
		header("location:".$thisformredirecturl);
	}
?>