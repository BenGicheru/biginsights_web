<?php

include_once "inc/head.php";
require_once("../includes/inc_files.php");
$myuserid=$user->username;
require_once("../includes/themes/".THEME_NAME."/qheader.php");

$pagetitle="Questions";
if ((isset($_GET['biginsightsid']))) {
	$thisformgenkey=$_GET['biginsightsid'];
	} else {
		header("location:mobidash.php");
	}

?>

<?php

//Perform Search from our database
if(isset($_POST['action_type']))
{
    if ($_POST['action_type'] == 'search')
    {
        $search = mysqli_real_escape_string($link, strip_tags($_POST['searchText']));
        $sql = "select q_value, q_type,
                q_wording, q_options from mobiquestions
                where msurvey_id='$thisformgenkey' AND q_wording like '%$search%'
                or q_options like '%$search%'";
 
        $result = mysqli_query($link, $sql);
 
        if(!$result)
        {
            echo mysqli_error($link);
            exit();
        }
        $contact_list = array(); //Loop through each row on array and store the data to $contact_list[]
        while($rows = mysqli_fetch_array($result))
        {
            $contact_list[] = array('q_value' => $rows['q_value'],
                                    'q_type' => $rows['q_type'],
                                    'q_wording' => $rows['q_wording'],
                                    'q_options' => $rows['q_options']);
        }
        include 'mobilist.php';
        exit();
    }
}





//Insert or Update contact information
if(isset($_POST['action_type']))
{
	if ($_POST['action_type'] == 'add' or $_POST['action_type'] == 'edit')
	{
		//Sanitize the data and assign to variables
		$contact_id = mysqli_real_escape_string($link, strip_tags($_POST['ContactID']));
		$fname = mysqli_real_escape_string($link, strip_tags($_POST['fname']));
		$lname = mysqli_real_escape_string($link, strip_tags($_POST['lname']));
		$contact_no = mysqli_real_escape_string($link, strip_tags($_POST['ContactNo']));
		$ResAddress = mysqli_real_escape_string($link, strip_tags($_POST['ResAddress']));
		$Company = mysqli_real_escape_string($link, strip_tags($_POST['Company']));
		$CompAddress = mysqli_real_escape_string($link, strip_tags($_POST['CompAddress']));
				
		if ($_POST['action_type'] == 'add')
		{
			$sql = "insert into mobiquestions set 
					q_type = '$fname',
					q_wording = '$lname',
					q_options = '$contact_no',
					msurvey_id='$thisformgenkey'";
		}else{
			$sql = "update mobiquestions set 
					q_type = '$fname',
					q_wording = '$lname',
					q_options = '$contact_no', 
					msurvey_id='$thisformgenkey'
					where q_value = $contact_id";
		}
		
		
		if (!mysqli_query($link, $sql))
		{
			echo 'Error Saving Data. ' . mysqli_error($link);
			exit();	
		}
	}
	header('Location: mobiquestions.php?biginsightsid='.$thisformgenkey);
	exit();
}
//End Insert or Update contact information

//Start of edit contact read
$gresult = ''; //declare global variable
if(isset($_POST["action"]) and $_POST["action"]=="edit"){
	$id = (isset($_POST["ci"])? $_POST["ci"] : '');
	$sql = "select q_value, q_type, q_wording,
			q_options from mobiquestions 
			where q_value = $id";

	$result = mysqli_query($link, $sql);

	if(!$result)
	{
		echo mysqli_error($link);
		exit();
	}
	
	$gresult = mysqli_fetch_array($result);
	
	include 'mobiupdate.php';
	exit();
}
//end of edit contact read

//Start Delete Contact
if(isset($_POST["action"]) and $_POST["action"]=="delete"){
	$id = (isset($_POST["ci"])? $_POST["ci"] : '');
	$sql = "delete from mobiquestions 
			where q_value = $id";

	$result = mysqli_query($link, $sql);

	if(!$result)
	{
		echo mysqli_error($link);
		exit();
	}
	
}
//End Delete Contact

//Read contact information from database
$sql = "select q_value,
		q_type, q_wording, 
		q_options from mobiquestions where msurvey_id='$thisformgenkey'";

$result = mysqli_query($link, $sql);

if(!$result)
{
	echo mysqli_error($link);
	exit();
}
//Loo through each row on array and store the data to $contact_list[]
while($rows = mysqli_fetch_array($result))
{
	$contact_list[] = array('q_value' => $rows['q_value'], 
							'q_type' => $rows['q_type'],
							'q_wording' => $rows['q_wording'],
							'q_options' => $rows['q_options']);
}
include 'mobilist.php';
exit();
?>
