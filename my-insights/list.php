<?php 
include_once "inc/head.php";
require_once("../includes/inc_files.php");
$page_title = "Lists"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="Lists";
include_once "inc/getlistinfo.php";

// add a single recipient
if (isset($_POST['addrecipient'])) {

	include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
		
    $recname=$_POST['name'];
	$recemail=$_POST['email'];
	$sql = "INSERT INTO `recipients` SET name='$recname', email='$recemail', listid='$thislistid', deleted='0'";
	mysqli_query($link, $sql) or die('Error, query failed');
	header("location:list.php?biginsightsid=".$thislistid);
}

// delete recipient
if ((isset($_GET['delete'])) && (is_numeric($_GET['delete']))) {
    $deleteid=$_GET['delete'];

	$sql = "UPDATE recipients SET deleted='1' WHERE id='$deleteid' AND listid='$thislistid'";
    mysqli_query($link, $sql);
    header("location:list.php?biginsightsid=".$thislistid);
}

// add from csv
if (isset ($_POST['addcsv'])) {
    include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
		
    $file = $_FILES['uploaded_file']['tmp_name']; 
    $org = $_FILES['uploaded_file']['name'];
	
	// get file extension
	$ext=explode(".", $org);
    $extension=$ext[1];
	$extension = strtolower($extension); // convert it to lowercase
	
	// get file size
	$filesize=$_FILES['uploaded_file']['size'];
	echo $filesize."++++";
	
	
	if (($filesize > 0) AND ($extension=="csv")) {

		//get the csv file
		$handle = fopen($file,"r");
		
		//loop through the csv file and insert into database
		do {
			if ($data[0]) {
				$addname=$myFilter->process($data[0]);
				$addemail=$myFilter->process($data[1]);
				$sql = "INSERT INTO recipients SET listid='$thislistid', name='$addname', email='$addemail', deleted='0'";
				mysqli_query($link, $sql);
			}
		} while ($data = fgetcsv($handle,1000,",","'"));
		//redirect
		header("location:list.php?biginsightsid=".$thislistid);
	} else {
		$note="wrong file data";
	}
}

// switch public->private and vice versa
if ((isset($_GET['switch'])) && (($thislistcreatedby==$myid) xor ($myadmin=='1'))) {
    // get status
    $resultswitch=mysqli_query($link, "select * from `lists` WHERE id='$thislistid' LIMIT 1");
    while ($rowswitch=mysqli_fetch_array($resultswitch)){
        $switchpublic=$rowswitch['public'];
    }
    // switch status
    if ($switchpublic==1) {
        $newstatus=0;
    } else {
        $newstatus=1;
    }
    // insert new status to db
    $sql = "UPDATE lists SET public='$newstatus' WHERE id='$thislistid'";
    mysqli_query($link, $sql);
    header("location:list.php?biginsightsid=".$thislistid);
}
?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-list icon-large"></i> <? echo $thislistname; ?> <span><? echo $thislistpublictext; ?></span></h3>
						<? if (($thislistcreatedby==$myid) xor ($myadmin=='1')) { ?>
						<div class="pull-right">
							<a href="list.php?biginsightsid=<? echo $thislistid; ?>&switch"><button type="button" class="btn btn-info"><i class="icon-<? echo $thislistswitchicon; ?>"></i> <? echo $thislistswitchtext; ?></button></a>
						</div>
						<? } ?>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->
				
				
				
				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<!-- table and form starts -->
						<div class="col-md-12">
							<? if (isset ($note)) { ?>
							<div class="alert alert-dismissable alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="icon-warning-sign"></i> <? echo $note; ?>
							</div>
							<? } ?>
						<!-- form -->
						<div class="col-md-6">
							<div class="page-content page-form">
							
							<div class="widget">
								<div class="widget-head">
									<h5><i class="icon-plus green"></i> Add Recipient</h5>
								</div>
								   <div class="widget-body">
									  <form class="form-horizontal" id="ValidForm" role="form" method="post" action="list.php?biginsightsid=<? echo $thislistid; ?>">
									  
										<div class="form-group">
										  <label class="col-lg-2 control-label">Name</label>
										  <div class="col-lg-10">
											<input type="text" name="name" class="form-control" placeholder="Name">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Email</label>
										  <div class="col-lg-10">
											<input type="text" name="email" class="form-control" placeholder="Email">
										  </div>
										</div>
															
										<div class="form-group">
										  <div class="col-lg-offset-2 col-lg-10">
											<button type="submit" class="btn btn-primary" name="addrecipient">Add</button>
										  </div>
										</div>
									  </form>
								   </div>
								   
								   <div class="widget-foot">
								   </div>
								   <br>
								   <!-- Upload from CSV -->
									<div class="well">
										<h5><i class="icon-upload"></i> Upload Recipients List</h5>
										<p>Upload list from file - Only CSV files accepted</p>
										<form class="form-inline" role="form" method="post" action="list.php?biginsightsid=<? echo $thislistid; ?>" enctype="multipart/form-data">
										  <div class="form-group">
											<input type="file" name="uploaded_file" id="file" required />
										  </div><br><br>
										  <div class="form-group">
											<button type="submit" class="btn btn-success" name="addcsv"><i class="icon-upload-alt"></i> Upload</button>
										  </div>
										</form>
									</div>
								</div>
							
							</div>
						</div>
						<!-- end form -->
						<!-- table -->
						<div class="col-md-6">
						
						<div class="widget">
						<div class="page-content page-tables">
							<div class="widget-head br-green">
								<h5><i class="icon-user green"></i> Recipients List</h5>
							</div>
							
							<div class="widget-body">
								<div class="row">
									<div class="col-md-12">
										<div class="table-responsive">
											<table cellpadding="0" cellspacing="0" border="0" id="data-table" width="100%">
												<thead>
													<tr>
														<th>Name</th>
														<th>Email</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
												<?
												// get users list
												$resultusers=mysqli_query($link, "select * from `recipients` WHERE listid='$thislistid' AND deleted='0' ORDER BY id DESC");
												while ($rowusers=mysqli_fetch_array($resultusers)){
													$id=$rowusers['id'];
													$name=$rowusers['name'];
													$email=$rowusers['email'];
												?>
													<tr>
														<td><? echo $name; ?></td>
														<td><? echo $email;?></td>
														<td><a href="list.php?biginsightsid=<? echo $thislistid; ?>&delete=<? echo $id; ?>" class="bs-tooltip" title="Permenantly Delete From List" data-placement="bottom"><i class="icon-trash"></i></a></td>
													</tr>
												<? } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							</div>
							
							<div class="widget-foot">
							
							</div>
						
							</div>
						</div>
						<!-- end table -->
						</div>
						<!-- table and form ends -->
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      <? include_once "inc/foot.php"; ?>
      
	</body>	
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>