<?php

include_once "inc/head.php";
require_once("../includes/inc_files.php");
$page_title = "My Insights"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="Dashboard"; ?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-laptop icon-large"></i> Dashboard </h3> 	
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->
				
				
				<!-- Start info box -->
				<div class="row top-summary">
					<div class="col-lg-3 col-md-6">
						<div class="widget green-1 animated fadeInDown">
							<div class="widget-content padding">
								
								<div class="text-box">
									<p class="maindata"><b>SURVEYS</b></p>
									<h2><span class="animate-number" data-value="25153" data-duration="3000"><? echo $totalforms; ?></span></h2>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="widget-footer">
								<div class="row">
									<div class="col-sm-12">
										<i class="fa fa-caret-up rel-change"></i> <b><? echo $totalforms; ?></b> total surveys
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6">
						<div class="widget darkblue-2 animated fadeInDown">
							<div class="widget-content padding">
								
								<div class="text-box">
									<p class="maindata"><b>RESPONSES</b></p>
									<h2><span class="animate-number" data-value="6399" data-duration="3000"><? echo $totalsubmissions; ?></span></h2>

									<div class="clearfix"></div>
								</div>
							</div>
							<div class="widget-footer">
								<div class="row">
									<div class="col-sm-12">
										<i class="fa fa-caret-down rel-change"></i> <b><? echo $totalsubmissions; ?></b> total responses
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6">
						<div class="widget pink-1 animated fadeInDown">
							<div class="widget-content padding">
								
								<div class="text-box">
									<p class="maindata">OVERALL <b>RECIPIENTS</b></p>
									<h2><span class="animate-number" data-value="70389" data-duration="3000"><? echo $totalrecipients; ?></span></h2>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="widget-footer">
								<div class="row">
									<div class="col-sm-12">
										<i class="fa fa-caret-down rel-change"></i> <b><? echo $totalrecipients; ?></b> total recipients
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<div class="col-lg-3 col-md-6">
						<div class="widget lightblue-1 animated fadeInDown">
							<div class="widget-content padding">
								
								<div class="text-box">
									<p class="maindata"><b>LISTS</b></p>
									<h2><span class="animate-number" data-value="18648" data-duration="3000"><? echo $totallists; ?></span></h2>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="widget-footer">
								<div class="row">
									<div class="col-sm-12">
										<i class="fa fa-caret-up rel-change"></i> <b><? echo $totallists; ?></b> total lists
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

				</div>
				<!-- End of info box -->
				

				<!-- Content starts -->
				
					<div class="container">
						
								<!-- system report -->
								<!-- forms -->
								<div class="widget contacts-widget">

									<!-- Widget head -->
									<div class="widget-head">
										<h5 class="pull-left"><i class="icon-archive"></i> Resources</h5>	
										<div class="widget-head-btns pull-right">
											<a href="#" class="wclose"><i class="icon-remove"></i></a>
										</div>
										<div class="clearfix"></div>
									</div>

									<!-- Widget body -->
									<div class="widget-body 300-scroll">

										<ul class="list-unstyled">
										
											<!-- number of forms -->
											<?
											if ($myadmin==0) { // if i am not admin
												// get list of forms that are public or I have created
												//$resultforms=mysqli_query($link, "select * from `forms` where createdby='$myid' OR public='1' ORDER BY id DESC");
												$resultforms=mysqli_query($link, "select * from `forms` where createdby='$myid' ORDER BY id DESC");
											} else { // if i am admin
												// get all forms
												$resultforms=mysqli_query($link, "select * from `forms` where createdby='$myid' ORDER BY id DESC");
											}
											$thisformscount=mysqli_num_rows($resultforms);
											?>
											<li class="contact-alpha">
												Web Surveys <span class="label label-info pull-right"><? echo $thisformscount; ?></span>
												<div class="clearfix"></div>
											</li>
											<? 
											
											while ($rowforms=mysqli_fetch_array($resultforms)){
												$thisformid=$rowforms['id'];
												$thisformgenkey = $rowforms['servekey'];
												$thisformname=$rowforms['name'];
												$thisformcreated=$rowforms['created'];
												$thisformclosed=$rowforms['closed'];
												$thisformpublic=$rowforms['public'];
												
												if ($thisformclosed!="0000-00-00") { // if is closed
													$createdtext="Created: ".$thisformcreated.", Closed: ".$thisformclosed;
												} else {
													$createdtext="Created: ".$thisformcreated;
												}
												
												if ($thisformpublic=="1") { // if is public
													$icon="eye-open";
													$color="green";
													$tooltip="Public";
												} else {
													$icon="eye-close";
													$color="red";
													$tooltip="Private";
												}
											?>

												
											<!-- Single form -->
											<li class="c-list">
												<!-- Contact pic -->
												<div class="contact-pic">
													<i class="icon-<? echo $icon." ".$color; ?> bs-tooltip" title="<? echo $tooltip; ?>" data-placement="top"></i>
												</div>
												<!-- Contact details -->
												<div class="contact-details">
													<!-- Contact name and number -->
													<div class="pull-left">
														<strong><a href="data.php?biginsightsid=<? echo $thisformgenkey; ?>"><? echo $thisformname; ?></a></strong>
														<small><? echo $createdtext; ?></small>
													</div>
													<!-- Call, Message and Email buttons -->
													<div class="pull-right">
														<a href="elements.php?biginsightsid=<? echo $thisformgenkey; ?>" class="btn btn-primary btn-xs bs-tooltip" title="Edit Form"><i class="icon-cogs"></i></a>
														<a href="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>" class="btn btn-success btn-xs bs-tooltip" title="Settings"><i class="icon-cog"></i></a>
														<a href="invitations.php?biginsightsid=<? echo $thisformgenkey; ?>" class="btn btn-info btn-xs bs-tooltip" title="Invitations"><i class="icon-envelope"></i></a>
														<a href="data.php?biginsightsid=<? echo $thisformgenkey; ?>" class="btn btn-warning btn-xs bs-tooltip" title="Submissions & Statistics"><i class="icon-bar-chart"></i></a>
													</div>
													<div class="clearfix"></div>
												</div>
											</li>
											<!-- end single form -->
										<? } ?>
										</ul>
										
										<!-- lists -->
										<ul class="list-unstyled">
										
											<!-- number of lists -->
											<?
											if ($myadmin==0) { // if i am not admin
												// get list of forms that are public or I have created
												$resultlists=mysqli_query($link, "select * from `lists` where createdby='$myid' OR public='1' ORDER BY id DESC");
											} else { // if i am admin
												// get all lists
												$resultlists=mysqli_query($link, "select * from `lists` where createdby='$myid' OR public='1' ORDER BY id DESC");
											}
											$thislistscount=mysqli_num_rows($resultlists);
											?>
											<li class="contact-alpha">
												All Lists <span class="label label-info pull-right"><? echo $thislistscount; ?></span>
												<div class="clearfix"></div>
											</li>
											<? 
											while ($rowlists=mysqli_fetch_array($resultlists)){
												$thislistid=$rowlists['id'];
												$thislistname=$rowlists['name'];
												$thislistcreated=$rowlists['created'];
												$thislistpublic=$rowlists['public'];
												
												if ($thislistpublic=="1") { // if is public
													$icon="eye-open";
													$color="green";
													$tooltip="Public";
												} else {
													$icon="eye-close";
													$color="red";
													$tooltip="Private";
												}
											?>
											<!-- Single list -->
											<li class="c-list">
												<!-- Contact pic -->
												<div class="contact-pic">
													<i class="icon-<? echo $icon." ".$color; ?> bs-tooltip" title="<? echo $tooltip; ?>" data-placement="top"></i>
												</div>
												<!-- Contact details -->
												<div class="contact-details">
													<!-- Contact name and number -->
													<div class="pull-left">
														<strong><? echo $thislistname; ?></strong>
														<small>Created: <? echo $thislistcreated; ?></small>
													</div>
													<!-- Call, Message and Email buttons -->
													<div class="pull-right">
														<a href="list.php?id=<? echo $thislistid; ?>" class="btn btn-warning btn-xs bs-tooltip" title="Manage"><i class="icon-cog"></i></a>
													</div>
													<div class="clearfix"></div>
												</div>
											</li>
											<!-- end single list -->
										<? } ?>
										</ul>
										<!-- end lists -->
										
									</div>

									<!-- Widget foot -->
									<div class="widget-foot">
									</div>

								</div>
						
						
						
						
						
						<div class="page-content">
							<div class="col-md-12">
								<!-- resources report -->
								<div class="report-block">
									<div class="container">
									
									
									
									<div class="col-md-6">
							
			
									

	 </div>
									
									</div>
								</div>
							</div>
						</div>
							
							</div>
						</div>
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      


	</body>	
</html>


<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>
