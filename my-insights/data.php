<?php
include_once "inc/headstats.php";
require_once("../includes/inc_files.php");
$pagetitle="Submissions and Statistics";
$page_title = "Forms"; require_once("../includes/themes/".THEME_NAME."/qheader.php");
include_once "inc/getforminfo.php";
?>
<?php 
// get total submissions by today, yesterday, this week, this month, 6 months, this year
//today
    $resultstoday=mysqli_query($link, "select * from `submissions` WHERE formid='$thisformgenkey' AND date>DATE_SUB(NOW(),INTERVAL 1 DAY)");
    $totaltoday=mysqli_num_rows($resultstoday);
//yesterday
    $resultsyesterday=mysqli_query($link, "select * from `submissions` WHERE formid='$thisformgenkey' AND date>DATE_SUB(NOW(),INTERVAL 2 DAY)");
    $total2days=mysqli_num_rows($resultsyesterday);
	$totalyesterday=$total2days-$totaltoday;
//this week
    $resultsweek=mysqli_query($link, "select * from `submissions` WHERE formid='$thisformgenkey' AND date>DATE_SUB(NOW(),INTERVAL 1 WEEK)");
    $totalthisweek=mysqli_num_rows($resultsweek);
//this month
    $resultsmonth=mysqli_query($link, "select * from `submissions` WHERE formid='$thisformgenkey' AND date>DATE_SUB(NOW(),INTERVAL 1 MONTH)");
    $totalthismonth=mysqli_num_rows($resultsmonth);
//last six months
    $results6months=mysqli_query($link, "select * from `submissions` WHERE formid='$thisformgenkey' AND date>DATE_SUB(NOW(),INTERVAL 6 MONTH)");
    $total6months=mysqli_num_rows($results6months);
//this year
    $resultsyear=mysqli_query($link, "select * from `submissions` WHERE formid='$thisformgenkey' AND date>DATE_SUB(NOW(),INTERVAL 12 MONTH)");
    $totalthisyear=mysqli_num_rows($resultsyear);	
	?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
     
            <div class="mainbar">
	
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-bar-chart icon-large"></i> Responses & Statistics <span><? echo $thisformname; ?></span></h3> 	
						<div class="pull-right">
						
							<a href="elements.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-cogs"></i> Edit Survey</button></a>
							<a href="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-cog"></i> Settings</button></a>
							<a target="_blank" href="viewform.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-external-link"></i> Preview Survey</button></a>
							
							<ul class="share-buttons">
						<li><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>&t=" title="Share on Facebook" target="_blank"><img src="img/si/Facebook.png"></a></li>
  <li><a href="https://twitter.com/intent/tweet?source=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>&text=:%20http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>" target="_blank" title="Tweet"><img src="img/si/Twitter.png"></a></li>
  <li><a href="https://plus.google.com/share?url=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>" target="_blank" title="Share on Google+"><img src="img/si/Google+.png"></a></li>
  <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>&title=&summary=&source=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>" target="_blank" title="Share on LinkedIn"><img src="img/si/LinkedIn.png"></a></li>
						</ul>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="page-head hidestats">
					<div class="container">
						<div class="row">
							<div class="colquick ">
								
								<div class="col-md-6 col-sm-0 hidden-sm hidden-xs">
								<span class="ph-status">
								<div class="survstats">
                                   <h2><i class="icon-bar-chart"></i> Quick Stats</h2>
									<div class="sresponses"><a href="#">Responses  &nbsp;<b><? echo $thisformsubtotal; ?></b> &nbsp; </a></div>
									<div class="sinv"><a href="#">Invitations  &nbsp;<b><? echo $thisforminvtotal; ?></b> &nbsp; </a></div>
									<div class="srate"><a href="#">Response Rate  &nbsp;<b><? echo $subinvpercent; ?> </b></a></div>
									
									<a href="csv.php?biginsightsid=<? echo $thisformgenkey; ?>" class="bs-tooltip" title="Export To CSV" data-placement="bottom"><i class="icon-upload-alt"></i> </a>
								
							    </div>
								</span>
							</div>
							</div>
							
							
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				
				
				
			
				
				<div class="container">
					<div class="page-content">
					
					
					
					
					
<div class="col-md-12">		
<div class="row">			
					<!--Map Starts-->
<div class="col-md-8">
<div class="widget">
<div class="widget-head" style="background:#e36159;">
<h5><i class="icon-bar-chart"></i>RESPONSES MAP- <? echo $thisformname; ?></h5>
  </div>
<div>
    <div id="leafmap" style="width: 100%; height: 400px"></div>
	
    <script
        src="js/leaflet.js">
    </script>
<?php 
	$mapresponses=mysqli_query($link, "select * from `submissions` WHERE formid='$thisformgenkey'");
	 $totalmapresponses=mysqli_num_rows($mapresponses);
	$mapelements = array();
	while( $mapresults = mysqli_fetch_array( $mapresponses))
	{
		array_push($mapelements, $mapresults);
	}
	?>
	
    <script>
	var planes = [
		<?php 
			for($i=0; $i<$totalmapresponses; $i++){
			$maplat=$mapelements[$i]['latitude'];
			$maplong=$mapelements[$i]['longitude'];
			$mapcity=$mapelements[$i]['city']; ?>
							
		<?php echo '['.'"'.$mapcity.'"'.','.$maplat.','.$maplong.']';
		if(($i+1)!=$totalmapresponses)
		{
		echo ",";}
		?>	
		
		 <?php } ?>
		];
		
	    var map = L.map('leafmap').setView([<?php echo $mapelements[($totalmapresponses-1)]['latitude'].", ".$mapelements[($totalmapresponses-1)]['longitude'];?>], 8);
        //var map = L.map('leafmap').setView([-41.3058, 174.82082], 8);
        mapLink = 
            '<a href="http://openstreetmap.org"></a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: ',' + mapLink,
            maxZoom: 18,
            }).addTo(map);
		for (var i = 0; i < planes.length; i++) {
			marker = new L.marker([planes[i][1],planes[i][2]])
				.bindPopup(planes[i][0])
				.addTo(map);
		}
               
    </script>
	</div>
	</div>
	</div>
	<!--Map Ends-->
	


<!-- times stats -->
				<div class="pcstats">
				
				<div class="col-md-4">
				<div class="widget">
				<div class="widget-head">
                   <h5><i class="icon-bar-chart">
				   </i>Responses by time for <? echo $thisformname; ?></h5>
                                          </div>
				<div class="statsdash">
								<div class="col-md-6 col-lg-12 col-xl-6">
							<div class="row">
								<div class="col-md-12 col-lg-6 col-xl-6 ">
									<section class="stats panel-featured-left statpanel-featured-primary">
										<div class="panel-body">
											<div class="stat-summary">
											
												<div class="stat-summary-col">
													<div class="summary">
														<h4 class="stattitle">Today </h4>
														<div class="summary-footer">
													<div class="stat-icon statbg-primary">
														<i class=""><strong class="amount"><?php echo $totaltoday; ?></strong></i>
													</div>
													</div>
														
													</div>
													
												</div>
											</div>
										</div>
									</section>
								</div>
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="stats panel-featured-left statpanel-featured-secondary">
										<div class="panel-body">
											<div class="stat-summary">
												<div class="stat-summary-col">
													<div class="summary">
														<h4 class="stattitle">Yesterday</strong></h4>
														<div class="summary-footer">
														<div class="stat-icon statbg-secondary">
														 <strong class="amount"><?php echo $totalyesterday;?>
													</div>
													</div>
														
													</div>
													
												</div>
											</div>
										</div>
									</section>
								</div>
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="stats panel-featured-left statpanel-featured-tertiary">
										<div class="panel-body">
											<div class="stat-summary">
												<div class="stat-summary-col">
													<div class="summary">
														<h4 class="stattitle">This Week </h4>
														<div class="summary-footer">
														<div class="stat-icon statbg-tertiary">
														<strong class="amount"><?php echo $totalthisweek;?></strong>
													</div>
													</div>
														
													</div>
													
												</div>
											</div>
										</div>
									</section>
								</div>
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="stats panel-featured-left statpanel-featured-quartenary">
										<div class="panel-body">
											<div class="stat-summary">
												<div class="stat-summary-col">
													<div class="summary">
														<h4 class="stattitle">This Month </h4>
														<div class="summary-footer">
														<div class="stat-icon statbg-quartenary">
														<strong class="amount"><?php echo $totalthismonth;?></strong>
													</div>
													</div>
														
													</div>
													
												</div>
											</div>
										</div>
									</section>
								</div>
								
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="stats panel-featured-left statpanel-featured-tertiary">
										<div class="panel-body">
											<div class="stat-summary">
												<div class="stat-summary-col">
													<div class="summary">
														<h4 class="stattitle">Last 6 months </h4>
														<div class="summary-footer">
														<div class="stat-icon statbg-tertiary">
														<strong class="amount"><?php echo $total6months;?></strong>
													</div>
													</div>
														
													</div>
													
												</div>
											</div>
										</div>
									</section>
								</div>
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="stats panel-featured-left statpanel-featured-quartenary">
										<div class="panel-body">
											<div class="stat-summary">
												
												<div class="stat-summary-col">
													<div class="summary">
														<h4 class="stattitle">This Year </h4>
														<div class="summary-footer">
														<div class="stat-icon statbg-quartenary">
														<strong class="amount"><?php echo $totalthisyear?></strong>
													</div>
													</div>
														
													</div>
													
												</div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</div>
						</div>
						</div>
						</div>
</div>
</div>						<!-- timestats ends -->	


<!-- times stats -->   			
								<div class="row">	
								<div class="mobilestats">
								<div class="col-md-6">
										
                                       <div class="widget">
                                          <div class="widget-head">
                                             <h5><i class="icon-bar-chart"></i>Responses by time for <? echo $thisformname; ?></h5>
                                          </div>
                                          <div class="insightsjs">
										  <p>
										    <?php
											echo $totaltoday."&nbsp;&nbsp;Today";?> <br>
											<?php echo $totalyesterday."&nbsp;&nbspYesterday";?><br>
											<?php echo $totalthisweek."&nbsp;&nbspThis week"; ?><br>	
											<?php echo $totalthismonth."&nbsp;&nbspThis month";?><br>
											<?php echo $total6months."&nbsp;&nbspLast 6 months";?><br>	
											<?php echo $totalthisyear."&nbsp;&nbspThis year"; ?><br>
											</p>

                                          </div>
                                       </div>
                                      
								</div>
								</div>
								</div>
								 <!-- timestats ends -->
</div>


		
					
					
					
						<div class="col-md-12">
						
						<?
						// get elements that are toggle, checkbox, radio and dropdown
						$resultelements=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0' ORDER BY serial ASC");
						while ($rowelements=mysqli_fetch_array($resultelements)){
							$elementid=$rowelements['id'];
							$elementlabel=$rowelements['label'];
							$elementtype=$rowelements['type'];
							
							if  (($elementtype=="radio") xor ($elementtype=="dropdown") xor ($elementtype=="toggle")) {
						?>
						
							<div class="row">
								<div class="col-md-6">
										<!-- Pie chart starts -->
                                       <div class="widget">
                                          <div class="widget-head">
                                             <h5><i class="icon-bar-chart"></i> <? echo $elementlabel; ?></h5>
                                          </div>
                                          <div class="widget-body">
											<div class="chart-container">
                                             <div id="a<? echo $elementid; ?>a" class="chart-placeholder"></div>
											</div>

                                          </div>
                                       </div>
                                       <!-- Pie chart ends -->
								</div>
								<div class="col-md-6">
									   <!-- Bar charts -->
                                       
                                       <div class="widget">
                                          <div class="widget-head">
                                             <h5><i class="icon-bar-chart"></i> <? echo $elementlabel; ?></h5>
                                          </div>
                                          <div class="widget-body">
                                             <div class="chart-container">
                                                <div id="b<? echo $elementid; ?>b" class="chart-placeholder"></div>
                                             </div>
                                          </div>
                                       </div>
								</div>
							</div>
							<!-- end single element charts -->
							<? } // end if
							} // end while ?>
							
							
						
							
							<!-- table -->						
						<div class="widget">
						<div class="page-content page-tables">
							<div class="widget-head br-green">
								<h5><i class="icon-double-angle-down green"></i> All Responses -  <strong><? echo $thisformsubtotal; ?></strong></h5>
							</div>
							<?php
    include('bi/xcrud.php');
    $xcrud = Xcrud::get_instance();
    $xcrud->table('submissions');
	$xcrud->join('formid','subfields','formid'); // join users and profiles on users.id = profiles.user_id
	$xcrud->join('subfields.elementid','elements','id'); // on profile.token_id = tokens.id
	$xcrud->columns('formid,invitationid,date,latitude,longitude,started,ended,browserversion,subfields.elementid,subfields.submissionid, elements.name,elements.formid,elements.subof,elements.placeholder,elements.type,elements.url,elements.email,elements.number,elements.digits,elements.creditcard,elements.minlength,elements.maxlength,elements.min,elements.max,elements.dateformat,elements.timeformat,elements.extensions,elements.onvalue,elements.offvalue,elements.required,elements.serial,elements.deleted', true);
	$xcrud->where('formid =', $thisformgenkey);
	$xcrud->label('os','Operating System');
	//$xcrud->emails_label('Contact email');
	$xcrud->button('submission.php?biginsightsid='.$thisformgenkey.'&submission={id}','My Title','icon-link','',array('target'=>'_blank'));
	$xcrud->unset_add();
	$xcrud->unset_edit();
	$xcrud->unset_view();
	$xcrud->unset_remove();
	$xcrud->table_name('Responses');
	//$xcrud->column_pattern('first_name','My name is {value}');
	//$xcrud->change_type('date_created','timestamp');
	
?>
							<div class="widget-body">
								<div class="row">
									<div class="col-md-12">
										<div class="table-responsive">
											<?php echo $xcrud->render(); ?>
										</div>
									</div>
								</div>
							</div>
							</div>
							
							<div class="widget-foot">
							
							</div>
						
							</div>
						<!-- end table -->
						</div>
						<!-- table and form ends -->
						
						
						
						<div class="row">
						<div class="col-md-6">
										<!-- javascript generator starts -->
                                       <div class="widget">
                                          <div class="widget-head">
                                             <h5><i class="icon-bar-chart"></i>JavaScript Code for <? echo $thisformname; ?></h5>
                                          </div>
                                          <div class="insightsjs">
										    <? 
											$jsurl = WWW;
											$thisjs="&lt;!--Big Insights--&gt;&lt;script src='$jsurl/ext/bss/insightscs.js'>&lt;/script&gt; 
		&lt;script&gt; BI(init);insightkey('$thisformkey'); &lt;/script&gt;&lt;!--Big Insights--&gt;";
											  echo $thisjs; ?>

                                          </div>
                                       </div>
                                       <!-- javascript generator ends -->
								</div>
								<div>
								
						
					</div>
				</div>
				
				
				
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      <? include_once "inc/foot.php"; ?>
      
    
      
      <script type="text/javascript">
     
         /* Pie chart starts */

         $(function () {

            <?
			// get elements that are toggle, radio and dropdown
			$resultelements=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0' ORDER BY serial ASC");
			while ($rowelements=mysqli_fetch_array($resultelements)){
				$elementid=$rowelements['id'];
				$elementlabel=$rowelements['label'];
				$elementtype=$rowelements['type'];
				$elementonvalue=$rowelements['onvalue'];
				$elementoffvalue=$rowelements['offvalue'];
							
				if  (($elementtype=="radio") xor ($elementtype=="dropdown") xor ($elementtype=="toggle")) {
			?>
			data<? echo $elementid; ?> = [
				<?
				if ($elementtype!="toggle") {
				// get subelements list
				$resultsubelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='$elementid' AND deleted='0' ORDER BY serial ASC");
				while ($rowsubelement=mysqli_fetch_array($resultsubelement)){
					$subelementid=$rowsubelement['id'];
					$subelementlabel=$rowsubelement['label'];
					
					$resultcount=mysqli_query($link, "select * from `subfields` WHERE formid='$thisformgenkey' AND elementid='$elementid' AND value='$subelementid'");
					$thiscount=mysqli_num_rows($resultcount);
				?>
				{label: "<? echo $subelementlabel; ?>",  data: <? echo $thiscount; ?>},
				<? } // end while 
				}  else { // if it is toggle
					$resultcount=mysqli_query($link, "select * from `subfields` WHERE formid='$thisformgenkey' AND elementid='$elementid' AND value='on'");
					$thiscount=mysqli_num_rows($resultcount);
					$resultcountoff=mysqli_query($link, "select * from `subfields` WHERE formid='$thisformgenkey' AND elementid='$elementid' AND value=''");
					$thiscountoff=mysqli_num_rows($resultcountoff);
				?>
				{label: "<? echo $elementonvalue; ?>",  data: <? echo $thiscount; ?>},
				{label: "<? echo $elementoffvalue; ?>",  data: <? echo $thiscountoff; ?>},
				<?
				}
				?>
			];

             $.plot($("#a<? echo $elementid; ?>a"), data<? echo $elementid; ?>,
             {
                series: {
					pie: {
						show: true,
						radius: 1,
						label: {
							show: true,
							radius: 3/4,
							formatter: function(label, series){
								return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
							},
							background: {
								opacity: 0.5,
								color: '#000'
							}
						}
					},
					legend: {
						show: false
					}
				}
             });
			 <? } // end if ?>
			 <? } // end while ?>

			<?
			// get elements that are toggle, radio and dropdown
			$resultelements=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0' ORDER BY serial ASC");
			while ($rowelements=mysqli_fetch_array($resultelements)){
				$elementid=$rowelements['id'];
				$elementlabel=$rowelements['label'];
				$elementtype=$rowelements['type'];
							
				if  (($elementtype=="radio") xor ($elementtype=="dropdown") xor ($elementtype=="toggle")) {
			?>
			data<? echo $elementid; ?>2 = [
				<?
				if ($elementtype!="toggle") {
				// get subelements list
				 $serial=1;
				$resultsubelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='$elementid' AND deleted='0' ORDER BY serial ASC");
				while ($rowsubelement=mysqli_fetch_array($resultsubelement)){
					$subelementid=$rowsubelement['id'];
					$subelementlabel=$rowsubelement['label'];
					
					$resultcount=mysqli_query($link, "select * from `subfields` WHERE formid='$thisformgenkey' AND elementid='$elementid' AND value='$subelementid'");
					$thiscount=mysqli_num_rows($resultcount);
				?>
				{label: "<? echo $subelementlabel; ?>",  data: [[<? echo $serial; ?>,<? echo $thiscount; ?>]]},
				<? 
				$serial++;
				} // end while
				} else { // if it is toggle 
					$resultcount=mysqli_query($link, "select * from `subfields` WHERE formid='$thisformgenkey' AND elementid='$elementid' AND value='on'");
					$thiscount=mysqli_num_rows($resultcount);
					$resultcountoff=mysqli_query($link, "select * from `subfields` WHERE formid='$thisformgenkey' AND elementid='$elementid' AND value=''");
					$thiscountoff=mysqli_num_rows($resultcountoff);
				?>
				{label: "<? echo $elementonvalue; ?>",  data: [[1,<? echo $thiscount; ?>]]},
				{label: "<? echo $elementoffvalue; ?>",  data: [[2,<? echo $thiscountoff; ?>]]},
				<? } // end if ?>
			];

             $.plot($("#b<? echo $elementid; ?>b"), data<? echo $elementid; ?>2,
             {
                series: {
					stack: 0,
					bars: {
						show: true,
						barWidth: 0.9,
						fill:1
					}
				}
             });
			 <? } // end if
			 } // end while ?>

         });
		 
      </script>
      
	</body>	
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>
