<?php 
include_once "inc/head.php";
require_once("../includes/inc_files.php");
$page_title = "Lists"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="Lists";

// create a new list
if (isset($_POST['createlist'])) {

	include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
	$_POST= str_replace("'", "''", $_POST);
	$_POST= str_replace('"', '\"', $_POST);
		
    $listname=$_POST['listname'];
	$public=$_POST['public'];
	
	$sql = "INSERT INTO `lists` SET name='$listname', public='$public', createdby='$myid', created=NOW()";
	mysqli_query($link, $sql) or die('Error, query failed');
	header("location:lists.php");
}
?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-list icon-large"></i> Lists </h3> 
						<div class="pull-right">
							<a href="#myModal" class="btn btn-info" data-toggle="modal"> <i class="icon-plus"></i> New List</a>
						</div>						
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->
				
				
				
				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<div class="col-md-12">
								<!-- forms -->
								<div class="widget contacts-widget">

									<!-- Widget head -->
									<div class="widget-head">
										<h5 class="pull-left"><i class="icon-list"></i> Lists</h5>	
										<div class="widget-head-btns pull-right">
											<a href="#" class="wclose"><i class="icon-remove"></i></a>
										</div>
										<div class="clearfix"></div>
									</div>

									<!-- Widget body -->
									<div class="widget-body 300-scroll">

										<ul class="list-unstyled">
										
											<!-- number of lists -->
											<?
											if ($myadmin==0) { // if i am not admin
												// get list of forms that are public or I have created
												$resultlists=mysqli_query($link, "select * from `lists` where createdby='$myid' OR public='1' ORDER BY id DESC");
											} else { // if i am admin
												// get all lists
												$resultlists=mysqli_query($link, "select * from `lists` ORDER BY id DESC");
											}
											$thislistscount=mysqli_num_rows($resultlists);
											?>
											<li class="contact-alpha">
												All Lists <span class="label label-info pull-right"><? echo $thislistscount; ?> total</span>
												<div class="clearfix"></div>
											</li>
											<? 
											while ($rowlists=mysqli_fetch_array($resultlists)){
												$thislistid=$rowlists['id'];
												$thislistname=$rowlists['name'];
												$thislistcreated=$rowlists['created'];
												$thislistpublic=$rowlists['public'];
												
												if ($thislistpublic=="1") { // if is public
													$icon="eye-open";
													$color="green";
													$tooltip="Public";
												} else {
													$icon="eye-close";
													$color="red";
													$tooltip="Private";
												}
											?>
											<!-- Single list -->
											<li class="c-list">
												<!-- Contact pic -->
												<div class="contact-pic">
													<i class="icon-<? echo $icon." ".$color; ?> bs-tooltip" title="<? echo $tooltip; ?>" data-placement="top"></i>
												</div>
												<!-- Contact details -->
												<div class="contact-details">
													<!-- Contact name and number -->
													<div class="pull-left">
														<a href="list.php?biginsightsid=<? echo $thislistid; ?>"><strong><? echo $thislistname; ?></strong></a>
														<small>Created: <? echo $thislistcreated; ?></small>
													</div>
													<!-- Call, Message and Email buttons -->
													<div class="pull-right">
														<a href="list.php?biginsightsid=<? echo $thislistid; ?>" class="btn btn-warning btn-xs bs-tooltip" title="Manage"><i class="icon-cog"></i></a>
													</div>
													<div class="clearfix"></div>
												</div>
											</li>
											<!-- end single list -->
										<? } ?>
										</ul>
										
									</div>

									<!-- Widget foot -->
									<div class="widget-foot">
									</div>

								</div>
						
							</div>
						</div>
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      <? include_once "inc/foot.php"; ?>

									<!-- Modal -->
									<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
										   <div class="modal-content">
											   <div class="modal-header">
												 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
												 <h4 class="modal-title">Create a New List</h4>
											   </div>
											   <div class="modal-body">
												 <form class="form-horizontal" id="ValidForm" role="form" method="post" action="lists.php">
									  
													<div class="form-group">
													  <label class="col-lg-2 control-label">List Name</label>
													  <div class="col-lg-10">
														<input type="text" name="listname" class="form-control" placeholder="Descriptive Name">
													  </div>
													</div>
													
													<div class="form-group">
													  <label class="col-lg-2 control-label">Type</label>
													  <div class="col-lg-10">
														<select class="form-control" name="public">
														  <option value="0">Private</option>
														  <option value="1">Public</option>
														</select>
													  </div>
													</div>
													<hr>
													<div class="form-group">
													  <div class="col-lg-offset-2 col-lg-10">
														<button type="submit" class="btn btn-primary pull-right" name="createlist">Create List</button>
													  </div>
													</div>
												  </form>
											   </div>
											</div>
										</div>
									</div>

	</body>	
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>