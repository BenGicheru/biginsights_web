<?php 
include_once "inc/head.php";
require_once("../includes/inc_files.php");
$page_title = "Submission"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="Submission";
include_once "inc/getforminfo.php";

	// get submission id
	if ((isset($_GET['submission'])) && (is_numeric($_GET['submission']))) {
    $thissubid=$_GET['submission'];
    $resultsub=mysqli_query($link, "select * from `submissions` WHERE id='$thissubid' AND formid='$thisformgenkey' LIMIT 1");
	} else {
		header("location:dashboard.php");
	}
	
	// make sure there's actually such a submission
	if (mysqli_num_rows($resultsub)=='0'){
		header("location:dashboard.php");
	}
?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-list icon-large"></i> <? echo $thisformname; ?> <span>submission #<? echo $thissubid; ?></span></h3> 	
						<div class="pull-right">
							<a href="data.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-angle-left"></i> Back To Submissions</button></a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->
				
				
				
				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<!-- table and form starts -->
						<div class="col-md-12">
							<!--table-->
									<table class="table table-bordered">
									<?
									$resultsubmission=mysqli_query($link, "select * from `subfields` WHERE submissionid='$thissubid' ORDER BY id ASC");
									while ($rowsub=mysqli_fetch_array($resultsubmission)){
										$fieldid=$rowsub['id'];
										$elementid=$rowsub['elementid'];
										$value=$rowsub['value'];
										
										// get element info
										$resultelement=mysqli_query($link, "select * from `elements` WHERE id='$elementid' LIMIT 1");
										while ($rowelement=mysqli_fetch_array($resultelement)){
											$elementlabel=$rowelement['label'];
											$elementtype=$rowelement['type'];
											$elementonvalue=$rowelement['onvalue'];
											$elementoffvalue=$rowelement['offvalue'];
										}
										
										// if type is toggle, get on value and off value
										if ($elementtype=="toggle") {
											if ($value=="on") {
												$value=$elementonvalue;
											} else {
												$value=$elementoffvalue;
											}
										} else if ($elementtype=="radio") {
											$resultelement2=mysqli_query($link, "select * from `elements` WHERE id='$value' LIMIT 1");
											while ($rowelement2=mysqli_fetch_array($resultelement2)){
												$value=$rowelement2['label'];
											}
										} else if ($elementtype=="dropdown") {
											$resultelement2=mysqli_query($link, "select * from `elements` WHERE id='$value' LIMIT 1");
											while ($rowelement2=mysqli_fetch_array($resultelement2)){
												$value=$rowelement2['label'];
											}
										} else if ($elementtype=="upload") {
											if ($value!='') {
												$value='<a href="formuploads/'.$value.'"><button type="button" class="btn btn-xs btn-primary"><i class="icon-download-alt"></i> View / Download File</button></a>';
											}
										} else if ($elementtype=="star") {
											if ($value!='') {
												$value2='<select id='.$elementid.'>';
												if ($value==0) {
												$value2=$value2.'<option value="0" checked></option>';
												} else {
												$value2=$value2.'<option value="0"></option>';
												}
												if ($value==1) {
												$value2=$value2.'<option value="1" checked></option>';
												} else {
												$value2=$value2.'<option value="1"></option>';
												}
												if ($value==2) {
												$value2=$value2.'<option value="2" checked></option>';
												} else {
												$value2=$value2.'<option value="2"></option>';
												}
												if ($value==3) {
												$value2=$value2.'<option value="3" checked></option>';
												} else {
												$value2=$value2.'<option value="3"></option>';
												}
												if ($value==4) {
												$value2=$value2.'<option value="4" checked></option>';
												} else {
												$value2=$value2.'<option value="4"></option>';
												}
												if ($value==5) {
												$value2=$value2.'<option value="5" selected></option>';
												} else {
												$value2=$value2.'<option value="5"></option>';
												}
												$value2=$value2.'</select>';
												$value=$value2.'<div class="rateit" data-rateit-backingfld="#'.$elementid.'" data-rateit-readonly="true"></div>';
											}
										}
									?>					
                                       <tr>
                                          <td class="active"><strong><? echo $elementlabel; ?></strong></td>
                                          <td><? echo $value; ?></td>
                                       </tr>
									<? } ?>
                                    </table>
							<!--end table-->
															
							<!-- End Data -->
						</div>
						<!-- table and form ends -->
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      <? include_once "inc/foot.php"; ?>
	</body>	
</html>

