<?php 
include_once "inc/head.php";
require_once("../includes/inc_files.php");
$page_title = "Edit User"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="Edit User";

// only admin allowed
if ($myadmin!=1) {
	header("location:dashboard.php");
}

// check if we have the id of the user to edit 
if ((isset($_GET['id'])) && (is_numeric($_GET['id']))) {
    $getid=$_GET['id'];
    $result=mysqli_query($link, "select * from `users` WHERE id='$getid' LIMIT 1");
	
	// make sure there's actually such a user
	if (mysqli_num_rows($result)=='0'){
		header("location:systemusers.php");
	}
	
    while ($row=mysqli_fetch_array($result)){
        $editname=$row['name'];
        $editemail=$row['email'];
        $editadmin=$row['admin'];
		$editusername=$row['username'];
    }
} else {
    header("location:systemusers.php");
}

// insert the new user data
if (isset($_POST['edituser'])) {
							
    include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
		
    $newname=$_POST['name'];
	$newusername=$_POST['username'];
	$newusername = preg_replace('/[^A-Za-z0-9]/', '', $newusername); // Only letters and numbers accepted
    $newemail=$_POST['useremail'];
    $newpassword=$_POST['pass'];
	$newadmin=$_POST['admin'];
	
	$encpassword=sha1($newpassword);
	
	// check if email exists
	$resultcheck=mysqli_query($link, "select * from `users` WHERE id !='$getid' AND email='$newemail'");
	
	// check if username exists
	$resultcheck2=mysqli_query($link, "select * from `users` WHERE id !='$getid' AND username='$newusername'");
		
	if (mysqli_num_rows($resultcheck)=='0'){	
		if (mysqli_num_rows($resultcheck2)=='0'){
			if ($newpassword=="") {
				$sql = "UPDATE `users` SET name='$newname', username='$newusername', email='$newemail', admin='$newadmin' WHERE id='$getid'";
				mysqli_query($link, $sql) or die('Error, query failed');
				$note="User information was edited successfully";
			} else {
				$sql = "UPDATE `users` SET name='$newname', username='$newusername', email='$newemail', password='$encpassword', admin='$newadmin' WHERE id='$getid'";
				mysqli_query($link, $sql) or die('Error, query failed');
				$note="User information was edited successfully";
			}
			// get new data to form
			$editname=$newname;
			$editemail=$newemail;
			$editadmin=$newadmin;
			$editusername=$newusername;
			
		} else {
			$note="The username you entered already exists";
		}
	} else {
		$note="The email you entered already exists";
	}
}
?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-group icon-large"></i> System Users</h3> 	
						
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->
				
				
				
				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<!-- table and form starts -->
						<div class="col-md-12">
							<? if (isset ($note)) { ?>
							<div class="alert alert-dismissable alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="icon-warning-sign"></i> <? echo $note; ?>
							</div>
							<? } ?>
						<!-- form -->
						<div class="col-md-6">
							<div class="page-content page-form">
							
							<div class="widget">
								<div class="widget-head">
									<h5><i class="icon-edit red"></i> Edit User #<? echo $getid; ?></h5>
								</div>
								   <div class="widget-body">
									  <form class="form-horizontal" id="ValidForm" method="post" action="edituser.php?id=<? echo $getid; ?>" role="form">
									  
										<div class="form-group">
										  <label class="col-lg-2 control-label">Name</label>
										  <div class="col-lg-10">
											<input type="text" name="name" value="<? echo $editname; ?>" class="form-control" placeholder="Name">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Username</label>
										  <div class="col-lg-10">
											<input type="text" name="username" value="<? echo $editusername; ?>" class="form-control" placeholder="Username">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Email</label>
										  <div class="col-lg-10">
											<input type="text" name="useremail" value="<? echo $editemail; ?>" class="form-control" placeholder="Email">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Password</label>
										  <div class="col-lg-10">
											<input type="password" name="pass" class="form-control" placeholder="Leave empty to maintain the old password">
										  </div>
										</div>
										
										<? if ($editadmin==0) {?>
										<div class="form-group">
										  <label class="col-lg-2 control-label">Type</label>
										  <div class="col-lg-10">
											<select class="form-control" name="admin">
											  <option value="0">Regular</option>
											  <option value="1">Admin</option>
											</select>
										  </div>
										</div>
										<? } else { ?>
										<div class="form-group">
										  <label class="col-lg-2 control-label">Type</label>
										  <div class="col-lg-10">
											<select class="form-control" name="admin">
											  <option value="1">Admin</option>
											  <option value="0">Regular</option>
											</select>
										  </div>
										</div>
										<? } ?>
															
										<div class="form-group">
										  <div class="col-lg-offset-2 col-lg-10">
											<button type="submit" name="edituser" class="btn btn-primary"><i class="icon-edit"></i> Edit</button>
										  </div>
										</div>
									  </form>
								   </div>
								   
								   <div class="widget-foot">
								   
								   </div>
								</div>
							
							</div>
						</div>
						<!-- end form -->
						<!-- table -->
						<div class="col-md-6">
						
						<div class="widget">
						<div class="page-content page-tables">
							<div class="widget-head br-green">
								<h5><i class="icon-group green"></i> Users List</h5>
							</div>
							
							<div class="widget-body">
								<div class="row">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-hover table-bordered" cellpadding="0" cellspacing="0" border="0" id="data-table">
												<thead>
													<tr>
														<th>Name</th>
														<th>Username</th>
														<th>Type</th>
														<th>Status</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
												<?
												// get users list
												$resultusers=mysqli_query($link, "select * from `users` ORDER BY id DESC");
												while ($rowusers=mysqli_fetch_array($resultusers)){
													$id=$rowusers['id'];
													$name=$rowusers['name'];
													$username=$rowusers['username'];
													$admin=$rowusers['admin'];
													$active=$rowusers['active'];
													
													if ($active==1) { 
														$activetext="Active";
														$color="success";
														$tooltip="Deactivate";
														$icon="remove";
														$iconcolor="danger";
													} else {
														$activetext="Inactive";
														$color="danger";
														$tooltip="Activate";
														$icon="ok";
														$iconcolor="success";
													}
													
													if ($admin==1) { 
														$admin="Admin";
													} else {
														$admin="Regular";
													}
												?>
													<tr>
														<td><? echo $name; ?></td>
														<td><? echo $username; ?></td>
														<td><? echo $admin; ?></td>
														<td class="<? echo $color; ?>"><? echo $activetext; ?></td>
														<td>
															<a href="edituser.php?id=<? echo $id; ?>" class="bs-tooltip" title="edit" data-placement="top"><button class="btn btn-xs btn-warning"><i class="icon-pencil"></i> </button></a>
															<a href="systemusers.php?active=<? echo $id; ?>" class="bs-tooltip" title="<? echo $tooltip; ?>" data-placement="top"><button class="btn btn-xs btn-<? echo $iconcolor; ?>"><i class="icon-<? echo $icon; ?>"></i> </button></a>
														</td>
													</tr>
												<? } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							</div>
							
							<div class="widget-foot">
							
							</div>
						
							</div>
						</div>
						<!-- end table -->
						</div>
						<!-- table and form ends -->
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      
	</body>	
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>