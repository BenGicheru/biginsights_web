<?php 
include_once "inc/head.php";
require_once("../includes/inc_files.php");
$page_title = "Edit User"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="Edit User";

// get my info for edit form
	$editname=$myname;
	$editusername=$myusername;
	$editemail=$myemail;
	
// insert the new user data
if (isset($_POST['edituser'])) {
							
    include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
		
    $newname=$_POST['name'];
    $newemail=$_POST['useremail'];
	$newusername=$_POST['username'];
	$newusername = preg_replace('/[^A-Za-z0-9]/', '', $newusername); // Only letters and numbers accepted
    $newpassword=$_POST['pass'];
	
	$encpassword=sha1($newpassword);
	
	// check if email exists
	$resultcheck=mysqli_query($link, "select * from `users` WHERE email='$newemail' AND id!='$myid' LIMIT 1");
	
	// check if username exists
	$resultcheck2=mysqli_query($link, "select * from `users` WHERE username='$newusername' AND id!='$myid' LIMIT 1");
    
	if (mysqli_num_rows($resultcheck)=='0'){	
		if (mysqli_num_rows($resultcheck2)=='0'){
			if ($newpassword=="") {
				$sql = "UPDATE `users` SET name='$newname', username='$newusername', email='$newemail' WHERE id='$myid'";
				mysqli_query($link, $sql) or die('Error, query failed');
				$note="Your information was edited successfully";
			} else {
				$sql = "UPDATE `users` SET name='$newname', username='$newusername', email='$newemail', password='$encpassword' WHERE id='$myid'";
				mysqli_query($link, $sql) or die('Error, query failed');
				$note="Your information was edited successfully";
			}
			// get new data to form
			$editname=$newname;
			$editusername=$newusername;
			$editemail=$newemail;
			
		} else {
			$note="The username you entered already exists";
		}
	} else {
		$note="The email you entered already exists";
	}
}
?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-group icon-large"></i> System Users</h3> 	
						
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->
				
				
				
				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<!-- table and form starts -->
						<div class="col-md-12">
							<? if (isset ($note)) { ?>
							<div class="alert alert-dismissable alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="icon-warning-sign"></i> <? echo $note; ?>
							</div>
							<? } ?>
						<!-- form -->
							<div class="page-content page-form">
							
							<div class="widget">
								<div class="widget-head">
									<h5><i class="icon-edit red"></i> Edit My Info</h5>
								</div>
								   <div class="widget-body">
									  <form class="form-horizontal" id="ValidForm" method="post" action="myinfo.php" role="form">
									  
										<div class="form-group">
										  <label class="col-lg-2 control-label">Name</label>
										  <div class="col-lg-10">
											<input type="text" name="name" value="<? echo $editname; ?>" class="form-control" placeholder="Name">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Username</label>
										  <div class="col-lg-10">
											<input type="text" name="username" value="<? echo $editusername; ?>" class="form-control" placeholder="Username">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Email</label>
										  <div class="col-lg-10">
											<input type="text" name="useremail" value="<? echo $editemail; ?>" class="form-control" placeholder="Email">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Password</label>
										  <div class="col-lg-10">
											<input type="password" name="pass" class="form-control" placeholder="Leave empty to maintain the old password">
										  </div>
										</div>
															
										<div class="form-group">
										  <div class="col-lg-offset-2 col-lg-10">
											<button type="submit" name="edituser" class="btn btn-primary"><i class="icon-edit"></i> Edit</button>
										  </div>
										</div>
									  </form>
								   </div>
								   
								   <div class="widget-foot">
								   
								   </div>
								</div>
							
							</div>
						<!-- end form -->
						</div>
						<!-- table and form ends -->
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      <? include_once "inc/foot.php"; ?>
      
	</body>	
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>