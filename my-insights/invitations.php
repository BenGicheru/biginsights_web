<?php 
include_once "inc/head.php";
require_once("../includes/inc_files.php");
$page_title = "Invitations"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="Invitations";
include_once "inc/getforminfo.php";

// send single invitation
if (isset($_POST['sendsingle'])) {
	include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
	$_POST= str_replace("'", "''", $_POST);
	$_POST= str_replace('"', '\"', $_POST);
	include_once "inc/sendsingleinvitation.php";
}

// send list invitation
if (isset($_POST['sendlistinv'])) {
	include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
	$_POST= str_replace("'", "''", $_POST);
	$_POST= str_replace('"', '\"', $_POST);
	include_once "inc/sendlistinvitation.php";
}

// delete invitation
if ((isset($_GET['delete'])) && (is_numeric($_GET['delete']))) {
	$deleteid=$_GET['delete'];
	$sql = "UPDATE invitations SET deleted='1' WHERE id='$deleteid' AND formid='$thisformgenkey'";
	header("location:invitations.php?biginsightsid=".$thisformgenkey);
}
?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-file icon-large"></i> Invitations Center <span><? echo $thisformname; ?></span></h3> 	
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->

				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<!-- table and form starts -->
						<div class="col-md-12">
							<? if (isset ($note)) { ?>
							<div class="alert alert-dismissable alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="icon-warning-sign"></i> <? echo $note; ?>
							</div>
							<? } ?>
							<!-- Data -->
							<div class="row">

							  <div class="col-md-3 col-sm-6">
								<div class="well br-lblue">
								  <h2><? echo $thisforminvtotal; ?></h2>
								  <p>Sent Invitations</p>                        
								</div>
							  </div>
							  
							  <div class="col-md-3 col-sm-6">
								<div class="well br-lblue">
								  <h2><? echo $thisformsubtotal; ?></h2>
								  <p>Submissions</p>                        
								</div>
							  </div>
							  
							  <div class="col-md-3 col-sm-6">
								<div class="well br-lblue">
								  <h2><? echo $subinvpercent; ?></h2>
								  <p>Response Percentage</p>                        
								</div>
							  </div>

							</div>
															
							<!-- End Data -->
						<!-- table -->						
						<div class="widget">
						<div class="page-content page-tables">
							<div class="widget-head br-green">
								<h5><i class="icon-file green"></i> Sent Invitations</h5>
							</div>
							
							<div class="widget-body">
								<div class="row">
									<div class="col-md-12">
										<div class="table-responsive">
											<table cellpadding="0" cellspacing="0" border="0" id="data-table" width="100%">
												<thead>
													<tr>
														<th>Name</th>
														<th>Email</th>
														<th>List</th>
														<th>Submitted</th>
														<th></th>
														<th></th>
													</tr>
												</thead>
												<tbody>
													<?
													// get invitations list
													$resultinv=mysqli_query($link, "select * from `invitations` WHERE formid='$thisformgenkey' AND deleted='0' ORDER BY id DESC");
													while ($rowinv=mysqli_fetch_array($resultinv)){
														$invitationid=$rowinv['id'];
														$recipientid=$rowinv['recipientid'];
														$email=$rowinv['email'];
														$listid=$rowinv['listid'];
														
														// check if this invitation has a submission
														$resultsubmitted=mysqli_query($link, "select * from `submissions` WHERE invitationid='$invitationid' LIMIT 1");
														
														if (mysqli_num_rows($resultsubmitted)==1){
															while ($rowsubmitted=mysqli_fetch_array($resultsubmitted)){
																$submissionid=$rowsubmitted['id'];
															}
														} else {
															$submissionid=0;
														}
														
														// get recipient information
														if ($recipientid!='0') {
															$resultrec=mysqli_query($link, "select * from `recipients` WHERE id='$recipientid' LIMIT 1");
															while ($rowrec=mysqli_fetch_array($resultrec)){
																$recname=$rowrec['name'];
																$recemail=$rowrec['email'];
															}
														} else {
															$recname="none";
															$recemail=$email;
														}
														
														// get list information
														if ($listid!='0') {
															$resultlist=mysqli_query($link, "select * from `lists` WHERE id='$listid' LIMIT 1");
															while ($rowlist=mysqli_fetch_array($resultlist)){
																$listname=$rowlist['name'];
															}
														} else {
															$listname="none";
														}
														
														// get submitted visuals
														if ($submissionid=='0') {
															$color="red";
															$icon="remove";
														} else {
															$color="green";
															$icon="ok";
														}
													?>
													<tr>
														<td><? echo $recname; ?></td>
														<td><? echo $recemail; ?></td>
														<td><? echo $listname; ?></td>
														<td class="text-center <? echo $color; ?>"><i class="icon-<? echo $icon; ?>"></i></td>
														<td>
														<? if ($submissionid!=0) {?>
														<a href="submission.php?biginsightsid=<? echo $thisformgenkey; ?>&submission=<? echo $submissionid; ?>"><button type="button" class="btn btn-xs btn-primary">View Submission</button></a>
														<? } ?>
														</td>
														<td><a href="invitations.php?biginsightsid=<? echo $thisformgenkey; ?>&delete=<? echo $invitationid; ?>"><button type="button" class="btn btn-xs btn-danger">Delete Invitation</button></a></td>
													</tr>
													<? } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							</div>
							
							<div class="widget-foot">
							
							</div>
						
							</div>
						<!-- end table -->
							<div class="row">
							<!-- send single invitation -->
							<div class="col-md-6">
								<div class="widget">
									<div class="widget-head">
										<h5><i class="icon-file green"></i> Single Inviation</h5>
									</div>
									   <div class="widget-body">
										  <form class="form-horizontal" id="ValidForm" role="form" method="post" action="invitations.php?biginsightsid=<? echo $thisformgenkey; ?>">
										  
											<div class="form-group">
											  <label class="col-lg-2 control-label">Email</label>
											  <div class="col-lg-10">
												<input type="text" name="email" class="form-control" placeholder="recipient email">
											  </div>
											</div>
																
											<div class="form-group">
											  <div class="col-lg-offset-2 col-lg-10">
												<button type="submit" class="btn btn-primary" name="sendsingle">Send</button>
											  </div>
											</div>
										  </form>
									   </div>
									   
									   <div class="widget-foot">
									   
									   </div>
								   </div>
							</div>
							<!-- end send single invitation -->
							<!-- send list invitations -->
							<div class="col-md-6">
								<div class="widget">
									<div class="widget-head">
										<h5><i class="icon-list green"></i> List Invitation</h5>
									</div>
									   <div class="widget-body">
										  <form class="form-horizontal" id="ValidForm" role="form" method="post" action="invitations.php?biginsightsid=<? echo $thisformgenkey; ?>">
											
											<div class="form-group">
												<?
												if ($myadmin==0) { // if i am not admin
													// get list of forms that are public or I have created
													$resultlists=mysqli_query($link, "select * from `lists` where createdby='$myid' OR public='1'");
												} else { // if i am admin
													// get all lists
													$resultlists=mysqli_query($link, "select * from `lists`");
												}
												// if there are lists show them
												if (mysqli_num_rows($resultlists)>0) {
												?>
											  <label class="col-lg-2 control-label">List</label>
											  <div class="col-lg-10">
												<select class="form-control" name="sendlistid">
												<?
												while ($rowforms=mysqli_fetch_array($resultlists)){
													$thislistid=$rowforms['id'];
													$thislistname=$rowforms['name'];
												?>
												  <option value="<? echo $thislistid; ?>"><? echo $thislistname; ?></option>
												<? } // close while ?>
												</select>
											  </div>
											  <? } // close if ?>
											</div>
																
											<div class="form-group">
											  <div class="col-lg-offset-2 col-lg-10">
											  <? if (mysqli_num_rows($resultlists)>0) { ?>
												<button type="submit" class="btn btn-primary" name="sendlistinv">Send Invitation</button>
											  <? } else { ?>
												<a href="lists.php"><button type="button" class="btn btn-primary">Create a List</button></a>
											  <? } ?>
											  </div>
											</div>
										  </form>
									   </div>
									   
									   <div class="widget-foot">
									   
									   </div>
								   </div>
							</div>
							<!-- end send list invitations -->
							</div>
						</div>
						<!-- table and form ends -->
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      <? include_once "inc/foot.php"; ?>
      
	</body>	
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>