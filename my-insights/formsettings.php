<?php
include_once "inc/head.php";
require_once("../includes/inc_files.php");
$page_title = "Form Settings"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="Form Settings";
include_once "inc/getforminfo.php";

// switch public->private and vice versa
if ((isset($_GET['switch'])) && (($thisformcreatedby==$myid) xor ($myadmin=='1'))){
    // get status
    $resultswitch=mysqli_query($link, "select * from `forms` WHERE servekey='$thisformgenkey' LIMIT 1");
    while ($rowswitch=mysqli_fetch_array($resultswitch)){
        $switchpublic=$rowswitch['public'];
    }
    // switch status
    if ($switchpublic==1) {
        $newstatus=0;
    } else {
        $newstatus=1;
    }
    // insert new status to db
    $sql = "UPDATE forms SET public='$newstatus' WHERE servekey='$thisformgenkey'";
    mysqli_query($link, $sql);
    header("location:formsettings.php?biginsightsid=".$thisformgenkey);
}

// switch open->closed and vice versa
if (isset($_GET['close'])) {
    // get status
    $resultswitch=mysqli_query($link, "select * from `forms` WHERE servekey='$thisformgenkey' LIMIT 1");
    while ($rowswitch=mysqli_fetch_array($resultswitch)){
        $switchclosed=$rowswitch['closed'];
    }
    // switch status
    if ($switchclosed=="0000-00-00") {
        $sql = "UPDATE forms SET closed=NOW() WHERE servekey='$thisformgenkey'";
    } else {
        $sql = "UPDATE forms SET closed='0000-00-00' WHERE servekey='$thisformgenkey'";
    }
    // insert new status to db
    mysqli_query($link, $sql);
    header("location:formsettings.php?biginsightsid=".$thisformgenkey);
}

// clear favicon
if (isset($_GET['clearfavicon'])) {
    $sql = "UPDATE forms SET favicon='' WHERE servekey='$thisformgenkey'";
    mysqli_query($link, $sql);
    header("location:formsettings.php?biginsightsid=".$thisformgenkey);
}

// clear background image
if (isset($_GET['clearbgimage'])) {
    $sql = "UPDATE forms SET bgimage='' WHERE servekey='$thisformgenkey'";
    mysqli_query($link, $sql);
    header("location:formsettings.php?biginsightsid=".$thisformgenkey);
}

// clear css
if (isset($_GET['clearcss'])) {
    $sql = "UPDATE forms SET cssfile='' WHERE servekey='$thisformgenkey'";
    mysqli_query($link, $sql);
    header("location:formsettings.php?biginsightsid=".$thisformgenkey);
}

// save settings
if (isset($_POST['editsettings'])){
		// strip ' and "
		$_POST= str_replace("'", "''", $_POST);
		
		// filter POST data
		require_once 'htmlpurifier/HTMLPurifier.auto.php';
		$config = HTMLPurifier_Config::createDefault();
		$config->set('HTML.SafeIframe', true);
		$config->set('HTML.SafeEmbed', true);
		$config->set('URI.SafeIframeRegexp','%^//www.youtube.com/embed/%');
		$purifier = new HTMLPurifier($config);
		
		$formname = $purifier->purify($_POST['formname']);
		$forminstructions = $purifier->purify($_POST['forminstructions']);
		$invitationsubject = $purifier->purify($_POST['invitationsubject']);
		$invitationbody = $purifier->purify($_POST['invitationbody']);
		$submitstring = $purifier->purify($_POST['submitstring']);
		$redirecturl = $purifier->purify($_POST['redirecturl']);
		$thankyoumessage = $purifier->purify($_POST['thankyoumessage']);
		$inaccessible = $purifier->purify($_POST['inaccessible']);
		$theme = $purifier->purify($_POST['theme']);
		$public = $purifier->purify($_POST['public']);
		$invitation = $purifier->purify($_POST['invitation']);
		$multiple = $purifier->purify($_POST['multiple']);
		
		if ($public=="on") {
			$public=1;
		} else {
			$public=0;
		}
		
		if ($invitation=="on") {
			$invitation=1;
		} else {
			$invitation=0;
		}
		
		if ($multiple=="on") {
			$multiple=1;
		} else {
			$multiple=0;
		}

        $sql = "UPDATE forms SET name='$formname', forminstructions='$forminstructions', invitationsubject='$invitationsubject', invitationbody='$invitationbody',";
		$sql = $sql." submitstring='$submitstring', redirecturl='$redirecturl', thankyoumessage='$thankyoumessage', inaccessible='$inaccessible', theme='$theme',";
		$sql = $sql." public='$public', invitation='$invitation', multiple='$multiple' WHERE servekey='$thisformgenkey'";
		mysqli_query($link, $sql);
        header("location:formsettings.php?biginsightsid=".$thisformgenkey);
}

// add css file
if (isset ($_POST['addcss'])) {
    $tmp = $_FILES['uploaded_file']['tmp_name']; 
    $org = $_FILES['uploaded_file']['name'];
    $rand=rand(00000000000,9999999999);
    $directory = "uploads/"; // Upload files to here.
    $ext=explode(".", $org);
    $extension=$ext[1];
    $extension = strtolower($extension); // convert it to lowercase
    $filename=$rand.".".$extension;
    $targetpath = $directory.$rand.".".$extension; // Filename and path
				 
    $filesize=round(($_FILES['uploaded_file']['size'] / 1024));
		
    if ($filesize < 1000){				
        if ($extension == 'css'){
            if(move_uploaded_file($tmp, $targetpath)) {
                $sql = "UPDATE forms SET cssfile='$filename' WHERE servekey='$thisformgenkey'";
                mysqli_query($link, $sql) or die('Error, query failed');
                $note = "Stylesheet Updated Successfully";
            }
        } else {
            $note = "Unsupported File Type";
        }
    } else {
        $note = "File Too Large, Maximum Size: 1 MB";
    }
}
// add favicon
if (isset ($_POST['addfavicon'])) {
    $tmp = $_FILES['uploaded_file']['tmp_name']; 
    $org = $_FILES['uploaded_file']['name'];
    $rand=rand(00000000000,9999999999);
    $directory = "uploads/"; // Upload files to here.
    $ext=explode(".", $org);
    $extension=$ext[1];
    $extension = strtolower($extension); // convert it to lowercase
    $filename=$rand.".".$extension;
    $targetpath = $directory.$rand.".".$extension; // Filename and path
				 
    $filesize=round(($_FILES['uploaded_file']['size'] / 1024));
		
    if ($filesize < 1000){				
        if  (($extension == 'jpg') xor ($extension == 'png') xor ($extension == 'gif')){
            if(move_uploaded_file($tmp, $targetpath)) {
                $sql = "UPDATE forms SET favicon='$filename' WHERE servekey='$thisformgenkey'";
                mysqli_query($link, $sql) or die('Error, query failed');
                $note = "Favicon Updated Successfully";
            }
        } else {
            $note = "Unsupported File Type";
        }
    } else {
        $note = "File Too Large, Maximum Size: 1 MB";
    }
}

// add background image
if (isset ($_POST['addbgimage'])) {
    $tmp = $_FILES['uploaded_file']['tmp_name']; 
    $org = $_FILES['uploaded_file']['name'];
    $rand=rand(00000000000,9999999999);
    $directory = "uploads/"; // Upload files to here.
    $ext=explode(".", $org);
    $extension=$ext[1];
    $extension = strtolower($extension); // convert it to lowercase
    $filename=$rand.".".$extension;
    $targetpath = $directory.$rand.".".$extension; // Filename and path
				 
    $filesize=round(($_FILES['uploaded_file']['size'] / 1024));
		
    if ($filesize < 1000){				
        if  (($extension == 'jpg') xor ($extension == 'png') xor ($extension == 'gif')){
            if(move_uploaded_file($tmp, $targetpath)) {
                $sql = "UPDATE forms SET bgimage='$filename' WHERE servekey='$thisformgenkey'";
                mysqli_query($link, $sql) or die('Error, query failed');
                $note = "Background Image Updated Successfully";
            }
        } else {
            $note = "Unsupported File Type";
        }
    } else {
        $note = "File Too Large, Maximum Size: 1 MB";
    }
}
?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-cog icon-large"></i> <? echo $thisformname; ?> <span>settings</span></h3> 	
						<div class="pull-right">
						    <a href="data.php?biginsightsid=<? echo $thisformgenkey; ?>"> <button type="button" class="btn btn-info"><i class="icon-bar-chart"></i> Responses</button></a>
							<a href="elements.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-cogs"></i> Edit Survey</button></a>
							<a target="_blank" href="viewform.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-external-link"></i> Preview Survey</button></a>
							<a href="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>&close"><button type="button" class="btn btn-<? echo $thisformclosecolor; ?>"><i class="icon-<? echo $thisformcloseicon; ?>"></i> <? echo $thisformclosetext; ?></button></a>
							<? if (($thisformcreatedby==$myid) xor ($myadmin=='1')) { ?>
							<a href="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>&switch"><button type="button" class="btn btn-info"><i class="icon-<? echo $thisformswitchicon; ?>"></i> <? echo $thisformswitchtext; ?></button></a>
							<? } ?>
							
							
							<ul class="share-buttons">
						<li><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>&t=" title="Share on Facebook" target="_blank"><img src="img/si/Facebook.png"></a></li>
  <li><a href="https://twitter.com/intent/tweet?source=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>&text=:%20http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>" target="_blank" title="Tweet"><img src="img/si/Twitter.png"></a></li>
  <li><a href="https://plus.google.com/share?url=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>" target="_blank" title="Share on Google+"><img src="img/si/Google+.png"></a></li>
  <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>&title=&summary=&source=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>" target="_blank" title="Share on LinkedIn"><img src="img/si/LinkedIn.png"></a></li>
						</ul>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->

				
				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<div class="col-md-12">
							<? if (isset ($note)) { ?>
							<div class="alert alert-dismissable alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="icon-warning-sign"></i> <? echo $note; ?>
							</div>
							<? } ?>
						<!-- form -->
							<div class="page-content page-form">
							
							<div class="widget">
								<div class="widget-head">
									<h5><i class="icon-cog green"></i> Survey Settings</h5>
								</div>
								   <div class="widget-body">
									  <form class="form-horizontal" id="ValidForm" role="form" method="post" action="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>" enctype="multipart/form-data">
									  
										<div class="form-group">
										  <label class="col-lg-2 control-label">Name</label>
										  <div class="col-lg-10">
											<input type="text" name="formname" class="form-control" value="<? echo $thisformname; ?>" placeholder="form name">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Survey Instuctions</label>
										  <div class="col-lg-10">
											<textarea class="form-control instructions" rows="3" name="forminstructions"><? echo $thisforminstructions; ?></textarea>
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Invitation Email Subject</label>
										  <div class="col-lg-10">
											<input type="text" name="invitationsubject" class="form-control" placeholder="invitationsubject" value="<? echo $thisforminvsubject; ?>">
										  </div>
										</div>
								
										<div class="form-group">
										  <label class="col-lg-2 control-label">Invitation Email Content</label>
										  <div class="col-lg-10">
											<textarea class="form-control email" rows="3" name="invitationbody"><? echo $thisforminvbody; ?></textarea>
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Submit Button String</label>
										  <div class="col-lg-10">
											<input type="text" class="form-control" name="submitstring" value="<? echo $thisformsubmitstring; ?>" placeholder="Send, Submit, ...etc">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Redirect URL</label>
										  <div class="col-lg-10">
											<input type="text" name="redirecturl" class="form-control" placeholder="Name" value="<? echo $thisformredirecturl; ?>">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Thank You Message</label>
										  <div class="col-lg-10">
											<input type="text" name="thankyoumessage" class="form-control" placeholder="Name" value="<? echo $thisformthankyou; ?>">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Inaccessible Survey message</label>
										  <div class="col-lg-10">
											<input type="text" name="inaccessible" class="form-control" placeholder="Name" value="<? echo $thisforminaccessible; ?>">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Theme</label>
										  <div class="col-lg-10">
											<select class="form-control" name="theme">
											<? if ($thisformtheme!="") { ?>
												<option value="<? echo $thisformtheme; ?>"><? echo $thisformtheme; ?></option>
											<? } ?>
												<option value="">Default</option>
											<?
											foreach(glob('themes/*.*') as $themefile) {
											$themename=explode("/", $themefile);
											?>
												<option value="<? echo $themename[1]; ?>"><? echo $themename[1]; ?></option>
											<? } ?>
											</select>
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Options</label>
										  <div class="col-lg-10">
											<label class="checkbox-inline">
											<label class="checkbox-inline">
												<div class="make-switch" data-on="success" data-off="danger">
													<input type="checkbox" name="invitation" <? echo $invitationchecked; ?>>
												</div>
												<strong>Invitation Based Only</strong>
											</label><br>
											<label class="checkbox-inline">
												<div class="make-switch" data-on="success" data-off="danger">
													<input type="checkbox" name="multiple" <? echo $multiplechecked; ?>>
												</div>
												<strong>Multiple Submissions Allowed</strong>
											</label><br>
										  </div>
										</div>
										<hr>				
										<div class="form-group">
										  <div class="col-lg-offset-2 col-lg-10">
											<button type="submit" class="btn btn-primary pull-right" name="editsettings">Save Settings</button>
										  </div>
										</div>
									  </form>
								   </div>
								   
								   <div class="widget-foot">
								   
								   </div>
								</div>
							
							</div>
						<!-- end form -->
						</div>
						<div class="col-md-12">
							<div class="col-md-4">
							<!-- css file upload -->
									<div class="well">
										<h5><i class="icon-upload"></i> Upload stylesheet</h5>
											  <? if ($thisformcssfile!="") { ?>
											  <span class="label"><a href="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>&clearcss">Delete Stylesheet</a></span>
											  <? } ?>
										<p>Only CSS files accepted</p>
										<form class="form-inline" role="form" method="post" action="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>" enctype="multipart/form-data">
										  <div class="form-group">
											<input type="file" name="uploaded_file" id="file" required />
										  </div><br><br>
										  <div class="form-group">
											<button type="submit" class="btn btn-success" name="addcss"><i class="icon-upload-alt"></i> Upload</button>
										  </div>
										</form>
									</div>
							</div>
							<div class="col-md-4">
									<div class="well">
										<h5><i class="icon-upload"></i> Upload favicon</h5>
											  <? if ($thisformfavicon!="") { ?>
											  <span class="label"><a href="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>&clearfavicon">Delete Favicon</a></span>
											  <? } ?>
										<p>jpg, png, gif</p>
										<form class="form-inline" role="form" method="post" action="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>" enctype="multipart/form-data">
										  <div class="form-group">
											<input type="file" name="uploaded_file" id="file" required />
										  </div><br><br>
										  <div class="form-group">
											<button type="submit" class="btn btn-success" name="addfavicon"><i class="icon-upload-alt"></i> Upload</button>
										  </div>
										</form>
									</div>
							</div>
							<div class="col-md-4">
									<div class="well">
										<h5><i class="icon-upload"></i> Upload background image</h5>
											  <? if ($thisformbgimage!="") { ?>
											  <span class="label"><a href="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>&clearbgimage">Delete Background image</a></span>
											  <? } ?>
										<p>jpg, png, gif</p>
										<form class="form-inline" role="form" method="post" action="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>" enctype="multipart/form-data">
										  <div class="form-group">
											<input type="file" name="uploaded_file" id="file" required />
										  </div><br><br>
										  <div class="form-group">
											<button type="submit" class="btn btn-success" name="addbgimage"><i class="icon-upload-alt"></i> Upload</button>
										  </div>
										</form>
									</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      <? include_once "inc/foot.php"; ?>

	</body>	
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>
