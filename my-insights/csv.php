<?
include 'inc/connect.php';
require_once("../includes/inc_files.php"); 

// if i am not logged in, kick me out
if(!$session->is_logged_in()) {redirect_to("../signin.php");}
else  {
	$user = User::find_by_id($_SESSION['biginsights']['ams']['user_id']);
	$myid=$user->id;
}
include 'inc/getmyinfo.php';
include_once "inc/getforminfo.php";
// create header
$countthat=0;
$output=$thisformname."\n\n\n";
$resultsubmissions2=mysqli_query($link, "select * from `submissions` WHERE formid='$thisformgenkey' ORDER BY id DESC LIMIT 1");
while ($rowsubs2=mysqli_fetch_array($resultsubmissions2)){
	$thissubid2=$rowsubs2['id'];

	$resultsubmission2=mysqli_query($link, "select * from `subfields` WHERE submissionid='$thissubid2' ORDER BY id ASC");
	while ($rowsub2=mysqli_fetch_array($resultsubmission2)){
		$elementid2=$rowsub2['elementid'];
		// get element label
		$resultelement2=mysqli_query($link, "select * from `elements` WHERE id='$elementid2' LIMIT 1");
		while ($rowelement2=mysqli_fetch_array($resultelement2)){
			$elementlabel=$rowelement2['label'];
			if ($countthat==0) {
				$output=$output.$elementlabel;
			} else {
				$output=$output.",".$elementlabel;
			}
			$countthat++;
		}
	}
}
$output=$output."\n";
	
$countthis=0;
// data
$resultsubmissions=mysqli_query($link, "select * from `submissions` WHERE formid='$thisformgenkey' ORDER BY DESC ASC");
while ($rowsubs=mysqli_fetch_array($resultsubmissions)){
	$thissubid=$rowsubs['id'];
	
	$countthis=0;
	$resultsubmission=mysqli_query($link, "select * from `subfields` WHERE submissionid='$thissubid' ORDER BY id ASC");
	while ($rowsub=mysqli_fetch_array($resultsubmission)){
		$fieldid=$rowsub['id'];
		$elementid=$rowsub['elementid'];
		$value=$rowsub['value'];
										
		// get element info
		$resultelement=mysqli_query($link, "select * from `elements` WHERE id='$elementid' LIMIT 1");
		while ($rowelement=mysqli_fetch_array($resultelement)){
			$elementlabel=$rowelement['label'];
			$elementtype=$rowelement['type'];
			$elementonvalue=$rowelement['onvalue'];
			$elementoffvalue=$rowelement['offvalue'];
		}
										
		// if type is toggle, get on value and off value
		if ($elementtype=="toggle") {
			if ($value=="on") {
				$value=$elementonvalue;
			} else {
				$value=$elementoffvalue;
			}
		} else if ($elementtype=="radio") {
			$resultelement2=mysqli_query($link, "select * from `elements` WHERE id='$value' LIMIT 1");
			while ($rowelement2=mysqli_fetch_array($resultelement2)){
				$value=$rowelement2['label'];
			}
		} else if ($elementtype=="dropdown") {
			$resultelement2=mysqli_query($link, "select * from `elements` WHERE id='$value' LIMIT 1");
			while ($rowelement2=mysqli_fetch_array($resultelement2)){
				$value=$rowelement2['label'];
			}
		} else if ($elementtype=="upload") {
			if ($value!='') {
				$value='<file>';
			}
		}
		if ($countthis==0) {
			$output=$output.$value;
		} else {
			$output=$output.",".$value;
		}
		$countthis++;
	}
	$output=$output."\n";
}
$csvformname=$newname = preg_replace('/[^A-Za-z0-9]/', '-', $thisformname); // Only letters and numbers accepted
$csvformname=$csvformname.".csv";
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$csvformname);
echo $output;
exit;
?>