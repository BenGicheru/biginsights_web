<?php 
include_once "inc/head.php";
require_once("../includes/inc_files.php");
$page_title = "System Users"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="System Users";

// only admin allowed
if ($myadmin!=1) {
	header("location:dashboard.php");
}

// activate/deactivate users
if ((isset($_GET['active'])) && (is_numeric($_GET['active']))) {
    $changeid=$_GET['active'];
    // get status
    $resultthisuser=mysqli_query($link, "select * from `users` WHERE id='$changeid' LIMIT 1");
    while ($rowthisuser=mysqli_fetch_array($resultthisuser)){
        $thisuseractive=$rowthisuser['active'];
    }
    // switch status
    if ($thisuseractive==1) {
        $newstatus=0;
    } else {
        $newstatus=1;
    }
    // insert new status to db
    $sql = "UPDATE users SET active='$newstatus' WHERE id='$changeid'";
    mysqli_query($link, $sql);
    header("location:systemusers.php");
}

// insert the new user data
if (isset($_POST['adduser'])) {
							
    include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
		
    $newname=$_POST['name'];
	$newusername=$_POST['username'];
	$newusername = preg_replace('/[^A-Za-z0-9]/', '', $newusername); // Only letters and numbers accepted
    $newemail=$_POST['email'];
    $newpassword=$_POST['pass'];
	$newadmin=$_POST['admin'];
	
	$encpassword=sha1($newpassword);
	
	// check if email exists
	$resultcheck=mysqli_query($link, "select * from `users` WHERE email='$newemail' AND id!='$getid' LIMIT 1");
	
	// check if username exists
	$resultcheck2=mysqli_query($link, "select * from `users` WHERE username='$newusername' AND id!='$getid' LIMIT 1");
		
	if (mysqli_num_rows($resultcheck)=='0'){	
		if (mysqli_num_rows($resultcheck2)=='0'){
			$sql = "INSERT INTO `users` SET name='$newname', username='$newusername', email='$newemail', password='$encpassword', admin='$newadmin', active='1'";
			mysqli_query($link, $sql) or die('Error, query failed');
			$note="User added successfully";			
		} else {
			$note="The username you entered already exists";
		}
	} else {
		$note="The email you entered already exists";
	}
}
?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-group icon-large"></i> System Users</h3> 	
						
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->
				
				
				
				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<!-- table and form starts -->
						<div class="col-md-12">
							<? if (isset ($note)) { ?>
							<div class="alert alert-dismissable alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="icon-warning-sign"></i> <? echo $note; ?>
							</div>
							<? } ?>
						<!-- form -->
						<div class="col-md-6">
							<div class="page-content page-form">
							
							<div class="widget">
								<div class="widget-head">
									<h5><i class="icon-plus green"></i> Add User</h5>
								</div>
								   <div class="widget-body">
									  <form class="form-horizontal" id="ValidForm" role="form" method="post" action="systemusers.php">
									  
										<div class="form-group">
										  <label class="col-lg-2 control-label">Name</label>
										  <div class="col-lg-10">
											<input type="text" name="name" class="form-control" placeholder="Name">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Username</label>
										  <div class="col-lg-10">
											<input type="text" name="username" class="form-control" placeholder="Username">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Email</label>
										  <div class="col-lg-10">
											<input type="text" name="email" class="form-control" placeholder="Email">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Password</label>
										  <div class="col-lg-10">
											<input type="password" name="pass" class="form-control" placeholder="Password">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Type</label>
										  <div class="col-lg-10">
											<select class="form-control" name="admin">
											  <option value="0">Regular</option>
											  <option value="1">Admin</option>
											</select>
										  </div>
										</div>
															
										<div class="form-group">
										  <div class="col-lg-offset-2 col-lg-10">
											<button type="submit" name="adduser" class="btn btn-primary"><i class="icon-plus"></i> Add</button>
										  </div>
										</div>
									  </form>
								   </div>
								   
								   <div class="widget-foot">
								   
								   </div>
								</div>
							
							</div>
						</div>
						<!-- end form -->
						<!-- table -->
						<div class="col-md-6">
						
						<div class="widget">
						<div class="page-content page-tables">
							<div class="widget-head br-green">
								<h5><i class="icon-group green"></i> Users List</h5>
							</div>
							
							<div class="widget-body">
								<div class="row">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-hover table-bordered" cellpadding="0" cellspacing="0" border="0" id="data-table">
												<thead>
													<tr>
														<th>Name</th>
														<th>Username</th>
														<th>Type</th>
														<th>Status</th>
														<th></th>
													</tr>
												</thead>
												<tbody>
												<?
												// get users list
												$resultusers=mysqli_query($link, "select * from `users` ORDER BY id DESC");
												while ($rowusers=mysqli_fetch_array($resultusers)){
													$id=$rowusers['id'];
													$name=$rowusers['name'];
													$username=$rowusers['username'];
													$admin=$rowusers['admin'];
													$active=$rowusers['active'];
													
													if ($active==1) { 
														$activetext="Active";
														$color="green";
														$tooltip="Deactivate";
														$icon="remove";
														$iconcolor="danger";
													} else {
														$activetext="Inactive";
														$color="red";
														$tooltip="Activate";
														$icon="ok";
														$iconcolor="success";
													}
													
													if ($admin==1) { 
														$admin="Admin";
													} else {
														$admin="Regular";
													}
												?>
													<tr>
														<td><? echo $name; ?></td>
														<td><? echo $username; ?></td>
														<td><? echo $admin; ?></td>
														<td class="<? echo $color; ?>"><? echo $activetext; ?></td>
														<td>
															<a href="edituser.php?id=<? echo $id; ?>" class="bs-tooltip" title="edit" data-placement="top"><button class="btn btn-xs btn-warning"><i class="icon-pencil"></i> </button></a>
															<a href="systemusers.php?active=<? echo $id; ?>" class="bs-tooltip" title="<? echo $tooltip; ?>" data-placement="top"><button class="btn btn-xs btn-<? echo $iconcolor; ?>"><i class="icon-<? echo $icon; ?>"></i> </button></a>
														</td>
													</tr>
												<? } ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							</div>
							
							<div class="widget-foot">
							
							</div>
						
							</div>
						</div>
						<!-- end table -->
						</div>
						<!-- table and form ends -->
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      <? include_once "inc/foot.php"; ?>
      
	</body>	
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>