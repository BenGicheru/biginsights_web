<?php
include_once "inc/head.php";
require_once("../includes/inc_files.php");
$page_title = "Form Elements"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="Form Elements";
include_once "inc/getforminfo.php";

// add an element
if (isset($_POST['addelement'])) {

	include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
	$_POST= str_replace("'", "''", $_POST);
	$_POST= str_replace('"', '\"', $_POST);
		
    $newname=$_POST['elementname'];
	$newtype=$_POST['type'];
	$newname = preg_replace('/[^A-Za-z0-9]/', '', $newname); // Only letters and numbers accepted
	$newname = strtolower($newname); // convert it to lowercase
	
	// check if the name already exists
	$resulttaken=mysqli_query($link, "select * from `elements` WHERE name='$newname' AND formid='$thisformgenkey'");
	if (mysqli_num_rows($resulttaken)==0) {
		$sql = "INSERT INTO `elements` SET name='$newname', type='$newtype', formid='$thisformgenkey', subof='0', min='', max='', minlength='', maxlength='', deleted='0'";
		mysqli_query($link, $sql) or die('Error, query failed');
		header("location:elements.php?biginsightsid=".$thisformgenkey);
	} else {
		$note="The name you picked is taken, please pick a unique name";
	}
}

// add sub-element
if (isset($_POST['addsub'])) {

	include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
	$_POST= str_replace("'", "''", $_POST);
	$_POST= str_replace('"', '\"', $_POST);
	
    $newlabel=$_POST['elementlabel'];
	$newsubof=$_POST['mainelement'];
	
	// check if the name already exists
	if (mysqli_num_rows($resulttaken)==0) {
		$sql = "INSERT INTO `elements` SET label='$newlabel', formid='$thisformgenkey', subof='$newsubof', min='', max='', minlength='', maxlength='', deleted='0'";
		mysqli_query($link, $sql) or die('Error, query failed');
		header("location:elements.php?biginsightsid=".$thisformgenkey);
	}
}

// delete element
if ((isset($_GET['delete'])) && (is_numeric($_GET['delete']))) {
    $deleteid=$_GET['delete'];

	$sql = "UPDATE elements SET deleted='1' WHERE id='$deleteid' AND formid='$thisformgenkey'";
    mysqli_query($link, $sql);
    header("location:elements.php?biginsightsid=".$thisformgenkey);
}

// edit element
if (isset($_POST['saveelement'])) {
	include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
	$_POST= str_replace("'", "''", $_POST);
	$_POST= str_replace('"', '\"', $_POST);
	
	$nid=$_POST['elementid'];
	$nname=$_POST['name'];
	$nname = preg_replace('/[^A-Za-z0-9]/', '', $nname); // Only letters and numbers accepted
	$nname = strtolower($nname); // convert it to lowercase
	$nlabel=$_POST['label'];
	$nplaceholder=$_POST['placeholder'];
	$ninputtype=$_POST['inputtype'];
	$nminlength=$_POST['minlength'];
	$nmaxlength=$_POST['maxlength'];
	$nmin=$_POST['min'];
	$nmax=$_POST['max'];
	$ndateformat=$_POST['dateformat'];
	$ntimeformat=$_POST['timeformat'];
	$nextensions=$_POST['extensions'];
	$nonvalue=$_POST['onvalue'];
	$noffvalue=$_POST['offvalue'];
	$nrequired=$_POST['required'];
	
	if ($ninputtype=="any") {
		$nurl="";
		$nemail="";
		$nnumber="";
		$ndigits="";
		$ncreditcard="";
	} else if ($ninputtype=="url") {
		$nurl="on";
		$nemail="";
		$nnumber="";
		$ndigits="";
		$ncreditcard="";
	} else if ($ninputtype=="email") {
		$nurl="";
		$nemail="on";
		$nnumber="";
		$ndigits="";
		$ncreditcard="";
	} else if ($ninputtype=="number") {
		$nurl="";
		$nemail="";
		$nnumber="on";
		$ndigits="";
		$ncreditcard="";
	} else if ($ninputtype=="digits") {
		$nurl="";
		$nemail="";
		$nnumber="";
		$ndigits="on";
		$ncreditcard="";
	} else if ($ninputtype=="creditcard") {
		$nurl="";
		$nemail="";
		$nnumber="";
		$ndigits="";
		$ncreditcard="on";
	}
	
	$sql = "UPDATE elements SET name='$nname', label='$nlabel', placeholder='$nplaceholder', minlength='$nminlength', maxlength='$nmaxlength',";
	$sql = $sql." min='$min', max='$max', dateformat='$ndateformat', timeformat='$ntimeformat', extensions='$nextensions',";
	$sql = $sql." onvalue='$nonvalue', offvalue='$noffvalue', required='$nrequired',";
	$sql = $sql." url='$nurl', email='$nemail', number='$nnumber', digits='$ndigits', creditcard='$ncreditcard' ";
	$sql = $sql." WHERE id='$nid' AND formid='$thisformgenkey'";
    mysqli_query($link, $sql);
    header("location:elements.php?biginsightsid=".$thisformgenkey);
}
?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-cogs icon-large"></i> <? echo $thisformname; ?></h3> 	
						<div class="pull-right">
						   <a href="data.php?biginsightsid=<? echo $thisformgenkey; ?>"> <button type="button" class="btn btn-info"><i class="icon-bar-chart"></i> Responses</button></a>
							<a href="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-cog"></i> Settings</button></a>
							<a target="_blank" href="viewform.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-external-link"></i> Preview Survey</button></a>
							
							<ul class="share-buttons">
						<li><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>&t=" title="Share on Facebook" target="_blank"><img src="img/si/Facebook.png"></a></li>
  <li><a href="https://twitter.com/intent/tweet?source=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>&text=:%20http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>" target="_blank" title="Tweet"><img src="img/si/Twitter.png"></a></li>
  <li><a href="https://plus.google.com/share?url=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>" target="_blank" title="Share on Google+"><img src="img/si/Google+.png"></a></li>
  <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>&title=&summary=&source=http%3A%2F%2Fbiginsights.io/my-insights/viewform.php?biginsightsid=<?php echo $thisformgenkey;?>" target="_blank" title="Share on LinkedIn"><img src="img/si/LinkedIn.png"></a></li>
						</ul>
							
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->

				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<div class="col-md-12">
							<? if (isset ($note)) { ?>
							<div class="alert alert-dismissable alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="icon-warning-sign"></i> <? echo $note; ?>
							</div>
							<? } ?>
							<div class="widget">
							   <div class="widget-head">
								  <h5><i class="icon-th"></i> Form Fields</h5>
							   </div>
							   <div class="widget-body no-padd">
									<div class="table-responsive">
									  <table class="table table-hover table-bordered ">
									   <thead>
										 <tr>
										   <th>ID</th>
										   <th>Category</th>
										   <th>Question/Label</th>
										   <th>Type</th>
										   <th></th>
										 </tr>
									   </thead>
									   <tbody>
									   <?
										// get elements list
										$resultelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0' ORDER BY serial ASC");
										while ($rowelement=mysqli_fetch_array($resultelement)){
											$elementid=$rowelement['id'];
											$elementname=$rowelement['name'];
											$elementlabel=$rowelement['label'];
											$elementtype=$rowelement['type'];
										?>
										 <tr>
										   <td><? echo $elementname; ?></td>
										   <td class="success">Main</td>
										   <td><? echo $elementlabel; ?></td>
										   <td><? echo $elementtype; ?></td>
										   <td>

											   <a href="#<? echo $elementid; ?>Modal" class="bs-tooltip" title="edit" data-placement="top" data-toggle="modal"><button class="btn btn-xs btn-warning"><i class="icon-pencil"></i> </button></a>
											   <a href="elements.php?biginsightsid=<? echo $thisformgenkey; ?>&delete=<? echo $elementid; ?>" class="bs-tooltip" title="delete" data-placement="top"><button class="btn btn-xs btn-danger"><i class="icon-remove"></i> </button></a>
										   
										   </td>
										 </tr>
										 <? if (($elementtype=="radio") xor ($elementtype=="dropdown")) {
										 // get subelements list
										$resultsubelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='$elementid' AND deleted='0' ORDER BY serial ASC");
										while ($rowsubelement=mysqli_fetch_array($resultsubelement)){
											$subelementid=$rowsubelement['id'];
											$subelementlabel=$rowsubelement['label'];
										 ?>
										 <!--sub element-->
										 <tr>
										   <td></td>
										   <td class="danger"><? echo $elementname; ?></td>
										   <td><? echo $subelementlabel; ?></td>
										   <td>sub-element</td>
										   <td>

											   <a href="#<? echo $subelementid; ?>Sub" class="bs-tooltip" title="edit" data-placement="top" data-toggle="modal"><button class="btn btn-xs btn-warning"><i class="icon-pencil"></i> </button></a>
											   <a href="elements.php?biginsightsidid=<? echo $thisformgenkey; ?>&delete=<? echo $subelementid; ?>" class="bs-tooltip" title="delete" data-placement="top"><button class="btn btn-xs btn-danger"><i class="icon-remove"></i> </button></a>
										   
										   </td>
										 </tr>
										 <!-- end subelement -->
										 <? } // end subelements while ?>
										 <? } // end if ?>
										 <? } // end main elements while statement ?>

									   </tbody>
									 </table>
								 </div>
							   </div>
							   
							   <div class="widget-foot">

							   </div>
							   <!-- end elements list -->
							   <div class="row">
							   <div class="col-md-12">
							   <div class="col-md-6">
							   <!-- add new element -->
							   <div class="widget">
									<div class="widget-head">
										<h5><i class="icon-plus"></i> Add Field</h5>
									</div>
									   <div class="widget-body">
										  <form class="form-horizontal" id="ValidForm" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
										  
											<div class="form-group">
											  <label class="col-lg-2 control-label">ID</label>
											  <div class="col-lg-10">
												<input type="text" name="elementname" class="form-control" placeholder="Name">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-lg-2 control-label">Type</label>
											  <div class="col-lg-10">
												<select class="form-control" name="type">
												  <option value="textbox">Text Box</option>
												  <option value="textarea">Text Area</option>
												  <option value="radio">Radio</option>
												  <option value="dropdown">Drop Down Menu</option>
												  <option value="upload">Upload Box</option>
												  <option value="checkbox">Check Box</option>
												  <option value="star">Star Rating</option>
												  <option value="toggle">Toggle Button</option>
												  <option value="password">Password Text Box</option>
												  <option value="datepicker">Date Picker</option>
												  <option value="timepicker">Time Picker</option>
												</select>
											  </div>
											</div>
																
											<div class="form-group">
											  <div class="col-lg-offset-2 col-lg-10">
												<button type="submit" class="btn btn-primary" name="addelement">Add</button>
											  </div>
											</div>
										  </form>
									   </div>
									   
									   <div class="widget-foot">
									   
									   </div>
								   </div>
								  <!-- add element end -->
								  <?
								  // get elements with sub potential
								  $resultsubable=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0' AND (type='radio' OR type='dropdown') ORDER BY serial ASC");
								  if (mysqli_num_rows($resultsubable)>0) {
								  ?>
								  <!-- add sub element -->
							   <div class="widget">
									<div class="widget-head">
										<h5><i class="icon-plus"></i> Add Field Options</h5>
									</div>
									   <div class="widget-body">
										  <form class="form-horizontal" id="ValidForm3" role="form" method="post" action="elements.php?biginsightsid=<? echo $thisformgenkey; ?>">
										  
											<div class="form-group">
											  <label class="col-lg-2 control-label">Label</label>
											  <div class="col-lg-10">
												<input type="text" name="elementlabel" class="form-control" placeholder="Label">
											  </div>
											</div>
											
											<div class="form-group">
											  <label class="col-lg-2 control-label">Element</label>
											  <div class="col-lg-10">
												<select class="form-control" name="mainelement">
												<?
												while ($rowsubable=mysqli_fetch_array($resultsubable)){
													$mainid=$rowsubable['id'];
													$mainname=$rowsubable['name'];
												?>
												  <option value="<? echo $mainid; ?>"><? echo $mainname; ?></option>
												<? } ?>
												</select>
											  </div>
											</div>
																
											<div class="form-group">
											  <div class="col-lg-offset-2 col-lg-10">
												<button type="submit" class="btn btn-primary" name="addsub">Add </button>
											  </div>
											</div>
										  </form>
									   </div>
									   
									   <div class="widget-foot">
									   
									   </div>
								   </div>
								   <!-- end sub elements -->
								   <? } // close if statement ?>
								</div>
								<div class="col-md-6">
							<div class="widget task-widget jstasks">

								<!-- elements order starts -->
								<!-- Widget head -->
								<div class="widget-head">
									<h5 class="pull-left"><i class="icon-list-ol"></i> Order of Fields</h5>	
									<div class="widget-head-btns pull-right">
										<a href="#" class="wclose"><i class="icon-remove"></i></a>
									</div>
									<div class="clearfix"></div>
								</div>

								<!-- Order body -->
								<div class="widget-body">
									  	  
								
								<!-- collapse starts -->
					
								<div class="panel-group" id="accordion">
								  <div class="panel panel-default">
								  
									<!-- Heading -->
									<div class="panel-heading">
									  <h5 class="panel-title">
										<!-- Icon, Title and Label -->
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseMain">
										  <i class="icon-plus"></i> Main <span class="label label-warning pull-right"><? echo $formmainelements; ?> fields</span>
										</a>
									  </h5>
									</div>
									<div id="collapseMain" class="panel-collapse collapse in">
									
									  <!-- Body content -->
									  <!--  section -->
									  <div class="panel-body noty-message">
										<ul class="sortable list" id="sortable">
											 <?
											// get elements list
											$resultelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0' ORDER BY serial ASC");
											while ($rowelement=mysqli_fetch_array($resultelement)){
												$elementid=$rowelement['id'];
												$elementname=$rowelement['name'];
												$elementtype=$rowelement['type'];
											?>
											 <li class="task-normal" id="element_<? echo $elementid; ?>"><? echo $elementname." (".$elementtype.")"; ?> </li>
											<? } ?>
										  </ul>
									  </div>
									  
									</div>
								  </div>
								  <?
										// get list of elements with sub-elements potenatial
										$resultmainelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0' AND (type='radio' OR type='dropdown') ORDER BY serial ASC");
										while ($rowmainelement=mysqli_fetch_array($resultmainelement)){
											$mainelementid=$rowmainelement['id'];
											$mainelementname=$rowmainelement['name'];
											$mainelementtype=$rowmainelement['type'];
								  ?>
								  <div class="panel panel-default">
								  
									<!-- Heading -->
									<div class="panel-heading">
									  <h5 class="panel-title">
										<!-- Icon, Title and Label -->
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse<? echo $mainelementid; ?>">
										  <i class="icon-plus"></i> <? echo $mainelementname; ?> <span class="label label-success pull-right"><? echo $mainelementtype; ?></span>
										</a>
									  </h5>
									</div>
									<div id="collapse<? echo $mainelementid; ?>" class="panel-collapse collapse">
									
									  <!-- Body content -->
									  <!--  section -->
									  <div class="panel-body noty-message">
										<ul class="sortable list" id="sortable">
											<?
											// get subelements list
											$resultsubelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='$mainelementid' AND deleted='0' ORDER BY serial ASC");
											while ($rowsubelement=mysqli_fetch_array($resultsubelement)){
												$subelementid=$rowsubelement['id'];
												$subelementlabel=$rowsubelement['label'];
											?>
											 <li class="task-normal" id="element_<? echo $subelementid; ?>"><? echo $subelementlabel; ?> </li>
											<? } ?>
										  </ul>
									  </div>
									  
									</div>
								  </div>
								  <? } // end parent while statement?>

								</div>
							
							<!--collapse ends -->

							</div>
							</div>							
							<!-- Order ends -->
							</div><!-- end col-md-6 -->
							
							</div><!-- end col-md-12-->
							</div><!-- end row -->
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      <? include_once "inc/foot.php"; ?>
      <? include_once "inc/modals.php"; ?>
	</body>	
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>
