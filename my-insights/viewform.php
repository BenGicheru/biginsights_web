<?php
error_reporting(0);
include_once "inc/browserinfo.php";
//referer info
if (!isset ($_POST['submitdata'])) {
define("REFERER", $_SERVER['HTTP_REFERER']);
$referer = REFERER;
$timestarted =time();
}
include 'inc/connect.php';
require_once("../includes/inc_files.php"); 
include_once "inc/getliveinfo.php";
include_once "inc/livehead.php";


//browser, os info
$browserobj = new OS_BR();
$browser = $browserobj->showInfo('browser');
$version = $browserobj->showInfo('version');
$os =$browserobj->showInfo('os');
//location of visitor
define("API_KEY","366d7ab44fd2a93c00b9958ddcde4f8d8298885801dd9e4dd348bcf0bfcfa65b");

        $ip = $_SERVER['REMOTE_ADDR'];
        if(is_private_ip($ip)) {
                //return $ip." - PRIVATE NETWORK";
        }
			$data = file_get_contents('http://api.ipinfodb.com/v3/ip-city/?key='.API_KEY.'&ip='.$ip.'&format=json');
			$data = json_decode($data,true);
                if(isset($data['cityName']) && trim(trim($data['cityName']),'-')) {
                //$location = $data['cityName'].", ".$data['regionName'].", ".$data['countryName'];
				///get loc details
				$city =$data['cityName'];
				$region=$data['regionName'];
				$country=$data['countryName'];
				$latitude=$data['latitude'];
				$longitude=$data['longitude'];

				
				
				}
        else {
                //If ipinfodb.com fails to return the location for some reason
                $data = file_get_contents('http://ip-api.com/json');
                $data = json_decode($data,true);
                //$location = $data['city'].", ".$data['regionName'].", ".$data['country'];
				$city =$data['city'];
				$region=$data['regionName'];
				$country=$data['country'];
				$latitude=$data['lat'];
				$longitude=$data['lon'];
				
        }



function is_private_ip($ip) {
        if (empty($ip) or !ip2long($ip)) {
                return false;
        }
        //Private Networks
        $private_ips = array (
                array('10.0.0.0','10.255.255.255'),array('172.16.0.0','172.31.255.255'),array('192.168.0.0','192.168.255.255')         );
        $ip = ip2long($ip);
        foreach ($private_ips as $ipr) {
                $min = ip2long($ipr[0]);
                $max = ip2long($ipr[1]);
                if (($ip >= $min) && ($ip <= $max)) return true;
        }
        return false;
}



if (isset ($_POST['submitdata'])) {
	$submittedon = time();
	$finalreferer = $_POST['preferer'];
	$started = $_POST['started'];
	// sanitize submitted data
	include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
	$_POST= str_replace("'", "''", $_POST);
	$_POST= str_replace('"', '\"', $_POST);
	
	// add new submission
	$sql = "INSERT INTO `submissions` SET formid='$thisformgenkey', invitationid='$thisinvitationid', date=NOW(), referer='$finalreferer', city='$city', region='$region', country='$country', latitude='$latitude', longitude='$longitude', browser='$browser', browserversion='$version', os='$os', started='$started', ended='$submittedon'";
	mysqli_query($link, $sql) or die('Error, query failed');
	
	// get this submission id (latest submission)
	$resultlastsubmission=mysqli_query($link, "select * from `submissions` ORDER BY id DESC LIMIT 1");
	while ($rowlastsubmission=mysqli_fetch_array($resultlastsubmission)){
		$thissubmissionid=$rowlastsubmission['id'];
	}
	
	// get elements list
	$resultsubmit=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0' ORDER BY serial ASC");
	while ($rowsubmit=mysqli_fetch_array($resultsubmit)){
		$selementid=$rowsubmit['id'];
		$selementtype=$rowsubmit['type'];
		$submittedvalue=$_POST[$selementid];
		
		if ($selementtype=="upload") {
			$tmp = $_FILES[$selementid]['tmp_name']; 
			$org = $_FILES[$selementid]['name'];
			$rand=rand(00000000000,9999999999);
			$directory = "formuploads/"; // Upload files to here.
			$ext=explode(".", $org);
			$extension=$ext[1];
			$extension = strtolower($extension); // convert it to lowercase
			$filename=$rand.".".$extension;
			$targetpath = $directory.$rand.".".$extension; // Filename and path
			
			// forbidden files
			if (($extension != 'php') && ($extension != 'php3') && ($extension != 'php5') && ($extension != 'exe') && ($extension != '')) {
				move_uploaded_file($tmp, $targetpath);
				$submittedvalue=$filename;
			}
		}
		
		$sql = "INSERT INTO `subfields` SET formid='$thisformgenkey', submissionid='$thissubmissionid', elementid='$selementid', value='$submittedvalue'";
		mysqli_query($link, $sql) or die('Error, query failed');	
	}
	header("location:viewformfinish.php?biginsightsid=".$thisformgenkey."&thankyou");
}
?>	
	<body>
		<? // if the form is closed, warn the owner
		if ($thisformclosed !="0000-00-00") {
		?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="icon-warning-sign"></i> This form is closed and not publicly visible, you can change the status in the <a href="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>">form settings page</a>
		</div>
		<? } ?>
        <!-- instructions start -->
		<div class="container">
					<div class="page-content page-form">
						
						<div class="widget well">
                           <div class="widget-body">
                             <? echo $thisforminstructions; ?>
                           </div>
                        </div>
						
					</div>
				</div> 
		<!-- form starts -->
				
				<div class="container">
					<div class="page-content page-form">
							<? if (isset ($note)) { ?>
							<div class="alert alert-dismissable alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="icon-warning-sign"></i> <? echo $note; ?>
							</div>
							<? } ?>
						<div class="widget">
                           <div class="widget-body">
                              <form class="form-horizontal well" role="form" id="ValidForm" method="post" action="viewform.php?biginsightsid=<? echo $thisformgenkey; ?>&key=<? echo $thisformkey; ?>" enctype="multipart/form-data">
							  
							  <input type="hidden" name="preferer" value="<?php echo $referer; ?>" />
							  <input type="hidden" name="started" value="<?php echo $timestarted; ?>" />
								<?
								// get elements list
								$resultelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='0' AND deleted='0' ORDER BY serial ASC");
								while ($rowelement=mysqli_fetch_array($resultelement)){
									$elementid=$rowelement['id'];
									$elementname=$rowelement['name'];
									$elementlabel=$rowelement['label'];
									$elementplaceholder=$rowelement['placeholder'];
									$elementdateformat=$rowelement['dateformat'];
									$elementtimeformat=$rowelement['timeformat'];
									$elementtype=$rowelement['type'];
									$elementonvalue=$rowelement['onvalue'];
									$elementoffvalue=$rowelement['offvalue'];
								
								if ($elementtype=="textbox") {
								?>
                                <div class="form-group">
                                  <label class="col-lg-2 control-label"><? echo $elementlabel; ?></label>
                                  <div class="col-lg-10">
                                    <input type="text" name="<? echo $elementid; ?>" class="form-control" placeholder="<? echo $elementplaceholder; ?>">
                                  </div>
                                </div>
								<? } else if ($elementtype=="textarea") { ?>
								<div class="form-group">
                                  <label class="col-lg-2 control-label"><? echo $elementlabel; ?></label>
                                  <div class="col-lg-10">
                                    <textarea name="<? echo $elementid; ?>" class="form-control" rows="3" placeholder="<? echo $elementplaceholder; ?>" /></textarea>
                                  </div>
                                </div>
								<? } else if ($elementtype=="password") { ?>
                                <div class="form-group">
                                  <label class="col-lg-2 control-label"><? echo $elementlabel; ?></label>
                                  <div class="col-lg-10">
                                    <input type="password" name="<? echo $elementid; ?>" class="form-control" placeholder="<? echo $elementplaceholder; ?>">
                                  </div>
                                </div>
								<? } else if ($elementtype=="star") { ?>
								<div class="form-group">
                                  <label class="col-lg-2 control-label"><? echo $elementlabel; ?></label>
                                  <div class="col-lg-10">
                                    <select id="<? echo $elementid; ?>" name="<? echo $elementid; ?>">
										<option value="0"></option>
										<option value="1"></option>
										<option value="2"></option>
										<option value="3"></option>
										<option value="4"></option>
										<option value="5"></option>
									</select>
									<div class="rateit" data-rateit-backingfld="#<? echo $elementid; ?>"></div>
                                  </div>
                                </div>
								<? } else if ($elementtype=="upload") { ?>
								<div class="form-group">
                                  <label class="col-lg-2 control-label"><? echo $elementlabel; ?></label>
                                  <div class="col-lg-10">
                                    <input type="file" name="<? echo $elementid; ?>" class="form-control">
                                  </div>
                                </div>
								<? } else if ($elementtype=="timepicker") { ?>
								<div class="form-group">
                                  <label class="col-lg-2 control-label"><? echo $elementlabel; ?></label>
                                  <div class="col-lg-10">
									<div id="datetimepicker<? echo $elementid; ?>" class="input-append">
										<span class="add-on">
										  <i data-time-icon="icon-time" data-date-icon="icon-time">
										  </i>
										</span>
										<input data-format="hh:mm:ss" name="<? echo $elementid; ?>" type="text">
									</div>
									<script type="text/javascript">
									  $(function() {
										$('#datetimepicker<? echo $elementid; ?>').datetimepicker({
										<? if ($elementtimeformat=="12h") { ?>
										  pick12HourFormat: true,
										<? } else { ?>
										  pick12HourFormat: false,
										<? } ?>
										  pickDate: false
										});
									  });
									</script>
                                  </div>
                                </div>
								<? } else if ($elementtype=="datepicker") { ?>
								<div class="form-group">
                                  <label class="col-lg-2 control-label"><? echo $elementlabel; ?></label>
                                  <div class="col-lg-10">
									<div id="datetimepicker<? echo $elementid; ?>" class="input-append">
										<span class="add-on">
										  <i data-time-icon="icon-calendar" data-date-icon="icon-calendar">
										  </i>
										</span>
										<input data-format="<? echo $elementdateformat; ?>" name="<? echo $elementid; ?>" type="text">
									</div>
									<script type="text/javascript">
									  $(function() {
										$('#datetimepicker<? echo $elementid; ?>').datetimepicker({
										  format: '<? echo $elementdateformat; ?>',
										  pickTime: false
										});
									  });
									</script>
                                  </div>
                                </div>
								<? } else if ($elementtype=="checkbox") { ?>
                                <div class="form-group">
                                  <label class="col-lg-2 control-label"></label>
                                  <div class="col-lg-10">
                                    <label class="checkbox-inline">
                                      <? echo $elementlabel; ?> <input type="checkbox" name="<? echo $elementid; ?>" id="inlineCheckbox1">
                                    </label>
                                  </div>
                                </div>
								<? } else if ($elementtype=="radio") { ?>
                                <div class="form-group">
                                  <label class="col-lg-2 control-label"><? echo $elementlabel; ?></label>
                                  <div class="col-lg-10">
								<?
										 // get subelements list
										$resultsubelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='$elementid' AND deleted='0' ORDER BY serial ASC");
										while ($rowsubelement=mysqli_fetch_array($resultsubelement)){
											$subelementid=$rowsubelement['id'];
											$subelementlabel=$rowsubelement['label'];
								?>
                                    <div class="radio">
                                      <label>
										<? echo $subelementlabel; ?>
                                        <input type="radio" name="<? echo $elementid; ?>" id="optionsRadios1" value="<? echo $subelementid; ?>">
                                      </label>
                                    </div>
								<? } ?>
                                  </div>
                                </div>
								<? } else if ($elementtype=="toggle") { ?>
								<div class="form-group">
                                  <label class="col-lg-2 control-label"></label>
                                  <div class="col-lg-10">
                                    <label class="checkbox-inline">
										<div class="make-switch" data-on="success" data-off="danger" data-on-label="<? echo $elementonvalue; ?>" data-off-label="<? echo $elementoffvalue; ?>">
											<input type="checkbox" name="<? echo $elementid; ?>">
											</div>
										<strong><? echo $elementlabel; ?></strong>
									</label><br>
                                  </div>
                                </div>
								<? } else if ($elementtype=="dropdown") { ?>
                                
                                <div class="form-group">
                                  <label class="col-lg-2 control-label"><? echo $elementlabel; ?></label>
                                  <div class="col-lg-10">
                                    <select name="<? echo $elementid; ?>" class="form-control">
									<?
										 // get subelements list
										$resultsubelement=mysqli_query($link, "select * from `elements` WHERE formid='$thisformgenkey' AND subof='$elementid' AND deleted='0' ORDER BY serial ASC");
										while ($rowsubelement=mysqli_fetch_array($resultsubelement)){
											$subelementid=$rowsubelement['id'];
											$subelementlabel=$rowsubelement['label'];
									?>
                                      <option value="<? echo $subelementid; ?>"><? echo $subelementlabel; ?></option>
									<? } ?>
                                      
                                    </select>
                                  </div>
                                </div>
								<? } // end if ?>
								<? } // end while ?>
                                <div class="form-group">
                                  <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" class="btn btn-success pull-right" name="submitdata"><? echo $thisformsubmitstring; ?></button>
                                  </div>
                                </div>
                              </form>
                           </div>
                        </div>
						
					</div>
				</div>                
<? include_once "inc/livefoot.php"; ?>

