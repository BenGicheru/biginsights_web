<?php

require_once __DIR__.'/vendor/autoload.php';
require_once("genkey/genkeyv2.php");

include_once "inc/headstats.php";
require_once("../includes/inc_files.php");
$pagetitle="Responses and Statistics";
$page_title = "Forms"; 
require_once("../includes/themes/".THEME_NAME."/qheader.php");
include_once "inc/getmobileforminfo.php";
$myuserid = $user->id;

$biginsights = "@biginsights.io";

if ((isset($_GET['biginsightsid']))) {
	$thisformgenkey=$_GET['biginsightsid'];
	} else {
		header("location:mobidash.php");
	}
	
	// create a App
if (isset($_POST['createapp'])) {

	include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
	$_POST= str_replace("'", "''", $_POST);
	$_POST= str_replace('"', '\"', $_POST);
	
    $myappname=$_POST['appname'];
	$mypackagename=$_POST['packagename'];
	$generatedkey = keygen(30);
	$sql = "INSERT INTO `mobiandroidapps` SET user_id='$myuserid', appname='$myappname', package_name='$mypackagename', apikey='$generatedkey'";
	mysqli_query($link, $sql) or die('Error, query failed');
	header("location:".$redirect);
}
//regenerate app key
if (isset($_GET['gen'])) {
    $myappname=$_GET['gen'];
	$generatedkey = keygen(30);
	$sql = "UPDATE `mobiandroidapps` SET apikey='$generatedkey' WHERE appname='$myappname'";
	mysqli_query($link, $sql) or die('Error, query failed');
	header("location:".$redirect);
}


if (isset($_POST['pushnow'])) {	
    $appapikey=$_POST['apikey'];
	
	$pushsql = "select * from `mobiappusers` WHERE androidapikey='$appapikey' ORDER BY id DESC";
	$pushquery= mysqli_query($link, $pushsql) or die('Error, query failed');
	$no_users = mysqli_num_rows($pushquery);
	
	if($no_users>0) {
	$xmppusers = array();
	while( $results = mysqli_fetch_array($pushquery))
	{
		array_push($xmppusers, $results);
	}
	$XMPP = new \BirknerAlex\XMPPHP\XMPP('biginsights.io', 5222, 'ben@biginsights.io', ',.bigio.,', 'PHP');
	$XMPP->connect();
		$XMPP->processUntil('session_start', 10);
		$XMPP->presence();
	for($i=0; $i<$no_users; $i++){
		$xmppusername=$xmppusers[$i]['xmppusername'];
		//$thisformname = "New York Weekly Survey";
		//$thisformgenkey = "e5d55a27fa53754a8e1b6196d1e0ad1e";		
		$XMPP->message($xmppusername.$biginsights, $thisformname.'-'.$thisformgenkey, 'chat');
	}	
	$XMPP->disconnect();
	}
	header("location:".$redirect);
	//echo $no_users;
}
?>
<?php
$queryapps=mysqli_query($link, "select * from `mobiandroidapps` WHERE user_id='$myuserid' ORDER BY id DESC");
$no_apps = mysqli_num_rows($queryapps);

?>

<?php
//sendpush();
function sendpush(){
$XMPP->connect();
$XMPP->processUntil('session_start', 10);
$XMPP->presence();
//$XMPP->message('benson@biginsights.io', 'Hello, how are you?', 'chat');
$XMPP->message('aG0jjT8lm9eAZXKfwEu6Jj5DD'.$biginsights, $thisformname.'-'.$thisformgenkey, 'chat');

$XMPP->disconnect();

}
?>

<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-list-alt icon-large"></i><?php echo $thisformname?> <span>Mobile Survey</span></h3> 	
						<div class="newsurvey-left">
							<a href="#myModal" class="btn btn-info" data-toggle="modal"> <i class="icon-plus"></i> ADD ANDROID APP</a>
							
							<a href="mobileinsights.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-cogs"></i> Responses</button></a>
							<a href="mobiquestions.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-cogs"></i> Edit Survey</button></a>
						</div>	
						<div class="clearfix"></div>
						
						
					</div>
				</div>
				<!-- Black block ends -->

				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<div class="col-md-12">
								<!-- forms -->
								<div class="widget contacts-widget">

									<!-- Widget head -->
									<div class="widget-head">
										<h5 class="pull-left"><i class="icon-list-alt"></i>Push <?php echo $thisformname?></h5>	
										<div class="widget-head-btns pull-right">
											<a href="#" class="wclose"><i class="icon-remove"></i></a>
										</div>
										<div class="clearfix"></div>
									</div>

									<!-- Widget body -->
									<div class="widget-body 300-scroll">

										<ul class="list-unstyled">
										
											<!-- number of forms -->
											<?
											if ($myadmin==0) { // if i am not admin
												// get list of forms that are public or I have created
												//$resultforms=mysqli_query($link, "select * from `forms` where createdby='$myid' OR //public='1' ORDER BY id DESC");
												$resultforms=mysqli_query($link, "select * from `forms` where createdby='$myid' ORDER BY id DESC");
												
											} else { // if i am admin
												// get all forms
												$resultforms=mysqli_query($link, "select * from `forms` ORDER BY id DESC");
											}
											$thisformscount=mysqli_num_rows($resultforms);
											?>
											<li class="contact-alpha">
												Push Notifications to your apps <span class="label label-info pull-right"><? echo $no_apps; ?> total Apps</span>
												<div class="clearfix"></div>
											</li>
											
											<? 
											
if($no_apps>0) {
	$appmetadata = array();
	while( $results = mysqli_fetch_array($queryapps))
	{
		array_push($appmetadata, $results);
	}
	for($i=0; $i<$no_apps; $i++){
		$appname=$appmetadata[$i]['appname'];
	    $packagename=$appmetadata[$i]['package_name'];
		$apikey=$appmetadata[$i]['apikey'];
		?>
								<li class="c-list">
												<!-- Contact pic -->
												
												<!-- Contact details -->
												<div class="contact-details">
													<!-- Contact name and number -->
													<div class="pull-left">
														<strong><a href="mobileinsights.php?biginsightsid=<? echo $thisformgenkey; ?>"><? echo $appname; ?></a></strong>
														<small>Package Name-   <? echo $packagename; ?></small> <br>
														<small>API KEY-&nbsp;&nbsp;<? echo $apikey; ?></small>
													</div>
													<!-- Call, Message and Email buttons -->
													<div class="pull-right">
													<form method="post" action="mobipush.php?biginsightsid=<? echo $thisformgenkey; ?>">
													<input type="hidden" name="apikey" value="<?php echo $apikey; ?>">
													<button type="submit" class="btn btn-primary pull-left" name="pushnow">Push Now</button>
													</form>
														<a href="mobiquestions.php?biginsightsid=<? echo $thisformgenkey; ?>" class="btn btn-primary btn-xs bs-tooltip" title="Edit App"><i class="icon-cogs"></i></a>
														<a href="mobipush.php?biginsightsid=<? echo $thisformgenkey; ?>&gen=<? echo $appname; ?>" class="btn btn-success btn-xs bs-tooltip" title="Regenerate Api Key"><i class="icon-cog"></i></a>
														<a href="mobileinsights.php?biginsightsid=<? echo $thisformgenkey; ?>" class="btn btn-warning btn-xs bs-tooltip" title="Submissions & Statistics"><i class="icon-bar-chart"></i></a>
													</div>
													<div class="clearfix"></div>
												</div>
											</li>
											
											
									<?php 	}	
									      }?>
	
										</ul>
										
									</div>

									<!-- Widget foot -->
									<div class="widget-foot">
									</div>

								</div>
						
							</div>
						</div>
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>

      
      <? include_once "inc/foot.php"; ?>
	  
	  
	  <!-- Modal -->
									<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										 <div class="modal-dialog">
										   <div class="modal-content">
											   <div class="modal-header">
												 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
												 <h4 class="modal-title">Add a New App</h4>
											   </div>
											   <div class="modal-body">
												 <form class="form-horizontal" id="ValidForm" role="form" method="post" action="mobipush.php">
									  
													<div class="form-group">
													  <label class="col-lg-2 control-label">App Name</label>
													  <div class="col-lg-10">
														<input type="text" name="appname" class="form-control" placeholder="Descriptive Name">
													  </div>
													</div>
													
													<div class="form-group">
													  <label class="col-lg-2 control-label">App's Package Name</label>
													  <div class="col-lg-10">
														<input type="text" name="packagename" class="form-control" placeholder="e.g com.example.app">
													  </div>
													</div>

													<hr>
													<div class="form-group">
													  <div class="col-lg-offset-2 col-lg-10">
														<button type="submit" class="btn btn-primary pull-right" name="createapp">Add App</button>
													  </div>
													</div>
												  </form>
											   </div>
										</div>
									</div>
