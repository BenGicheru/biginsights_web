<?php
include 'inc/connect.php';
require_once("../includes/inc_files.php");
$page_title = "Forms"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?


if(!$session->is_logged_in()) {redirect_to("../signin.php");}

else if ($session->is_logged_in()) { // if the user is logged on take them to login area
    header("location:dashboard.php");
}

// login action
if (isset($_POST['login'])) {
    // filter POST data
    include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
	$_POST= str_replace("'", "''", $_POST);
	$_POST= str_replace('"', '\"', $_POST);

    // grab login data
    $username=$_POST['username'];
	$username = strtolower($username); // convert it to lowercase
    $pass=$_POST['password'];
    $pazz= sha1($pass); // encrypt password

    // check data with database
    $result11=mysqli_query($link, "select * from `users` where username LIKE '$username' AND password LIKE '$pazz' AND active='1'");
    if (mysqli_num_rows($result11)!='0') {
        // grab user id
        while ($row11=mysqli_fetch_array($result11)) {
            $myid=$row11['id'];

			$_SESSION['uid']=$myid; // register session
            header("location:dashboard.php"); // redirect to login area
        }
    } else {
        $note="Wrong login information";
    }
}

// reset password
if (isset($_POST['resetpass'])){

	include 'inc/class-inputfilter.php';
	$myFilter = new InputFilter();
	$_POST = $myFilter->process($_POST);

	$thisusername=$_POST['username'];
	$thisemail=$_POST['email'];

	$result010=mysqli_query($link, "select * from `users` where email LIKE '$thisemail' AND username LIKE '$thisusername' AND active='1'");
	if (mysqli_num_rows($result010)!='0'){
		while ($row010=mysqli_fetch_array($result010)){
			$thisid=$row010['id'];
			$thisname=$row010['name'];
		}
		$newpass=rand(000000000,999999999);
		$encoded=sha1($newpass);

		// enter new password
		$sql = "UPDATE users SET password='$encoded' WHERE id=$thisid";
		mysqli_query($link, $sql);

		// prepare mailer
		$message="Hi ".$thisname.",\r\n\r\n";
		$message=$message."Your new password is ".$newpass."\r\n\r\n";
		$message=$message."QPanel";
		$to = $thisemail;
		$subject = "QPanel Password Reset";
		$from = "admin@x20labs.com";
		$headers = "From: admin@x20labs.com";

		// send email
		mail($to,$subject,$message,$headers);
		$note="Your new password was sent to your inbox";
	} else {
		$note="Wrong email/username combination";
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<!-- Title here -->
		<title>QPanel</title>

		<!-- Google web fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- jQuery -->
	  <script src="js/jquery.js"></script>
	  	<!-- Validate plugin -->
		<script src="js/jquery.validate.js"></script>
		<script>
		$().ready(function() {
			// validate form on keyup and submit
			$("#ValidForm").validate({
				rules: {
					username: "required",
					password: "required"
				},
				messages: {
					username: "Please enter your username",
					password: "Please enter your password"
				}
			});
			$("#ValidForm2").validate({
				rules: {
					username: "required",
					email: "required"
				},
				messages: {
					username: "Please enter your username",
					email: "Please enter your email"
				}
			});
		});
		</script>
	<!-- end validate plugin -->

		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">


		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="css/style.css" rel="stylesheet">

		<!--[if IE]>
			<link rel="stylesheet" type="text/css" href="css/style-ie.css" />
		<![endif]-->

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.jpg">
	</head>

	<body>

      <div class="out-container">

		 <div class="login-page">
			<div class="container">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav-justified">
				  <li class="active"><a href="#login" data-toggle="tab"><i class="icon-signin"></i> Login</a></li>
				  <li><a href="#forgotpass" data-toggle="tab"><i class="icon-exclamation"></i> Forgot Password</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane fade active in" id="login">
					<? if (isset($note)) { // if there's a note from the above script, show it here ?>
					<div class="alert alert-warning"><? echo $note; ?></div>
					<? } ?>
					<!-- Login form -->

					<form role="form" id="ValidForm" action="index.php" method="post">
					  <div class="form-group">
						<label for="username">Username</label>
						<input type="text" class="form-control" name="username" placeholder="username">
					  </div>
					  <div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control" name="password" placeholder="password">
					  </div>
					  <button type="submit" class="btn btn-danger btn-sm" name="login">Login</button>
					</form>

				  </div>

				  <div class="tab-pane fade" id="forgotpass">

					<!-- Contact Form -->

					<form role="form" id="ValidForm2" action="index.php" method="post">
					  <div class="form-group">
						<label for="username">Username</label>
						<input type="text" class="form-control" name="username" placeholder="username">
					  </div>
					  <div class="form-group">
						<label for="email">Email</label>
						<input type="text" class="form-control" name="email" placeholder="email">
					  </div>
					  <button type="submit" class="btn btn-danger btn-sm" name="resetpass">Reset Password</button>
					</form>

				  </div>
				</div>

			</div>
		 </div>

      </div>


	  <!-- Javascript files -->
	  <!-- Bootstrap JS -->
	  <script src="js/bootstrap.min.js"></script>
	  <!-- Respond JS for IE8 -->
	  <script src="js/respond.min.js"></script>
	  <!-- HTML5 Support for IE -->
	  <script src="js/html5shiv.js"></script>

	</body>
</html>
