<?php 
include_once "inc/head.php";
require_once("../includes/inc_files.php");
$page_title = "Email Settings"; require_once("../includes/themes/".THEME_NAME."/qheader.php"); ?>
<?
$pagetitle="Email Settings";

// only admin allowed
if ($myadmin!=1) {
	header("location:dashboard.php");
}

// update server settings
if (isset($_POST['savesettings'])) {
    include 'inc/class-inputfilter.php';
    $myFilter = new InputFilter();
    $_POST = $myFilter->process($_POST);
	$_POST= str_replace("'", "''", $_POST);
	$_POST= str_replace('"', '\"', $_POST);

    $newurl=$_POST['baseurl'];
	$newsendername=$_POST['sendername'];
	$newemail=$_POST['email'];

    $sql = "UPDATE `mailer` SET name='$newsendername', email='$newemail', url='$newurl' WHERE id='1'";
    mysqli_query($link, $sql) or die('Error, query failed');
	
    $note="Email Settings Updated Successfully";
	
	// get new form data
	$mailurl=$newurl;
	$mailname=$newsendername;
	$mailemail=$newemail;
}
?>
	
	<body>
	
      <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
            <!-- Sidebar ends -->
            
            <!-- Mainbar starts -->
            <div class="mainbar">
				
				<!-- Black block starts -->
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-forward icon-large"></i> Email Settings</h3> 	
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- Black block ends -->
				
				
				
				<!-- Content starts -->
				
				<div class="container">
					<div class="page-content">
						<div class="col-md-12">
						<? if (isset ($note)) { ?>
							<div class="alert alert-dismissable alert-info">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<i class="icon-warning-sign"></i> <? echo $note; ?>
							</div>
						<? } ?>
						<!-- form -->
							<div class="page-content page-form">
							
							<div class="widget">
								<div class="widget-head">
									<h5><i class="icon-forward"></i> Mail Sender's Settings</h5>
								</div>
								   <div class="widget-body">
									  <form class="form-horizontal" id="ValidForm" role="form" method="post" action="emailsettings.php">
	
										<div class="form-group">
										  <label class="col-lg-2 control-label">Base URL</label>
										  <div class="col-lg-10">
											<input type="text" name="baseurl" value="<? echo $mailurl; ?>" class="form-control" placeholder="http://www.example.com/">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Sender's Name</label>
										  <div class="col-lg-10">
											<input type="text" name="sendername" value="<? echo $mailname; ?>" class="form-control" placeholder="Name">
										  </div>
										</div>
										
										<div class="form-group">
										  <label class="col-lg-2 control-label">Sender's Email</label>
										  <div class="col-lg-10">
											<input type="text" name="email" value="<? echo $mailemail; ?>" class="form-control" placeholder="Email">
										  </div>
										</div>

										<hr>				
										<div class="form-group">
										  <div class="col-lg-offset-2 col-lg-10">
											<button type="submit" name="savesettings" class="btn btn-primary pull-right">Save Settings</button>
										  </div>
										</div>
									  </form>
								   </div>
								   
								   <div class="widget-foot">
								   
								   </div>
								</div>
							
							</div>
						<!-- end form -->
						</div>
					</div>
				</div>
				
				<!-- Content ends -->				
			   
            </div>
            <!-- Mainbar ends -->
            
            <div class="clearfix"></div>
         </div>
      </div>
      
      <? include_once "inc/foot.php"; ?>

      
	</body>	
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>