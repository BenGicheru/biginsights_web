<?php

if ((isset($_GET['biginsightsid']))) {
    //$thisformid=$_GET['id'];
	$thisformgenkey=$_GET['biginsightsid'];
    //$resultform=mysqli_query($link, "select * from `mobiquestions` WHERE msurvey_id='$thisformgenkey' LIMIT 1");
	} else {
		header("location:mobidash.php");
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Users - Update</title>
	<link href="mobi.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">

function Validate(){
	var valid = true;
	var message = '';
	var fname = document.getElementById("fname");
	var lname = document.getElementById("lname");
	
	
	
	if (valid == false){
		alert(message);
		return false;
	}
}

function GotoHome(){
	window.location = 'mobiquestions.php?biginsightsid=<?php echo $thisformgenkey?>';
}

</script>
</head>
<body>
 <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
     
            <div class="mainbar">
<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-bar-chart icon-large"></i> Responses & Statistics <span><? echo $thisformname; ?></span></h3> 	
						<div class="pull-right">
						
							<a href="mobiquestions.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-cogs"></i> Back to Survey</button></a>
							<a href="mobileinsights.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-cogs"></i> Responses</button></a>
							<a href="formsettings.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-cog"></i> Settings</button></a>
							
							
							
						</div>
						<div class="clearfix"></div>
					</div>
				</div>

				
				
				
								<div class="row" style="margin-left:15px; margin-right:15px;">
								<div class="col-md-12">
										
                                       <div class="widget">
                                          <div class="widget-head">
                                             <h5><i class="icon-bar-chart"></i>Edit Question<? echo $thisformname; ?></h5>
                                          </div>
                                <div class="wrapper">
		<div class="content" style="width: 100% !important;">
			<div>
			<form id="frmContact" method="POST" action="mobiquestions.php?biginsightsid=<?php echo $thisformgenkey?>" 		
					onSubmit="return Validate();">
				<input type="hidden" name="ContactID" 
				value="<?php echo (isset($gresult) ? $gresult["q_value"] :  ''); ?>" />
				<table>
					<tr>
						<td>
							<label for="fname">Type of Question: </label>
						</td>
						<td>
							<!--
							<input type="text" name="fname" 
							value="<?php echo (isset($gresult) ? $gresult["q_type"] :  ''); ?>" 
							id="fname" class="txt-fld"/>
							-->
							
							<select name="fname" class="txt-fld">
							<option value="radio">Radio Button</option>
							<option value="textbox">Text Box</option>
							<option value="rating5">Star Rating</option>
							<option value="numberscale">Number Scale</option>
							<option value="mapget">Select on Map</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<label for="fname">The Question itself: </label>
						</td>
						<td>
						
							<input type="text" name="lname" 
							value="<?php echo (isset($gresult) ? $gresult["q_wording"] :  ''); ?>" 
							id="lname" class="txt-fld"/>
			
						</td>
					</tr>
					<tr>
						<td>
							<label for="fname">Question Options(options seperated by comma): </label>
						</td>
						<td>
							<input type="text" name="ContactNo" 
							value="<?php echo (isset($gresult) ? $gresult["q_options"] :  ''); ?>" 
							class="txt-fld"/>
							
						</td>
					</tr>
				
				
				
				</table>
				<input type="hidden" name="action_type" value="<?php echo (isset($gresult) ? 'edit' :  'add');?>"/>
				<div style="text-align: center; padding-top: 30px;">
					<input class="buttons" type="submit" name="save" id="save" value="Save" />
					<input class="btn btn-primary" type="submit" name="save" id="cancel" value="Cancel" 
					onclick=" return GotoHome();"/>
				</div>
			</form>
			</div>
		</div>
	</div>
                                       </div>
                                      
								</div>
								</div>
								</div>		
				
	
</body>
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>
