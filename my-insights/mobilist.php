<?php

include_once 'mobiquestions.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Users</title>
	<link href="mobi.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function ConfirmDelete(){
	var d = confirm('Do you really want to delete this question?');
	if(d == false){
		return false;
	}
}
</script>
</head>
<body>
 <div class="out-container">
         <div class="outer">
            <!-- Sidebar starts -->
            <? include_once "inc/sidebar.php"; ?>
     
            <div class="mainbar">
	
				<div class="blue-block">
					<div class="page-title">
						<h3 class="pull-left"><i class="icon-bar-chart icon-large"></i> Responses & Statistics <span><? echo $thisformname; ?></span></h3> 	
						<div class="pull-right">
						<a href="mobipush.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-cogs"></i> Push To Apps</button></a>
						
							<a href="mobileinsights.php?biginsightsid=<? echo $thisformgenkey; ?>"><button type="button" class="btn btn-info"><i class="icon-cogs"></i> Responses</button></a>
							
							
							
						</div>
						<div class="clearfix"></div>
					</div>
				</div>


<div class="row" style="margin-left:15px; margin-right:15px;">
								<div class="col-md-12">
										
                                       <div class="widget">
                                          <div class="widget-head">
                                             <h5><i class="icon-bar-chart"></i>Edit Question<? echo $thisformname; ?></h5>
                                          </div>

	<div class="wrapper">
		<div class="content" >
			
			<div style="margin-bottom: 5px;">
			<form method="POST" action="mobiquestions.php?biginsightsid=<?php echo $thisformgenkey?>">
				<input type="text" id="searchText" name="searchText" style="width:50%" class="search" placeholder="Search Questions"/>
				<input type="hidden" name="action_type"  value="search"/>
				<!--<button type="button">Search</button>-->
				<input type="submit" class="buttons" value="Search">
			</form>
			<a href="mobiadd.php?biginsightsid=<?php echo $thisformgenkey?>" class="link-btn add-question">Add New Question</a>
			</div>
			<div style="max-height:400px; overflow:auto;">
			
			<table class="pbtable">
				<thead>
					<tr>
						<th>
							Question Type
						</td>
						<th>
							Question 
						</th>
						<th>
							Question Options
						</th>
						
						<th></th><th></th>
					</tr>
				</thead>
				
				<tbody>
					<?php foreach($contact_list as $contact) : ?>
						<tr>
							<td>
							    <?php 
								if($contact["q_type"]=="radio")
									echo "Radio Button";
								
								else if($contact["q_type"]=="textbox")
								echo "Text Box";
							
								else if($contact["q_type"]=="rating5")
									echo "Star Rating";
								
								else if($contact["q_type"]=="numberscale")
								echo "Number Scale";
							
								else if($contact["q_type"]=="mapget")
								echo "Select on Map";
								?>
							</td>
							<td>
								<?php echo $contact["q_wording"]; ?>
							</td>
							<td>
								<?php echo $contact["q_options"]; ?>
							</td>
							
							
							
							<td>
								<form method="post" action="mobiquestions.php?biginsightsid=<?php echo $thisformgenkey?>">
									<input type="hidden" name="ci" 
									value="<?php echo $contact["q_value"]; ?>" />
									<input type="hidden" name="action" value="edit" />
									<input type="submit" class="buttons" value="Edit" />
								</form> 
							</td>
							<td>
								<form method="POST" action="mobiquestions.php?biginsightsid=<?php echo $thisformgenkey?>" 
								onSubmit="return ConfirmDelete();">
									<input type="hidden" name="ci" 
									value="<?php echo $contact["q_value"]; ?>" />
									<input type="hidden" name="action" value="delete" />
									<input type="submit" class="btn btn-primary" value="Delete" />
								</form>
							</td>
						<tr>
					<?php endforeach; ?>
				</tbody>
			</table><br/>
			
		</div>
	</div>
	
	</div>
	</div>
	</div>
</body>
</html>
<?php require_once("../includes/themes/".THEME_NAME."/footer.php"); ?>
