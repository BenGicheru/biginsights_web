<?php require_once("includes/inc_files.php"); 


if($session->is_logged_in()) {
	$user = User::find_by_id($_SESSION['biginsights']['inc']['user_id']);
	
	redirect_to("my-insights/index.php");
}

$current_page = "home";

if(isset($_SESSION['oauth_message'])){
	$message = $_SESSION['oauth_message'];
	unset($_SESSION['oauth_message']);
}

?>

<?php $page_title = "Welcome"; require_once("includes/themes/".THEME_NAME."/header.php"); ?>

<div class="homepage tabs">
	<div class="container">
		<a href="">
			<div class="first box col-md-4">
				<img src="<?php echo WWW."includes/themes/".THEME_NAME."/img/homepage/"; ?>installer.png" style="float:left;margin: 3px -11px 0px -28px;">
				<div>
					<h3>QPanel Page1</h3>
					<p>Easy to use,  stuff about it</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</a>
		<a href="">
			<div class="box col-md-4">
				<img src="<?php echo WWW."includes/themes/".THEME_NAME."/img/homepage/"; ?>support.png" style="float:left;margin: 3px -6px 0px -19px;">
				<div>
					<h3>Free Support</h3>
					<p>We offer fee support for all of our clients! <br /><br /> Simply create a thread on our support forum, and we will do our best to help you out.</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</a>
		<a href="">
			<div class="box col-md-4">
				<img src="<?php echo WWW."includes/themes/".THEME_NAME."/img/homepage/"; ?>secure.png" style="float:left;margin: 3px 0px 0px -19px;">
				<div>
					<h3>Secure Surveys</h3>
					<p>QPanel has been tested using the very latest vulrenibility testing tools, and has passed with flying colors!</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</a>
	</div>
</div>


<div class="container">

<?php echo output_message($message); ?>

<div class="row center">

	<div class="col-md-12">
		<p>More and more stuff </p>

		<p>More and more stuff</p>

		<p>More and more stuff</p>
	</div>

</div>

<br />

<?php require_once("includes/themes/".THEME_NAME."/footer.php"); ?>