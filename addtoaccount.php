<?php

require_once("includes/inc_files.php");

if(!$session->is_logged_in()) {redirect_to("signin.php");}

$user = User::find_by_id($_SESSION['biginsights']['inc']['user_id']);
$current_page = "addtoaccount";
$location = "gateway/paypal.php";

$token_packages = User::get_token_packages();

$page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
$per_page = 20;
$total_count = count($token_packages);
$pagination = new Pagination($page, $per_page, $total_count);
$sql = "SELECT * FROM token_packages WHERE status = '1' LIMIT {$per_page} OFFSET {$pagination->offset()}";
$token_packages = User::find_by_sql($sql);

?>
<?php $page_title = "Add to Account"; require_once("includes/themes/".THEME_NAME."/header.php"); ?>

<?php echo output_message($message); ?>

	<?php if(empty($token_packages)){ ?>
		Sorry, no token packages could be found.
	<?php } else { ?>
	<table class="table table-bordered">
	  <thead>
	    <tr>
	      <th>Name</th>
	      <th>Amount to add</th>
			<th>Package Price</th>
			<th style="width: 100px;"></th>
	    </tr>
	  </thead>
	  <tbody>
		<?php foreach($token_packages as $data): ?>
	    <tr>
			<td><?php echo $data->name; ?></td>
			<td><?php echo CURRENCYSYMBOL.$data->qty; ?></td>
			<td><?php echo CURRENCYSYMBOL . TOKEN_PRICE * $data->qty; ?></td>
			<td><a href="addtoaccount.php?purchase=<?php echo $data->id; ?>" class="btn btn-primary" style="padding: 1px 5px;">Add</a></td>
	    </tr>
		<?php endforeach; ?>
	  </tbody>
	</table>

	<?php
		if($pagination->total_pages() > 1) {
		echo "<ul class=\"pagination\">";

			for($i=1; $i <= $pagination->total_pages(); $i++) {
				if($i == $page) {
					echo " <li class='active'><a>{$i}</a></li> ";
				} else {
					echo " <li><a href=\"find.php?search={$query}&amp;filter={$_GET['filter']}&amp;page={$i}\">{$i}</a></li> "; 
				}
			}

		}

		echo "</ul>";
	?>
	
		<?php if(isset($_GET['purchase'])) {?>
		
			<form action="<?php echo $location; ?>" method="POST" id="purchase" class="modal">
			    <div class="modal-header"><a href="addtoaccount.php" class="close" data-dismiss="modal">×</a>
			        <h3 id="myModalLabel">Add to Account</h3>
			    </div>
			    <div class="modal-body">
			      <strong>Are you sure you want to add this amount to your account?</strong>
					<input type="hidden" name="id" value="<?php echo $_GET['purchase']; ?>" />
			    </div>
			    <div class="modal-footer">
				   <a href="addtoaccount.php" class="btn">Close</a>
				   <button class="btn btn-danger" type="submit" name="purchase">Add</button>
				 </div>
			</form>​
			<div class="modal-backdrop fade in"></div>

			<form action="<?php echo $location; ?>" method="POST" class="modal" id="purchase" tabindex="-1" role="dialog" aria-labelledby="purchase" aria-hidden="false" style="display: block;">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			      	<a href="addtoaccount.php" class="close" data-dismiss="modal">&times;</a>
			        <h4 class="modal-title">Add to account</h4>
			      </div>
			      <div class="modal-body">
					<strong>Are you sure you want to add this amount to your account?</strong>
					<input type="hidden" name="id" value="<?php echo $_GET['purchase']; ?>" />
			      </div>
			      <div class="modal-footer">
			        <a href="addtoaccount.php" class="btn btn-default">Close</a>
			        <button class="btn btn-danger" type="submit" name="purchase">Add</button>
			      </div>
			    </div><!-- /.modal-content -->
			  </div><!-- /.modal-dialog -->
			</form><!-- /.modal -->
			<div class="modal-backdrop fade in"></div>
		<?php } ?>
	
	<?php } ?>

<?php require_once("includes/themes/".THEME_NAME."/footer.php"); ?>