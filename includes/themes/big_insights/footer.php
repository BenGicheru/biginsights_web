</div> <!-- /container -->

<div style=" padding: 10px 0px;background-color: #F8F8F8;border-top: 1px solid #DDD;margin-bottom: -9px;margin-top:15px;">
		<div class="container">
		<span style="float:left; padding-right:50px;"><a href="http://biginsights.io/"><img src="<?php echo WWW; ?>includes/themes/<?php echo THEME_NAME; ?>/img/biginsights-blue.png" width="107" height="29" alt="BIG INSIGHTS" style="margin: -5px 0px 0px;"></a></span>
			<div style="float:left;margin-top: 3px;margin-right:15px;"><strong style="font-size: 21px;color: #6D6D6D;">Start getting Insights</strong></div> 
			<div style="float:left;margin-top: 8px;margin-right:15px;">Why not deploy a Survey?</div> 
			<a href="<?php echo WWW; ?>my-insights/forms.php" class="btn btn-success btn-large" style="float:left">Deploy Survey &raquo;</a>
		</div>
	</div>


	
	<footer class="footer-distributed">

			<div class="footer-left">

				<h3><img src="<?php echo WWW; ?>includes/themes/<?php echo THEME_NAME; ?>/img/biginsights-blue.png" width="107" height="29" alt="BIG INSIGHTS" style="margin: -5px 0px 0px;"></h3>

				<p class="footer-links">
					<a href="http://biginsights.io">Home</a>
					·
					<a href="https://github.com/BensonGicheru/Big-Insights">Android SDK</a>
					·
					<a href="http://biginsights.io/my-insights/dashboard.php">My Insights</a>
					
					<a href="http://biginsights.io/contact.php">Contact</a>
				</p>

				<p class="footer-company-name">Big Insights &copy; 2015</p>
			</div>

			<div class="footer-center">

			<!--
				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>3rd Floor, Bishop Magua Center</span> Ngong Rd, Nairobi Kenya</p>
				</div>
				-->

				<div>
					<i class="fa fa-phone"></i>
					<p>+254 714 649278</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@biginsights.io">support@biginsights.io</a></p>
				</div>

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>About BIG Insights</span>
					Big Insights is a Web & Mobile Survey and Analytics Engine available on the Cloud as a service. Deploy one survey and reach millions of people with our awesome tools.
				</p>

				<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="https://github.com/BensonGicheru/Big-Insights"><i class="fa fa-github"></i></a>

				</div>

			</div>

		</footer>


    
    <script type="text/javascript">var WWW = "<?php echo WWW; ?>";</script>
    <script src="<?php echo WWW; ?>includes/global/js/main.js"></script>
    <script src="<?php echo WWW; ?>includes/themes/<?php echo THEME_NAME; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo WWW; ?>includes/themes/<?php echo THEME_NAME; ?>/js/tweetable.jquery.js"></script>
    <script src="<?php echo WWW; ?>includes/themes/<?php echo THEME_NAME; ?>/js/jquery.bxslider.js"></script>
    <script type="text/javascript"></script>
	<script src="<?php echo WWW; ?>includes/global/js/jquery.imgareaselect.min.js"></script>
	<script src="<?php echo WWW; ?>includes/global/js/chosen.jquery.min.js"></script>
	<script type="text/javascript">
		$('#tweets').tweetable({username: 'biginsights',limit: 2,replies: false});
		$(".chzn-select").chosen();
		$(document).ready(function() {
			$(function() {
				$('.dropdown-toggle').dropdown();
				$('.dropdown, .dropdown input, .dropdown label').click(function(e) {
					e.stopPropagation();
				});
			});
		});
		$(function(){
			$("[rel='tooltip']").tooltip();
			$("[rel='popover']").popover();
		});
	</script>
  </body>
</html>

<?php if(!$session->is_logged_in()) { ?>

  <!-- Sign In Modal -->
  <div class="modal fade" id="signin_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Sign In</h4>
        </div>
        <div id="login_form" onkeypress="if(event.keyCode == 13){login()}" class="modal-body form-signin">
			<h2 class="form-signin-heading">Please sign in</h2>

			<div id="message"></div>

			<input type="text" class="form-control" name="username" id="username" placeholder="Username">
			<input type="password" class="form-control" name="password" id="password" placeholder="Password">
			<label class="checkbox">
				<input type="checkbox" name="remember_me" id="remember_me"> Remember me
			</label>
			<button class="btn btn-lg btn-primary btn-block" type="submit" name="login_btn" id="login_btn" onclick="login()">Sign in</button>

			<br />

			<a href="reset_password.php" style="float:right">Forgot Password?</a>
			<div class="clearfix"></div>

			<?php if(OAUTH == "ON"){ ?>
			<hr />
				
			<div class="row-fluid">
				<div class="span12 center">
					<div class="span12">
						<?php if(FACEBOOK_APP_ID != ""){ ?><a href="<?php echo WWW; ?>auth/facebook" class="zocial facebook">Sign in with Facebook</a><?php } ?>
						<?php if(TWITTER_CONSUMER_KEY != ""){ ?><a href="<?php echo WWW; ?>auth/twitter" class="zocial twitter">Sign in with Twitter</a><?php } ?>
						<?php if(GOOGLE_CLIENT_ID != ""){ ?><a href="<?php echo WWW; ?>auth/google" class="zocial google">Sign in with Google</a><?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

 <?php } else { ?>

  <!-- Confirm Purchase Modal -->
  <div class="modal fade" id="confirm_purchase_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Confirm Purchase</h4>
        </div>
        <div class="modal-body">
        	<div class="row">
        		<div class="col-md-12">
        			<strong>Area you sure about purchasing access to this page?</strong>
        		</div>
        	</div>
        	<br />
        	<div class="row">
        		<div class="col-md-12">
        			<strong>Once you have clicked "Confirm", <span id="purchase_amount">NUM</span> tokens will be deducted from your account and you will be able to see the content right away.</strong>
        		</div>
        	</div>
        </div>
	    <div class="modal-footer">
			<button class="btn btn-primary" data-dismiss="modal">Close</button>
			<input class="btn btn-success" type="submit" id="confirm" value="Confirm" />
	    </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->


 <?php } ?>
