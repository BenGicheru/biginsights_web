<?php

if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) exit("No direct access allowed.");


error_reporting(0);
ob_start();
ob_clean();
session_start();
defined("DB_SERVER") ? null : define("DB_SERVER", "localhost");
defined("DB_USER")   ? null : define("DB_USER", "kad");
defined("DB_PASS")   ? null : define("DB_PASS", "kad");
defined("DB_NAME")   ? null : define("DB_NAME", "ams4");
require("core_settings.class.php");
$core_settings = Core_Settings::find_by_sql("SELECT * FROM core_settings");
$count = count($core_settings);
for($i=0;$i <= $count-1;$i++){
	defined($core_settings[$i]->name) ? null : define($core_settings[$i]->name, $core_settings[$i]->data);
}
defined("IMAGES") ? null : define("IMAGES", WWW."img/");
date_default_timezone_set(TIMEZONE);

defined("RECAPTCHA_PUBLIC")   ? null : define("RECAPTCHA_PUBLIC", "");
defined("RECAPTCHA_PRIVATE")   ? null : define("RECAPTCHA_PRIVATE", "");

defined("AUTHPATH")   ? null : define("AUTHPATH", "/auth/");
defined("AUTHSALT")   ? null : define("AUTHSALT", "7687687yuiy997097987986355w343fhttki75");

defined("FACEBOOK_APP_ID")   ? null : define("FACEBOOK_APP_ID", "");
defined("FACEBOOK_APP_SECRET")   ? null : define("FACEBOOK_APP_SECRET", "");

defined("TWITTER_CONSUMER_KEY")   ? null : define("TWITTER_CONSUMER_KEY", "");
defined("TWITTER_CONSUMER_SECRET")   ? null : define("TWITTER_CONSUMER_SECRET", "");

defined("GOOGLE_CLIENT_ID")   ? null : define("GOOGLE_CLIENT_ID", "");
defined("GOOGLE_CLIENT_SECRET")   ? null : define("GOOGLE_CLIENT_SECRET", "");


?>
